﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        hdPlatformID.Value = rbm.getPlatformID();
        var obj = rbm.Platform.getData();
        hdPlatform.Value = obj.platform;
        hdPlatformUrl.Value = obj.url;
        
        try
        {
            rbm.member.stMembers user = (rbm.member.stMembers)Session["user"];
            if (rbm.isNull(user))
            {
                adminControls.Visible = false;
                lnkBucketWrench.Visible = false;
                lnkThemeWrench.Visible = false;
            }
            if (!user.isAdmin&&!rbm.isNull(user))
            {
                lnkBucketWrench.Visible = false;
                lnkThemeWrench.Visible = false;
            }
        }
        catch (Exception)
        {
            adminControls.Visible = false;
            lnkBucketWrench.Visible = false;
            lnkThemeWrench.Visible = false;
        }
    }
    protected string getUser()
    {
       
        try
        {
            var user = (rbm.member.stMembers)Session["user"];
            return user.id;
        }
        catch (Exception)
        {

            return "0";

        }
    }

    protected string wrenchLink()
    {
        var obj = rbm.Platform.getData();
        hdPlatform.Value = obj.platform;
        hdPlatformUrl.Value = obj.url;
        return "/" + obj.url + "/admin/themes";
    }

    protected void lnkThemeWrench_Click(object sender, EventArgs e)
    {
        var obj = rbm.Platform.getData();
        hdPlatform.Value = obj.platform;
        hdPlatformUrl.Value = obj.url;
        Response.Redirect("/" + obj.url + "/admin/themes");
    }

    protected void lnkBucketWrench_Click(object sender, EventArgs e)
    {
        var obj = rbm.Platform.getData();
        hdPlatform.Value = obj.platform;
        hdPlatformUrl.Value = obj.url;
        Response.Redirect("/" + obj.url + "/admin/buckets");
    }
</script>
<asp:Label ID="meta_title" runat="server" Text="ASSETS" Visible="false" />

<inc:module ID="Script" runat="server">
    <script type="text/javascript" src="/js/controllers/LibraryController.js"></script>

</inc:module>
<inc:module ID="style" runat="server">
    <link rel="stylesheet" href="/styles/ng-tags-input.min.css" />
</inc:module>

<div data-ng-controller="LibraryController" class="row">
        <div class="loading-data" id="loading-data" data-ng-hide="loadingData" >
        LOADING DATA... <img data-ng-src="{{loadingImageSrc}}" />
    </div>
    <asp:HiddenField ID="hdPlatform" runat="server" />
    <asp:HiddenField ID="hdPlatformID" runat="server" />
    <asp:HiddenField ID="hdPlatformUrl" runat="server" />
    <!--[if lte IE 8]>
    <style>
    .old_browser        {color: #FFF; font-size: 15px; text-align: center; padding: 20px; background: #ff3600 }
    .old_browser strong    {font-weight: bold; font-size: 18px}
    </style>
    <![endif]-->


    <div class="old_browser" data-ng-show="updateBrowser">
        <strong>Alert:  Old Browser detected</strong><br />
        It has been detected that you are using an older browser that cannot run this website's tools properly.<br />
        Please update your browser or use Firefox, Chrome or Safari to view this site.  
        <br />
        <a class="browser-chrome" href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">
            <img class="browser-chrome" style="width:100px"  src="/images/chrome.png" />
        </a>
        <a class="browser-firefox" href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">
            <img class="browser-firefox" style="width:100px"  src="/images/firefox.png" />
        </a>
        <a class="browser-safari" href="https://support.apple.com/downloads/safari"  target="_blank">
            <img class="browser-safari" style="width:100px"  src="/images/safari.png" />
        </a>
    </div>

    <div data-ng-cloak  data-ng-show="loadingData">
        <!-- SIDEBAR START -->
        <div id="sidebar" class="col-md-3">  
            <h2>Refine Search</h2>  
           <!-- <a href="/" class="btn btn-default btn-block"><img alt="Search" src="/images/icon_search.png"/> &nbsp; Search</a>-->
            <div id="assetSearch" class="asset_search">
                <div class="bucket_search" >
                    <h4>TYPE <asp:linkbutton id="lnkBucketWrench" runat="server" onclick="lnkBucketWrench_Click"><img alt="Manage" src="/images/icon_manage.png" class="pull-right"/></asp:linkbutton></h4>
				    <span class="filter_help">Check type to filter</span>
                    <ul class="SidebarList" data-ng-if="hasCategories" >
                        <li ng-repeat="category in categories">
                            <input type="checkbox" id="promo_{{$index}}" ng-model="category.selected" ng-change="watchBucketSet(category)" />
                            <label for="promo_{{$index}}""><span></span>{{category.categoryTitle}}</label>
                            <ul class="SidebarSublist">
                                <li ng-repeat="bucket in searchBuckets">
                                    <div data-ng-if="bucket.bucketCategoryID == category.categoryID">
                                        <input type="checkbox" name="selectedBuckets[]" ng-model="bucket.selected" ng-change="watchBucket(bucket);watchForAllBuckets()" id="type_{{$index}}" />
                                        <label for="type_{{$index}}"><span></span>{{bucket.title}}</label>
                                    </div>

                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
                <div class="theme_search">
                    <h4>THEME <asp:linkbutton id="lnkThemeWrench" runat="server" OnClick="lnkThemeWrench_Click"><img alt="Manage" src="/images/icon_manage.png" class="pull-right"/></asp:linkbutton></h4>
                    <%--<a data-ng-click="searchAllThemes()" class="btn btn-default btn-block">More Themes</a>--%>
                    <div ng-repeat="opt in allMyThemes| limitTo:5" class="SidebarList">
                        <input type="checkbox" name="selectedThemes[]" ng-model="opt.selected" ng-change="watchTheme(opt)" id="theme_{{$index}}" >
                        <label for="theme_{{$index}}"><span></span>{{opt.theme}}</label>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR ENDS -->

        <!-- PAGE CONTENT STARTS -->
        <div id="PgCnt" class="col-md-9">
            <h1>Promotional Materials <img src="/images/bg_title.png" class="title_decor" /></h1>
            <div id="assetList" class="asset_list">
                <div class="row">
                    <div class="col-md-5">
                        <input placeholder="Quick Search" ng-model="searchBox" class="form-control input-sm search">
                        <a class="btn btn-primary btn-xs"  ng-click="clearFilters()" ng-show="platformSelection||categorySelection||themeSelection||searchBox||themeIncludes.length > 0">Reset Search</a>
                    </div>
                    <div class="col-md-7" align="right">
                        <div class="row">
                            <div class="col-md-6">
							    <div class="form-inline">
								    <label class="items">Sort by:</label>
									    <select class="form-control input-sm"  ng-model="selectSort" ng-options="option as option.label for option in selectSortOptions" ng-init="selectSort = selectSortOptions[0]; setInitialSort('saveDate')" ng-change="watchSelectSort(selectSort.value)"></select>
							    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <label class="items">Items per page:</label>
                                    <select id="item_per_page" class="form-control input-sm"  ng-model="displayCount" ng-options="option as option.value for option in displayCountOptions" ng-init="displayCount = displayCountOptions[0]"></select>
                                </div>
                            </div>
                            <div class="clearboth">&nbsp;</div>
                        </div>
                    </div>
                    <div class="clearboth">&nbsp;</div>
                </div>
                <div  data-ng-if="!editMode">
                <table class="table table-hover TblList">
                    <thead>
                        <tr>
                            <th width="14%"></th>
                            <th width="8%" ng-click="sort('bucketID')" align="center">TYPE<i ng-class="{'icon-chevron-up': isSortUp('category'), 'icon-chevrondown':isSorthown('category')}"></i></th>
                            <th width="50%" ng-click="sort('title')">TITLE<i ng-class="{'icon-chevron-up': isSortUp('title'), 'icon-chevrondown':isSorthown('title')}"></i></th>
                             <th width="13%" ng-click="sort('saveDate')">ADDED ON<i ng-class="{'icon-chevron-up': isSortUp('saveDate'), 'icon-chevrondown':isSorthown('saveDate')}"></i></th> 
                            <th width="15%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="asset in filteredAssets = (allMyAssets |filter:{active:true} |filter: {title:searchBox}| filter: filterBuckets | filter: filterThemes | orderBy:sortField:reverse) | itemsPerPage: displayCount.value" current-page="currentPage" data-ng-class="{new:asset.new}">
                            <td align="center">
                                <div data-ng-if="asset.bucketTypeID == 2">
                                    <img class="table-img" alt="{{asset.title}}" ng-src="{{asset.path}}" /> 
                                </div>  
                                <div data-ng-if="asset.bucketTypeID == 1">
                                    <img class="table-img" alt="{{asset.title}}" ng-src="{{asset.linkImage}}" /> 
                                </div>
                                <div data-ng-if="asset.bucketTypeID == 3">
                                    <img class="table-img" alt="{{asset.title}}" ng-src="{{asset.path}}" /> 
                                </div> 
                                <div data-ng-if="asset.bucketTypeID == 4">
                                    <img class="table-img" alt="{{asset.title}}" ng-src="{{asset.linkImage}}" err-src="{{asset.path}}" /> 
                                </div> 
                            </td>
                           <td class="TD_type">
							    <span ng-if="asset.bucketTypeID == 1">Video</span>
							    <span ng-if="asset.bucketTypeID == 2 || asset.bucketTypeID == 4">Image</span>
							    <span ng-if="asset.bucketTypeID == 3">Document</span>
					       </td>
                            <td>{{asset.title}}</td>
                            <!-- <td>{{asset.platform}}</td> -->
                             <td>{{asset.saveDate | date: 'M/d/yyyy'}} </td> 
                            <td>
                                <div class="TblBtns">
                                    <div class="TblBtnsCnt">
                                        <a Class="btn btn-default btn-block btn-xs" ng-click="preview(asset)">Preview</a>
                                        <a Class="btn btn-default btn-block btn-xs" ng-click="previewDownload(asset)">Download</a>
                                    </div>
                                </div>
                                <div id="adminControls" runat="server" class="TblBtns BtnsAdmin">
                                    <div class="TblBtnsCnt">
                                        <a Class="btn btn-primary btn-block btn-xs MarginTopThree" data-ng-click="editAsset(asset)">Manage</a>
                                        <a Class="btn btn-danger btn-block btn-xs MarginTopThree" data-ng-click="deleteResource(asset, $index)">Delete</a> 
                                    </div>
                                </div>
             
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="CntPagination">
                    <div class="row">
                        <div class="ItemsPerPg col-md-6" data-ng-if="filteredAssets.length == allMyAssets.length">{{pagerMessage}}</div>
                        <div class="ItemsPerPg col-md-6"  data-ng-if="filteredAssets.length != allMyAssets.length">{{filteredAssets.length + ' items found.'}}</div>
                        <div class="PgPrevNext col-md-6" align="right">
                            <dir-pagination-controls direction-links="true" on-page-change="myMethod(newPageNumber, displayCount.value, filteredAssets.length);setPage(newPageNumber)" template-url="/js/directives/pagination/dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
        <!-- EDIT PAGE CONTENT STARTS -->
        <div data-ng-if="editMode" id="PgCnt" class="col-md-9" data-ng-show="!loading">
            <h1>Administration <img src="/images/bg_title.png" class="title_decor" /></h1>
          <input type="hidden" data-ng-model="userID" data-ng-init="setUserID('<%= getUser().ToString() %>')" />

            <div class="form-horizontal MarginTopThirty">
                <div class="form-group">
                    <label id="lblNewAsset" for="btnNewAsset" class="col-sm-2 control-label">NEW RESOURCE</label>
                    <div class="col-sm-10">
                        <a href="/admin/new_resource" class="btn btn-default" id="btnNewAsset">NEW RESOURCE</a>
                    </div>
                </div>
                <div class="form-group">
                    <label id="lblPlatform" for="ddlPlatform" class="col-sm-2 control-label">PLATFORM</label>
                    <div class="col-sm-10">
                        <select id="ddlPlatform" class="form-control" data-ng-model="selectedAsset.platformID" data-ng-options="opt.id as opt.platform for opt in platforms" data-ng-disabled="true"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ddlBucket" class="col-sm-2 control-label">BUCKET</label>
                    <div class="col-sm-10">
                        <select id="ddlBucket" class="form-control" data-ng-model="selectedAsset.bucketID" data-ng-options="opt.id as opt.title for opt in buckets"  data-ng-disabled="true"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtTitle" class="col-sm-2 control-label">TITLE</label>
                    <div class="col-sm-10">
                        <input type="text" id="txtTitle" class="form-control" data-ng-model="selectedAsset.title" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtDescription" class="col-sm-2 control-label">DESCRIPTION</label>
                    <div class="col-sm-10">
                        <textarea id="txtDescription" class="form-control" rows="3" data-ng-model="selectedAsset.description"></textarea>
                    </div>
                </div>
                <!-- VIDEO TYPE -->
                <div class="form-group" data-ng-if="selectedAsset.bucketTypeID==1">
                    <label class="col-sm-2 control-label">VIDEO LINK</label>
                    <div class="col-sm-10">
                        <input type="text" id="txtVideoLink" class="form-control" data-ng-model="selectedAsset.link" />

                    </div>
                </div>
                <div class="form-group" data-ng-if="selectedAsset.bucketTypeID==1">
                    <label  class="col-sm-2 control-label">PREVIEW</label>
                    <div class="col-sm-10">
                        <div class="videoWrapper">
                            <iframe width="512" height="288" src="{{trustedLink}}"  frameborder="0" allowfullscreen></iframe> 
                        </div>
                    </div>
                </div>
                <!--END VIDEO TYPE -->
                <!-- IMAGE OR PRINT TYPE -->
                <div class="col-md-12" data-ng-if="selectedAsset.bucketTypeID != 1">

                    <div class="clearboth">&nbsp;</div>
                    <div class="form-group" ng-repeat="item in uploader.queue">
                        <label for="fileNameSd" class="form-label">Name:</label> <span id="fileNameSd" ng-bind="item.file.name"></span><br/>
                        <label for="fileSizeSd" class="form-label">Size:</label> <span id="fileSizeSd" ng-bind="item.file.size"></span><br/>
                        <a href="" class="btn btn-primary btn-sm" ng-click="uploader.clearQueue()">Cancel</a>
                        <a class="btn btn-primary btn-sm" ng-click="item.upload()">Upload File</a>
                    </div>
                </div>
                <div class="clearboth">&nbsp;</div>
                <div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-10">
                        <div>
                            <div class="col-md-7 NoPadLeft ">
                                <label ID="lblFileInfo1" data-ng-if="selectedAsset.bucketTypeID!=1" class="current_file">Filename: {{fileData.filename}}</label><br />
                                <label ID="lblFileInfo2" data-ng-if="selectedAsset.bucketTypeID!=1"   class="current_file">Size: {{fileData.filesize}}</label><br />
                                <label ID="lblFileInfo3" data-ng-if="selectedAsset.bucketTypeID==2"   class="current_file">Width: {{fileData.width}} | Height:{{fileData.height}}</label>  
                            </div>
                            <div class="col-md-5 NoPadRight">
                                <label class="btn btn-default btn-sm pull-right" for="my-file-sd">BROWSE FILES
                                    <input id="my-file-sd" style="display:none"  type="file" nv-file-select uploader="uploader" ng-click="uploader.clearQueue()"/><br/>
                                </label>
                            </div>
                            <div class="clearboth">&nbsp;</div>
                        </div>
                    </div>
                    </div>
                    <div class="clearboth">&nbsp;</div>
                </div>
                <!--END IMAGE OR PRINT TYPE -->

                <div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-6">
                        <!-- BEGIN TOOLS -->
                        <div class="form-group XtraOptions" data-ng-if="selectedAsset.bucketTypeID!=3">
                            <div  data-ng-repeat="tool in resourceTools" class="TopBottomPad" >
                                <input id="cb_{{$index}}" type="checkbox" data-ng-model="tool.selected" ng-change="checkForOutput(tool,selectedAsset.bucketTypeID)"> 
                                <label for="cb_{{$index}}" class="custom-label"><span></span>{{tool.title}}</label>
                                <!-- VIDEO OPTIONS -->
                                <div data-ng-if="selectedAsset.bucketTypeID==1">
                                    <input type="text" id="txtToolWidth1" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setHeight($index)" />
                                    <span class="size_option">X</span>
                                    <input type="text" id="txtToolHeight1" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setWdith($index)" />
                                    <span class="size_options">pixels</span>
                                </div>
                                <!-- PRINT MEDIA OPTIONS -->
                                <div data-ng-if="selectedAsset.bucketTypeID==4 && (tool.toolID==19||tool.toolID==20||tool.toolID==21)">
                                    <input type="text" id="printMediaWidth" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setPrintMediaHeight($index)" />
                                    <span class="size_option"> % of actual size.</span>

                                </div>
                                <!-- IMAGE OPTIONS -->
                                <div data-ng-if="selectedAsset.bucketTypeID == 2 && tool.toolID != 17 && tool.toolID !=18" class="TopBottomPad">
                                    <input type="text" id="imageWidth" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setImageHeight($index)" />
                                    <span class="size_option">X</span>
                                    <input type="text" id="imageHeight" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setImageWidth($index)" />
                                    <span class="size_options">pixels</span>
                                </div>
                            </div>
                        </div>
                        <!-- END TOOLS -->
                    </div>
                    <div class="col-md-4">
                        <div  data-ng-if="selectedAsset.bucketTypeID==3">
                            <div class="image_preview">
                                DOCUMENT ICON HERE               
                            </div>
                        </div>
                        <div data-ng-if="selectedAsset.bucketTypeID==2">
                            <div class="image_preview">
                                <img id="litImagePreview" alt="{{selectedAsset.title}}" src="{{'/_resources/' + platform_title +'/' + selectedAsset.bucket + '/' + selectedAsset.filename}}" />                  
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">&nbsp;</div>
                </div>

                <!--DOCUMENT TYPE -->
                <!--END DOCUMENT TYPE -->

                <!-- BEGIN THEMES -->          
                <div class="form-group">
                        <div class="image_themes">
                            <label id="lblTags" for="txtTags" class="col-sm-2 control-label">THEMES</label>
                            <div class="col-sm-10">
                                Press 'ESC' to enter a new theme.
                                <tags-input ng-model="$parent.tags" addFromAutocompleteOnly="false">
                                    <auto-complete source="loadedTags"></auto-complete>
                                </tags-input>
                            </div>
                        </div>
                    </div>
                <!-- END THEMES -->
                <!-- BEGIN FAQ -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Related FAQ</label>
                        <div class="col-sm-10">
                            <div data-ng-repeat="question in resourceQuestions" class="form-cnt">
                                {{question.question}}
                                <a data-ng-click="removeFAQ($index)" selectFirstMatch="false"><span class="glyphicon glyphicon-remove-circle remove-icon"></span></a>            
                            </div>
                        </div>
                        </div>
                    <div class="form-group">
                        <label for="ddlFaq" id="lblFaq" class="col-sm-2 control-label">Add Custom FAQ</label>
                        <div class="col-sm-10">
                            <select ID="ddlFaq" class="form-control" data-ng-model="includedQuestion.opt" data-ng-options="idx as opt.question for (idx,opt) in allQuestions" data-ng-change="addQuestion()"></select>

                        </div>
                    </div>
                <!-- END FAQ -->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                            <a id="btnCancel" class="btn btn-primary" data-ng-click="cancelEditMode()">Cancel</a>
                            <a id="btnSave" class="btn btn-default" data-ng-click="saveEdits()">Save</a>
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                </div>

        <!-- EDIT PAGE CONTENT ENDS -->
    </div>
    </div>

</div>

