﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<script language="c#" runat="server">

    public void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lstRotator.DataSource = rbm.Resources.recentlyAdded(rbm.getPlatformID());
            lstRotator.DataBind();

            string URL = Request.Url.ToString();
            if (URL.Contains("how_to") || URL.Contains("about") || URL.Contains("admin"))
            {
                SliderContainer.Visible = false;
            }
        }
    }
</script>

<!-- SLIDER STARTS -->
<div id="SliderContainer" runat="server" data-ng-controller="RotatorController">
  <h1>Recently Added <img src="/images/bg_subtitle.png" /></h1>
  <div class="slider">
    <div class="carousel">
      <asp:Repeater runat="server" ID="lstRotator">
        <ItemTemplate>
          <div>
            <a class="slick_link" data-ng-click="viewResource('<%# Eval("id") %>','<%# Eval("bucketID") %>')"><img class="carousel_image" alt="<%# Eval("title") %>" src="<%# Eval("path") %>" /></a>
          </div>
        </ItemTemplate>
      </asp:Repeater>
    </div>
    <div class="clearboth">&nbsp;</div>
  </div>
</div>
<!-- SLIDER ENDS -->

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css"/>
<script type="text/javascript" src="/js/controllers/RotatorController.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.carousel').slick({
            arrows:true,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });

    });


</script>