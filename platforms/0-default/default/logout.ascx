﻿<%@ Control Language="C#" %>

<script runat="server">

	protected void Page_Load(object sender, EventArgs e){
		Session.Abandon();
		Response.Redirect("/");
	}
</script>