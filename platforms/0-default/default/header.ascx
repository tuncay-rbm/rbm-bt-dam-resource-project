﻿<%@ Control Language="C#" ClassName="header" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
	    if (!Page.IsPostBack)
	    {
		    try
		    {
			    var user = (rbm.member.stMembers) Session["user"];

			    if (user.isAdmin)
			    {
				    admin_nav.Visible = true;
				    logoutLink.Visible = true;
				    resourceLink.Visible = true;
				    faqLinkAdmin.Visible = true;
				    bucketLink.Visible = true;
				    memberLink.Visible = true;
				    statsLink.Visible = true;
				    platformLink.Visible = true;
				    categoryLink.Visible = true;

			    }
			    else
			    {
				    logoutLink.Visible = true;
				    resourceLink.Visible = true;
				    faqLinkAdmin.Visible = true;
				    bucketLink.Visible = false;
				    memberLink.Visible = false;
				    platformLink.Visible = false;
				    statsLink.Visible = false;
				    categoryLink.Visible = false;

			    }
		    }
		    catch (Exception)
		    {
			    admin_nav.Visible = false;

			    logoutLink.Visible = false;
			    resourceLink.Visible = false;
			    faqLinkAdmin.Visible = false;
			    bucketLink.Visible = false;
			    memberLink.Visible = false;
			    platformLink.Visible = false;
			    statsLink.Visible = false;
			    categoryLink.Visible = false;

		    }

		    string dom = Request.QueryString["platformName"];
		    if (Request.RawUrl.Contains("/admin/resources"))
		    {
			    resourceLink.CssClass = "header_link active";
		    }
		    else if (Request.RawUrl.Contains("/admin/faqs"))
		    {
			    faqLinkAdmin.CssClass = "header_link active";
		    }
		    else if (Request.RawUrl.Contains("/admin/buckets"))
		    {
			    bucketLink.CssClass = "header_link active";
		    }
		    else if (Request.RawUrl.Contains("/admin/categories"))
		    {
			    categoryLink.CssClass = "header_link active";
		    }
		    else if (Request.RawUrl.Contains("/admin/stats"))
		    {
			    categoryLink.CssClass = "header_link active";
		    }
		    else if (Request.RawUrl.Contains("/admin/members"))
		    {
			    memberLink.CssClass = "header_link active";
		    }

		    resourceLink.NavigateUrl = "/" + dom + resourceLink.NavigateUrl;
		    faqLinkAdmin.NavigateUrl = "/" + dom + faqLinkAdmin.NavigateUrl;
		    bucketLink.NavigateUrl = "/" + dom + bucketLink.NavigateUrl;
		    categoryLink.NavigateUrl = "/" + dom + categoryLink.NavigateUrl;

		    memberLink.NavigateUrl = "/" + dom + memberLink.NavigateUrl;
		    platformLink.NavigateUrl = "/" + dom + platformLink.NavigateUrl;
		    statsLink.NavigateUrl = "/" + dom + statsLink.NavigateUrl;
		    logoutLink.NavigateUrl = "/" + dom + logoutLink.NavigateUrl;
	    }
        
    }
    protected string getHomeLink()
    {
        var platform = rbm.Platform.getData(rbm.getPlatformID());

        return "/" + platform.url; 
    }
</script>

<!-- HEADER START -->
<header>
	<div class="CntWrapper">
		<div class="logo">
			<a href="<%= getHomeLink() %>">
                <img src="/_static_docs/logo_CustomReach.jpg" alt="Home Page" class="logo_customreach">
			</a>
			<a href="<%= getHomeLink() %>"><inc:module runat="server" ID="platformLogo"/></a>
		</div>
	</div>
    <inc:module runat="server" ID="platformHeaderText"></inc:module>
	<div class="CntWrapper">
	    <div class="navigation">
		    <inc:module runat="server" ID="platformMenu"></inc:module>
		    <div class="clearboth">&nbsp;</div>
	    </div>
    </div>
    <div class="AdminNav" id="admin_nav" runat="server">
        <div class="AdminNavContainer">
            <ul id="AdminMenu" class="AdminMenu" runat="server">
				<li><asp:HyperLink CssClass="header_link" ID="resourceLink" runat="server" Visible="false" NavigateUrl="/admin/new_resource" title="Add New Resources">ADD RESOURCE</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="faqLinkAdmin" runat="server" Visible="false" NavigateUrl="/admin/faqs" title="FAQS">FAQS</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="bucketLink" runat="server" Visible="false" NavigateUrl="/admin/buckets" title="Manage Buckets">Buckets</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="categoryLink" runat="server" Visible="false" NavigateUrl="/admin/categories" title="Manage Bucket Categories">Categories</asp:HyperLink></li>

				<li><asp:HyperLink CssClass="header_link" ID="memberLink" runat="server" Visible="false" NavigateUrl="/admin/members" title="Manage Members">Members</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="platformLink" runat="server" Visible="false" NavigateUrl="/admin/platforms" title="Manage Platforms">Platforms</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="statsLink" runat="server" Visible="false" NavigateUrl="/admin/stats" title="View Stats">Stats</asp:HyperLink></li>
				<li><asp:HyperLink CssClass="header_link" ID="logoutLink" runat="server" Visible="false" NavigateUrl="/logout" title="Logout">LOGOUT</asp:HyperLink></li>
            </ul>
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>
</header>
<script>
    jQuery(function () {
        jQuery('.header_link').click(function () {
            var url = jQuery(this).attr('href');
            window.location.href = url;
        });
    });
</script>
<!-- HEADER ENDS -->

<!-- CONTENT AREA START -->
<div class="cnt">
	<div class="CntWrapper">