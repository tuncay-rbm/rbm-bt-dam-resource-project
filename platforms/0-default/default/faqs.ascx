﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="faqs.ascx.cs" Inherits="files_default_faqs" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<asp:Label ID="meta_title" runat="server" Text="FAQS" Visible="false" />

<inc:module ID="Script" runat="server">
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.min.js"></script>
    <script type="text/javascript" src="/js/directives/ngClip.js"></script>
    <script type="text/javascript" src="/js/controllers/FaqsController.js"></script>
</inc:module>

<div ng-controller="FaqsController" class="row">

    <!-- SIDEBAR START -->
    <div id="sidebar" class="col-md-3">   
        <h2>CATEGORIES</h2>

        <div  ng-repeat="category in faqCategories" class="faq">
            <span><a ng-click="filterQuestionSet(category); setClass(category.id)" data-ng-class="{'active': activeCategory===category.id}">{{category.title}}</a></span>
        </div>
    </div>
    <!-- SIDEBAR ENDS -->

    <!-- PAGE CONTENT STARTS -->
    <div id="PgCnt" class="col-md-9">
        <h1>FAQS <img src="/images/bg_title.png" class="title_decor" /></h1>
        <h2>{{categoryTitle}}</h2>
        <div data-ng-if="!customQ">
            <asp:Panel ID="pnlQuestion" runat="server" Visible="false">
            <span><asp:Literal ID="litQuestion" runat="server"></asp:Literal></span>
                <span>ANSWER</span>
              <span class="faq_answer"><asp:Literal ID="litAnswer" runat="server"></asp:Literal></span>
        </asp:Panel>
        </div>

        <div class="accordion">
            <accordion>
                <accordion-group heading="{{faq.question}}"  ng-repeat="faq in faqs| filter:{categoryID:selectedCategory}" ng-click="filterQuestions(faq.id)">
                    <span class="answer">ANSWER</span>
                    <span class="faq_answer" ng-bind-html="renderHtml(faq.answer)">{{faq.answer}}</span>
                </accordion-group>
            </accordion>
        </div>
    </div>
    <!-- PAGE CONTENT ENDS -->
    
</div>
