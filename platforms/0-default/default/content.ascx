﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<script runat="server">

	protected void Page_Load(object sender, EventArgs e)
	{
		string keyword = Request.QueryString["page"];
		rbm.Platform.stPage page = rbm.Platform.getPage(keyword, true);
		meta_title.Text = header.InnerText = page.title;
		ltContent.Text = page.html;
	}

</script>

<asp:Label ID="meta_title" runat="server" Text="" Visible="false" />
<h1 id="header" runat="server"></h1>
<asp:Literal ID="ltContent" runat="server"></asp:Literal>