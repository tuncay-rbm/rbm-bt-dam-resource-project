﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<script language="c#" runat="server">
    public void Page_Load(object sender, EventArgs e){
        var obj = rbm.Platform.getData(rbm.getPlatformID());
        if (!rbm.isNull(obj.header_text))
        {
            ltHeaderText.Text = "<div class=\"platform_hdr_text\">" + "<div class=\"CntWrapper\">" + obj.header_text + "</div></div>";
        }
        else
            ltHeaderText.Text = "";
    }
</script>

<asp:Literal ID="ltHeaderText" runat="server"></asp:Literal>