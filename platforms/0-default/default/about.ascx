﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="about.ascx.cs" Inherits="files_about" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<inc:module ID="style" runat="server">
    <link href="/styles/uploadifive.css" rel="stylesheet" />
    <link href="/styles/jquery.tagsinput.css" rel="stylesheet" />

</inc:module>
<inc:module ID="script" runat="server">
    <script src="/js/jquery.uploadifive.min.js"></script>
    <script src="/js/jquery.tagsinput.min.js"></script>
</inc:module>


<inc:module runat="server" ID="login_check"></inc:module>

<div id="admin_resource" class="row">

    <!-- SIDEBAR START -->
    <div id="sidebar" class="col-md-3">  
        <h2>About</h2>  

    </div>
    <!-- SIDEBAR ENDS -->

    <!-- PAGE CONTENT STARTS -->
    <div id="PgCnt" class="col-md-9">
        <h1>About <img src="/images/bg_title.png" class="title_decor" /></h1>

    </div>
    <!-- PAGE CONTENT ENDS -->


