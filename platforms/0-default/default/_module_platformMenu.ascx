﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<script language="c#" runat="server">
    public void Page_Load(object sender, EventArgs e){
		ltMenu.Text = rbm.Platform.topMenu(rbm.getPlatformID());
    }
</script>

<asp:Literal ID="ltMenu" runat="server"></asp:Literal>
<script>
	jQuery(function () {
		var url = window.location.pathname;
		switch (url) {
			case "/":
				jQuery('#assetLink').addClass('active');
				break;
			case "/faqs":
				jQuery('#faq').addClass('active');
				break;
			case "/home":
				jQuery('#home').addClass('active');
				break;
			case "/extra1":
				jQuery('#extra1').addClass('active');
				break;
			case "/extra2":
				jQuery('#extra2').addClass('active');
				break;
			case "/extra3":
				jQuery('#extra3').addClass('active');
				break;
			case "/extra4":
				jQuery('#extra4').addClass('active');
				break;
		}
	});
</script>
