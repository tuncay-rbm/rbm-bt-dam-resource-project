﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_members: UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindData();
            togglePanel("admin");
        }
    }

    protected void bindData()
    {
		lstMembers.DataSource = rbm.admin.memberList();
		lstMembers.DataBind();
    }
	
	protected void deleteUser(object sender, EventArgs e)
    {
        rbm.member.delete(rbm.getCommandArgument(sender));
        Response.Redirect(Request.RawUrl);
    }

    protected void clearForm()
    {
        txtId.Value = "";
        txtName.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        lstStatus.SelectedValue = "0";
        lstAdmin.SelectedValue = "0";
        bindData();
    }
    
	protected void lnkEdit_Click(object sender, EventArgs e)
    {
        togglePanel("edit");
        rbm.member.stMembers obj = rbm.member.getMember(rbm.getCommandArgument(sender));
        txtId.Value = obj.id;
        txtName.Text = obj.name;
        txtEmail.Text = obj.email;
        txtPassword.Text = obj.password;
        lstStatus.SelectedValue = obj.status;
        lstAdmin.SelectedValue = obj.isAdmin ? "1" : "0";
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        rbm.member.delete(rbm.getCommandArgument(sender));
        bindData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clearForm();
        togglePanel("admin");
    }

    protected void togglePanel(string value)
    {
        if (value == "admin")
        {
            pnlAdmin.Visible = true;
            pnlEdit.Visible = false;
        }
        else
        {
            pnlAdmin.Visible = false;
            pnlEdit.Visible = true;
        }
    }
    
	protected void btnSave_Click(object sender, EventArgs e)
    {
        rbm.member.stMembers obj = new rbm.member.stMembers();
        obj.id = txtId.Value;
        obj.name = txtName.Text;
        obj.email = txtEmail.Text;
        obj.password = txtPassword.Text;
        obj.status = lstStatus.SelectedValue;
        obj.isAdmin = lstAdmin.SelectedValue == "1" ? true:false;

        if (obj.id == "")
        {
            rbm.member.add(obj);
        }
        else
            rbm.member.update(obj, false);

        togglePanel("admin");
        clearForm();
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        togglePanel("edit");
    }

	protected void lstMembers_PageIndexChanging(Object sender, GridViewPageEventArgs e)
	{
		lstMembers.PageIndex = e.NewPageIndex;
		bindData();
	}
}