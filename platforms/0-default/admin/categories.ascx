﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="categories.ascx.cs" Inherits="files_admin_categories" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<inc:module runat="server" ID="login_check"></inc:module>
<inc:module runat="server" ID="script">
    <script src="/js/ckeditor/ckeditor.js"></script>
</inc:module>
<asp:Label ID="meta_title" runat="server" Text="FAQ" Visible="false" />

    <asp:Panel ID="pnlView" runat="server">

    <div class="OneColTitle">
        <h1>CATEGORIES <img src="/images/bg_title.png" class="title_decor" /> <asp:LinkButton ID="lnkAddCat" runat="server" OnClick="lnkAddCat_Click" CssClass="btn btn-default pull-right"><i class="fa fa-plus-square"></i>New Category</asp:LinkButton></h1>
        <div class="clearboth">&nbsp;</div>
    </div>
        <div class="form-horizontal MarginTopThirty row">
    <div class="form-group">
        <asp:Label ID="lblCategorySort" runat="server" AssociatedControlID="ddlCategorySort" CssClass="col-sm-2 control-label">Filter Category Type:</asp:Label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddlCategorySort" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCategorySort_SelectedIndexChanged">
                <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                <asp:ListItem Value="bucket" Text="Bucket Categories"></asp:ListItem>
                <asp:ListItem Value="faq" Text="FAQ Categories"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
      </div>
              <asp:GridView runat="server" ID="lstView" AutoGenerateColumns="false"  CssClass="table table-hover table-custom" AllowSorting="true" AllowPaging="true" PageSize="10" OnPageIndexChanging="lstView_PageIndexChanging" OnSorting="lstView_Sorting">
                <Columns>
                    <asp:TemplateField HeaderText="Category" HeaderStyle-CssClass="header_class" ItemStyle-CssClass="item_class" SortExpression="title">
                        <ItemTemplate>
                            <%# Eval("title").ToString().ToUpper() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type" HeaderStyle-CssClass="header_class" ItemStyle-CssClass="item_class" SortExpression="type">
                        <ItemTemplate>
                            <%# Eval("type").ToString().ToUpper() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEditBucket" runat="server" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary btn-xs" OnClick="lnkEditBucket_Click"> <i class="fa fa-pencil"></i> </asp:LinkButton>
                            <asp:LinkButton ID="lnkDeleteBucket" runat="server" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-danger btn-xs" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" OnClick="lnkDeleteBucket_Click"> <i class="fa fa-trash"></i> </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
        <div class="OneColTitle">
            <h1>CATEGORIES <img src="/images/bg_title.png" class="title_decor" /></h1>
        </div>
        <asp:HiddenField ID="hdID" runat="server" />
        <div class="form-horizontal MarginTopThirty row">
            <div class="form-group">
                <asp:Label ID="lblPlatform" runat="server" AssociatedControlID="txtPlatform" CssClass="col-sm-2 control-label">Category Title:</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtPlatform" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" AssociatedControlID="ddlType" CssClass="col-sm-2 control-label">Category Type:</asp:Label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control">
                        <asp:ListItem Value="faq" Text="FAQ"></asp:ListItem>
                        <asp:ListItem Value="bucket" Text="Bucket"></asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="MarginTopThirty">
            <div class="pull-right">
                <asp:Button ID="btnCancelTag" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancelTag_Click" />
                <asp:Button ID="btnSaveTag" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSaveTag_Click"/>
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>
    </asp:Panel>

<script>
    function UserDeleteConfirmation() {
        return confirm("Are you sure you want to delete this category?");
    }

</script>