﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Text;

public partial class files_admin_new_resource : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdPlatformID.Value = rbm.getPlatformID();
        var obj = rbm.Platform.getData();
        hdPlatform.Value = obj.platform;
        hdPlatformUrl.Value = obj.url;
    }

    protected string getUser() {
        try
        {
            var user = (rbm.member.stMembers)Session["user"];

            return user.id;
        }
        catch (Exception)
        {
            Response.Redirect("/admin");

        }
        return "0";

    }

}