﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="admin_faqs.ascx.cs" Inherits="files_admin_admin_faqs" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<inc:module runat="server" ID="login_check"></inc:module>
<inc:module runat="server" ID="script">
    <script src="/js/ckeditor/ckeditor.js"></script>
</inc:module>
<asp:Label ID="meta_title" runat="server" Text="FAQ" Visible="false" />
<div data-ng-controller="AdminFaqController"></div>
