﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="buckets.ascx.cs" Inherits="files_admin_themes" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<inc:module runat="server" ID="login_check"></inc:module>
<asp:Label ID="meta_title" runat="server" Text="Buckets" Visible="false" />
    <asp:Panel ID="pnlView" runat="server">

        <div class="OneColTitle">
            <h1>Bucket Manager <img src="/images/bg_title.png" class="title_decor" /> <asp:LinkButton ID="lnkAddCat" runat="server" OnClick="lnkAddCat_Click" CssClass="btn btn-default pull-right"><i class="fa fa-plus-square"></i>Add New</asp:LinkButton></h1>
            <div class="clearboth">&nbsp;</div>
        </div>

        <asp:GridView ID="lstBucket" runat="server" AllowPaging="true" PageSize="25" AllowSorting="true" OnPageIndexChanging="lstView_PageIndexChanging"
             OnSorting="lstBucket_Sorting" AutoGenerateColumns="false" CssClass="table table-hover table-custom">
            <Columns>
                <asp:BoundField HeaderText="TITLE" DataField="title" SortExpression="title" />
                <asp:BoundField HeaderText="CATEGORY" DataField="category" SortExpression="category" />
                <asp:BoundField HeaderText="BUCKET TYPE" DataField="bucketType" SortExpression="bucketType" />
                <asp:TemplateField HeaderText="STATUS">
                    <ItemTemplate>
                        <%# Convert.ToString(Eval("active"))=="1" ? "Active" : "Disabled" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEditBucket" runat="server" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary btn-xs" OnClick="lnkEditBucket_Click"> <i class="fa fa-pencil"></i> </asp:LinkButton>
                        <asp:LinkButton ID="lnkDeleteBucket" runat="server" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-danger btn-xs" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" OnClick="lnkDeleteBucket_Click" Visible='<%# Convert.ToString(Eval("active"))=="1" ? true : false %>'> <i class="fa fa-trash"></i> </asp:LinkButton>
                        <asp:LinkButton ID="lnkEnableBucket" runat="server" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-success btn-xs"  OnClick="lnkEnableBucket_Click" Visible='<%# Convert.ToString(Eval("active"))=="1" ? false : true%>'> <i class="fa fa-check"></i> </asp:LinkButton>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
        <asp:HiddenField ID="hdID" runat="server" />
        <asp:HiddenField ID="hdBucketTitle" runat="server" />
         <div class="OneColTitle">
            <h1>Bucket Manager <img src="/images/bg_title.png" class="title_decor" /></h1>
            <div class="clearboth">&nbsp;</div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <asp:Label ID="lblTitle" runat="server" Text="Title" AssociatedControlID="txtTitle" CssClass="control-label"></asp:Label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <asp:Label ID="lblBucketTypeID" runat="server" Text="Bucket Type" AssociatedControlID="ddlBucketType" CssClass="control-label"></asp:Label>
                    <asp:DropDownList ID="ddlBucketType" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBucketType_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:Label ID="lblCategory" runat="server" Text="Category" AssociatedControlID="ddlCategory" CssClass="control-label"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlCategory" CssClass="form-control">

                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:Label ID="lblDescription" runat="server" Text="Bucket Description" AssociatedControlID="txtDescription" CssClass="control-label"></asp:Label>
                    <asp:textbox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:textbox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:Label ID="lblTools" runat="server" Text="Tool Options - (Admins will have the selected options available when adding resources to this bucket.)" AssociatedControlID="cblTools" CssClass="control-label"></asp:Label>
                    <div>
                        <asp:CheckboxList ID="cblTools" runat="server" CssClass="checkbox-inline"></asp:CheckboxList>
                    </div>
                </div>
            </div>
        </div>

        <div class="MarginTopThirty">
            <div class="pull-right">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancel_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSave_Click" />
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>

    </asp:Panel>

<script>
    function UserDeleteConfirmation() {
        return confirm("Are you sure you want to disable this bucket?");
    }

</script>