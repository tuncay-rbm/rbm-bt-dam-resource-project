﻿using System;
using System.Data;
using System.IO;
using System.ServiceModel.Activities;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_platforms : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindData();
        }
        else {
            if (!rbm.isNull(rbm._get("action"))) {
                Response.Write(rbm._get("action"));
                Response.End();
            }
        }
    }
	private void bindData() {
        //gridview
		lstView.DataSource = Session["TaskTable"] = rbm.Platform.list().Tables[0];
        lstView.DataBind();
    }
	private void bindPages(string platformID)
	{
		ViewState["platformID"] = platformID;
		lstPages.DataSource = rbm.Platform.pageList(platformID);
		lstPages.DataBind();
    }
	private void clearForm()
	{
        hdID.Value = "0";
        txtPlatform.Text = txtPath.Text = txtURL.Text = lnkCss.Text = "";
		ddlFresh.SelectedIndex = -1;
	}
	private void toggle(string panel)
    {
        if(panel=="view")
        {
            pnlEdit.Visible = false;
            pnlView.Visible = true;
        }
        else if(panel=="edit")
        {
            pnlEdit.Visible = true;
            pnlView.Visible = false;
        }
    }
    protected void lnkAddCat_Click(object sender, EventArgs e)
    {
	    clearForm();
	    pnlPages.Visible = false;
        toggle("edit");
    }
    protected void lnkEditBucket_Click(object sender, EventArgs e)
    {
		editPlatform(rbm.getCommandArgument(sender));
    }
	private void editPlatform(string platformID)
	{
		var obj = rbm.Platform.getData(platformID);
		hdID.Value = obj.id;
		txtPlatform.Text = obj.platform;
		ddlFresh.SelectedValue = obj.new_item;
		txtPath.Text = obj.path;
		txtURL.Text = obj.url;
        txtContactUs.Text = obj.contact_link;
        txtHeaderText.Text = obj.header_text;
		string cssPath = "/css/" + obj.id + "-" + obj.path + ".css";
		lnkCss.Text = "<a href='" + cssPath + "' target='_blank'>" + cssPath + "</a>";
		if (!File.Exists(cssPath))
			ltWarning.Text = "<i class='glyphicon glyphicon-exclamation-sign' style='color: red'>File not exist!</i> ";
		pnlPages.Visible = true;
		bindPages(platformID);
		lblPagesHeader.InnerText = "Pages for \"" + obj.platform + "\"";
		toggle("edit");
	}
	protected void lnkDeleteBucket_Click(object sender, EventArgs e)
    {
        rbm.Platform.delete(rbm.getCommandArgument(sender));
        bindData();
    }
    protected void btnCancelTag_Click(object sender, EventArgs e)
    {
        toggle("view");
        clearForm();
    }
    protected void btnSaveTag_Click(object sender, EventArgs e)
    {
	    bool _new = false;
        var obj = new rbm.Platform.stPlatform
        {
            id = hdID.Value,
            platform = txtPlatform.Text,
            path = txtPath.Text,
            url = txtURL.Text,
            new_item = ddlFresh.SelectedValue,
            contact_link = txtContactUs.Text,
            header_text = txtHeaderText.Text
        };

	    if (obj.id == "0")
	    {
		    obj.id = rbm.Platform.add(obj);
		    _new = true;
	    }
	    else
		    rbm.Platform.update(obj);

	    if (flCss.HasFile)
	    {
			if (flCss.FileName.Contains(".css"))
				flCss.SaveAs(Server.MapPath("/css/" + obj.id + "-" + obj.path + ".css"));
	    }
		
		if (flLogo.HasFile)
	    {
			if (flLogo.FileName.Contains(".jpg") || flLogo.FileName.Contains(".gif") || flLogo.FileName.Contains(".png"))
				flLogo.SaveAs(Server.MapPath("/images/logos/" + obj.id + "-" + obj.path + ".jpg"));
	    }
	    clearForm();
        toggle("view");
        bindData();
        if (_new)
        {
            try
            {
                DirectoryCopy(Server.MapPath("/_source_code/default"), Server.MapPath("/platforms/" + obj.id + "-" + obj.path + "/default/"));
            }
            catch (Exception)
            {

            }
            editPlatform(obj.id);
        }
        Response.Redirect(Request.RawUrl);

    }
    private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true)
    {
        try
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        catch (Exception)
        {

            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }


    }
    protected void lstView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = Session["TaskTable"] as DataTable;
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
        }
        lstView.DataSource = Session["TaskTable"];
        lstView.DataBind();
    }
    private string GetSortDirection(string column)
    {
        string sortDirection = "ASC";
        string sortExpression = ViewState["SortExpression"] as string;
        if (sortExpression != null)
        {
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = column;
        return sortDirection;
    }
    protected void lstView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        //gridview
        Session["TaskTable"] = rbm.Platform.list().Tables[0];
        lstView.DataSource = Session["TaskTable"];
        //lstView.PageIndex = e.NewPageIndex;
        lstView.DataBind();
        
    }
	protected string getStatus(object status)
	{
		return rbm.toStr(status) == "1" ? "Yes" : "No";
	}
	protected void lnkEditPage_OnClick(object sender, EventArgs e)
	{
		hdPageID.Value = rbm.getCommandArgument(sender);
		rbm.Platform.stPage obj = rbm.Platform.getPage(hdPageID.Value);
		txtPageTitle.Text = obj.title;
		lstStatus.SelectedValue = obj.status;
		lstStatus.Enabled = obj.isHideable;
		if (obj.isContentEditable)
		{
			ckContent.Visible = true;
			ckContent.Text = obj.html;
		}
		else
		{
			lblNotEditable.Visible = true;
			ckContent.Visible = false;
		}

		pnlEditPage.Visible = true;
		pnlEditPage.GroupingText = "Edit \"" + obj.title + "\" Page";
		Page.ClientScript.RegisterStartupScript(this.GetType(), "hash", "location.hash = '#editPage';", true);
	}
	protected void btnCancelPage_OnClick(object sender, EventArgs e)
	{
		cancelPage();
	}
	private void cancelPage()
	{
		hdPageID.Value = "";
		txtPageTitle.Text = ckContent.Text = "";
		lstStatus.SelectedIndex = -1;
		pnlEditPage.Visible = false;

		Page.ClientScript.RegisterStartupScript(this.GetType(), "hash", "location.hash = '#pageList';", true);
	}
	protected void btnSavePage_OnClick(object sender, EventArgs e)
	{
		rbm.Platform.stPage obj = new rbm.Platform.stPage
		{
			id = hdPageID.Value, title = txtPageTitle.Text
		};

		if (lstStatus.Enabled)
			obj.status = lstStatus.SelectedValue;

		if (ckContent.Visible)
			obj.html = ckContent.Text;

		rbm.Platform.updatePage(obj);
		bindPages(rbm.toStr(ViewState["platformID"]));
		cancelPage();
        
	}
}