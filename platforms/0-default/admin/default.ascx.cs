﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_login : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        rbm.member.stMembers user = rbm.member.login(txtUsername.Text, txtPassword.Text);
        if (user.id != "0")
        {
            Session["user"] = user;
            Response.Redirect(!rbm.isNull(rbm._get("returnUrl")) ? rbm._get("returnUrl") : "/"+rbm.getPlatformName());
        }
        else
        { lblError.Visible = true; }
    }
    protected void lnkRemember_Click(object sender, EventArgs e)
    {
        pnlRemember.Visible = true;
    }
    protected void btnRemember_Click(object sender, EventArgs e)
    {
        if (!rbm.member.isExist(txtEmail.Text))
        {
            lblError.Visible = true;
            lblRemember.Text = "This user was not found.";
        }
        else
        {
            buildMessage(txtEmail.Text);
            lblError.Visible = true;
            lblRemember.Text = "The password was sent to the email provided.";
        }
    }

    protected void buildMessage(string email)
    {
        rbm.member.stMembers obj = rbm.member.getMemberByEmail(txtEmail.Text);
        string password = obj.password;


        try
        {
            //create the mail message
            MailMessage mail = new MailMessage();

            //set the addresses
            mail.From = new MailAddress(txtEmail.Text, "Password Help");

            //set the content
            mail.Subject = "Password Help";

            // Plain Text part
            AlternateView plainView = AlternateView.CreateAlternateViewFromString("Your password:" + password);

            // Html part
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString("Your password: " + password);
            mail.AlternateViews.Add(plainView);
            mail.AlternateViews.Add(htmlView);

            //send the message
            SmtpClient client = new SmtpClient
            {
                Host = "",
                Port = 25,
                Credentials = new NetworkCredential("", "")
            };

            //recipient address
            mail.To.Add(new MailAddress(txtEmail.Text));

            //send
            client.Send(mail);
        }
        catch (Exception)
        {
            lblRemember.Text = "Email sending failed, please try again later<br />"; lblError.Visible = true;
        }
        finally
        {

        }
    }
}