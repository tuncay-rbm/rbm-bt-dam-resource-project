﻿<%@ Control Language="C#" ClassName="footer" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

	</div>
	<!-- CONTENTWRAPPER ENDS -->
</div>
<!-- CONTENT AREA ENDS -->


<!-- FOOTER STARTS -->
<footer>
	<div class="CntWrapper">
		<div class="col-md-6">
			<a href="http://www.baker-taylor.com/" title="Baker and Taylor" target="_blank"><img src="/images/logo_BandT.png" alt="Baker and Taylor" /></a>
		</div>
		<div class="col-md-6" align="right">
			<p>&copy; 2015. Baker &amp; Taylor. All Rights Reserved.</p>
			<strong><a href="http://goo.gl/pvjnqm" title="Contact Us" target="_blank">Contact Us For More Information</a></strong>
		</div>
		<div class="clearboth">&nbsp;</div>
	</div>
</footer>
<!-- FOOTER ENDS -->