﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_themes : System.Web.UI.UserControl
{
    string platformName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string refer = HttpContext.Current.Request.UrlReferrer.ToString();
            if (!rbm.isNull(refer))
            {
                string key = "platformName=";
                if (refer.Contains(key))
                {
                    int p1 = refer.IndexOf(key) + key.Length;
                    platformName = refer.Substring(p1, refer.Length - p1);
                }
                else
                {
                    Uri url = new Uri(refer);
                    platformName = url.Segments[1];
                }
            }
            bindData();
        }
    }

    protected void bindData()
    {
        lstThemes.DataSource = rbm.Tags.getThemes(rbm.getPlatformID());
        lstThemes.DataBind();
    }


    protected void lstMembers_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        lstThemes.PageIndex = e.NewPageIndex;
        bindData();
    }


    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        rbm.Tags.deleteTheme(rbm.getCommandArgument(sender), rbm.getPlatformID());
        bindData();
    }
}