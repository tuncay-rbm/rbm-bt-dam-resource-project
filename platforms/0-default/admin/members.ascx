﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="members.ascx.cs" Inherits="files_admin_members" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<inc:module runat="server" ID="login_check"></inc:module>

    <asp:Panel ID="pnlAdmin" runat="server">

	<div class="OneColTitle">
        <h1>DAM Users <img src="/images/bg_title.png" class="title_decor" /> <asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click" CssClass="btn btn-default pull-right"><i class="fa fa-plus-square"></i>Add New Member</asp:LinkButton></h1>
        <div class="clearboth">&nbsp;</div>
    </div>
	
        <asp:GridView ID="lstMembers" runat="server" CssClass="table table-hover table-custom" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnPageIndexChanging="lstMembers_PageIndexChanging">
	        <RowStyle CssClass="odd"></RowStyle>
	        <AlternatingRowStyle CssClass="even"></AlternatingRowStyle>
			<PagerSettings Mode="Numeric"></PagerSettings>
            <Columns>
	            <asp:BoundField HeaderText="Name" DataField="name"  />
	            <asp:BoundField HeaderText="Email" DataField="email" />
				<asp:TemplateField HeaderText="Status">
					<ItemTemplate><%# Eval("status").ToString() == "1" ? "Active" : "Disabled" %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Admin">
					<ItemTemplate><%# Eval("isAdmin").ToString() == "1" ? "Yes" : "No" %></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton ID="lnkEdit" CssClass="btn btn-primary btn-xs" runat="server" CommandArgument='<%# Eval("id") %>' OnClick="lnkEdit_Click"> <i class="fa fa-pencil"></i> </asp:LinkButton>
						<asp:LinkButton ID="lnkDelete" CssClass="btn btn-danger btn-xs" runat="server" CommandArgument='<%# Eval("id") %>' OnClick="lnkDelete_Click" OnClientClick="return confirm('Are you sure?')"> <i class="fa fa-trash"></i> </asp:LinkButton>
					</ItemTemplate>
				</asp:TemplateField>
            </Columns>
        </asp:GridView>
		
    </asp:Panel>

    <asp:Panel ID="pnlEdit" runat="server" Visible="false" CssClass="form">
        <asp:HiddenField ID="txtId" runat="server" />
		<div class="OneColTitle">
	        <h1>Edit User <img src="/images/bg_title.png" class="title_decor" /></h1>
	        <div class="clearboth">&nbsp;</div>
	    </div>
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="form-group">
	    			<asp:Label ID="lblName" runat="server" Text="Name" AssociatedControlID="txtName"></asp:Label>
	    			<asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
	    		</div>
	    		<div class="form-group">
	    			<asp:Label ID="lblPassword" runat="server" Text="Password" AssociatedControlID="txtPassword"></asp:Label>
	    			<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"></asp:TextBox>
	    		</div>
	    	</div>
	    	<div class="col-md-6">
	    		<div class="form-group">
	    			<asp:Label ID="lblEmail" runat="server" Text="Email" AssociatedControlID="txtEmail"></asp:Label>
	    			<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
	    		</div>
	    	</div>
	    	<div class="clearboth">&nbsp;</div>
	    </div>
	    <div class="clearboth">&nbsp;</div>
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="form-group">
	    			<asp:Label ID="lblStatus" runat="server" Text="Status" AssociatedControlID="lstStatus"></asp:Label>
		            <div class="radio_opt">
		                <asp:RadioButtonList ID="lstStatus" runat="server" CssClass="radio-inline">
		                    <asp:ListItem Value="0" Text="Disabled" Selected="True"></asp:ListItem>
		                    <asp:listItem Value="1" Text="Active"></asp:listItem>
		                </asp:RadioButtonList>
		            </div>
	    		</div>
	    	</div>
	    	<div class="col-md-6">
	    		<div class="form-group">
	    			<asp:Label ID="lblAdmin" runat="server" Text="Is Administrator" AssociatedControlID="lstAdmin"></asp:Label>
		            <div class="radio_opt">
		                <asp:RadioButtonList ID="lstAdmin" runat="server" CssClass="radio-inline">
		                    <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
		                    <asp:listItem Value="1" Text="Yes"></asp:listItem>
		                </asp:RadioButtonList>
		            </div>
	    		</div>
	    	</div>
	    </div>

        <div class="MarginTopThirty">
            <div class="pull-right">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancel_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSave_Click" />
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>
		
    </asp:Panel>