﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="new_resource.ascx.cs" Inherits="files_admin_new_resource" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<inc:module ID="Script" runat="server">
    <script type="text/javascript" src="/js/controllers/NewResourceController.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.min.js"></script>
    <script type="text/javascript" src="/js/directives/ngClip.js"></script>
</inc:module>

<inc:module ID="style" runat="server">
    <link rel="stylesheet" href="/styles/ng-tags-input.min.css" />
</inc:module>
<inc:module runat="server" ID="login_check"></inc:module>

<div id="admin_resource" data-ng-controller="NewResourceController">
    <asp:HiddenField ID="hdPlatform" runat="server" />
    <asp:HiddenField ID="hdPlatformID" runat="server" />
    <asp:HiddenField ID="hdPlatformUrl" runat="server" />
    <!-- SIDEBAR START -->
    <div id="sidebar" class="col-md-3">  

    </div>
    <!-- SIDEBAR ENDS -->

      <!-- PAGE CONTENT STARTS -->
      <div id="PgCnt" class="col-md-9">
          <h1>Administration <img src="/images/bg_title.png" class="title_decor" /></h1>
          <input type="hidden" data-ng-model="userID" data-ng-init="setUserID('<%= getUser().ToString() %>')" />
          <div class="form-horizontal MarginTopThirty">
              <div class="form-group" data-ng-show="false">
                  <label id="lblPlatform" for="ddlPlatform" class="col-sm-2 control-label">PLATFORM</label>
                  <div class="col-sm-10">
                      <select id="ddlPlatform" class="form-control" data-ng-disabled="true">
                          <option>AXIS 360</option>
                      </select>
                  </div>
              </div>
              <div class="form-group" data-ng-if="hasPlatform">
                  <label for="ddlBucket" class="col-sm-2 control-label">BUCKET</label>
                  <div class="col-sm-10">
                      <select id="ddlBucket" class="form-control" data-ng-model="selectedBucket.bucket" data-ng-options="opt.title for opt in buckets track by opt.id" data-ng-change="watchBucket()"></select>
                  </div>
              </div>
              <div class="form-group" data-ng-if="hasBucket">
                  <label for="txtTitle" class="col-sm-2 control-label">TITLE</label>
                  <div class="col-sm-10">
                      <input type="text" id="txtTitle" class="form-control" data-ng-model="selectedAsset.title" />
                  </div>
              </div>
              <div class="form-group" data-ng-if="hasBucket">
                  <label for="txtDescription" class="col-sm-2 control-label">DESCRIPTION</label>
                  <div class="col-sm-10">
                      <textarea id="txtDescription" class="form-control" rows="3" data-ng-model="selectedAsset.description"></textarea>
                  </div>
              </div>
          <!-- UPLOADER -->
          <div data-ng-if="hasBucket && selectedBucket.bucket.bucketTypeID != 1">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
              <div class="col-md-7 NoPadLeft">
                <div class="form-group" ng-repeat="item in uploader.queue">
                    <label for="fileNameSd" class="form-label">Name:</label> 
                    <span id="fileNameSd" ng-bind="item.file.name"></span><br/>
                    <label for="fileSizeSd" class="form-label">Size:</label> 
                    <span id="fileSizeSd" ng-bind="item.file.size"></span><br/>
                    <a class="btn btn-primary btn-sm" ng-click="uploader.clearQueue()">Cancel</a>
                    <a class="btn btn-primary btn-sm" ng-click="item.upload()">Upload File</a>
                </div>
              </div>
              <div class="col-md-5 NoPadRight">
                <div class="pull-right">
                  <label class="btn btn-default btn-sm" for="my-file-sd">BROWSE FILES
                      <input id="my-file-sd" style="display:none"  type="file" nv-file-select uploader="uploader" ng-click="uploader.clearQueue()"/><br/>
                  </label>
                </div>
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
            <div class="clearboth">&nbsp;</div>
          </div>
          <!-- END UPLOADER -->
  
          <!-- VIDEO TYPE -->
          <div class="form-group" data-ng-if="hasBucket && selectedBucket.bucket.bucketTypeID == 1">
              <label class="col-sm-2 control-label">VIDEO LINK</label>
              <div class="col-sm-10">
                  <input type="text" id="txtVideoLink" class="form-control" data-ng-model="selectedAsset.link" />
              </div>
          </div>
          <!-- <div class="form-group" data-ng-if="hasBucket && selectedBucket.bucket.bucketTypeID == 1">
              <label  class="col-sm-2 control-label">PREVIEW</label>
              <div class="col-sm-10">
                  <iframe width="512" height="288" src="{{trustedLink}}"  frameborder="0" allowfullscreen></iframe> 
              </div>
          </div> -->
          <!--END VIDEO TYPE -->
  
          <!-- BEGIN TOOLS * No tools available for document type buckets-->
          <div>
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
              <div class="col-md-8 NoPadLeft">
                <div class="form-group XtraOptions" data-ng-if="(hasFile ||selectedBucket.bucket.bucketTypeID==1)">
                    <label ID="lblFileInfo1" data-ng-if="hasFile && selectedBucket.bucket.bucketTypeID!=1" class="current_file">Filename: {{fileData.filename}}</label>   <br />
                    <label ID="lblFileInfo2" data-ng-if="hasFile && selectedBucket.bucket.bucketTypeID!=1"   class="current_file">Size: {{fileData.filesize}}</label>
                    <label ID="lblFileInfo3" data-ng-if="hasFile && selectedBucket.bucket.bucketTypeID==2"   class="current_file">Width: {{fileData.width}} Height:{{fileData.height}}</label>   
      
                    <div  data-ng-repeat="tool in resourceTools" data-ng-if="selectedBucket.bucket.bucketTypeID!=3" class="MarginBottom">
                        <input id="cb_{{$index}}" type="checkbox" data-ng-model="tool.selected" data-ng-change="watchToolSelection(tool)"> 
                        <label for="cb_{{$index}}" class="custom-label"><span></span>{{tool.title}}</label>
                        <!-- VIDEO OPTIONS -->
                        <div data-ng-if="selectedBucket.bucket.bucketTypeID==1 && tool.toolID==1">
                            <input type="number" min="0" id="videoWidth_{{$index}}" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setVideoHeight($index)" />
                            <span class="size_option">X</span>
                            <input type="number" min="0" id="videoHeight_{{$index}}" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setVideoWidth($index)" />
                            <span class="size_options">pixels</span>
                        </div>
                            <div data-ng-if="selectedBucket.bucket.bucketTypeID==1 && tool.toolID==2">
                            <input type="number" min="0" id="videoWidth_{{$index}}" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setVideoHeight($index)" />
                            <span class="size_option">X</span>
                            <input type="number" min="0" id="videoHeight_{{$index}}" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setVideoWidth($index)" />
                            <span class="size_options">pixels</span>
                        </div>
                        <div data-ng-if="selectedBucket.bucket.bucketTypeID==1 && tool.toolID==3">
                            <input type="number" min="0" id="videoWidth_{{$index}}" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setVideoHeight($index)" />
                            <span class="size_option">X</span>
                            <input type="number" min="0" id="videoHeight_{{$index}}" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setVideoWidth($index)" />
                            <span class="size_options">pixels</span>
                        </div>
                        <div data-ng-if="selectedBucket.bucket.bucketTypeID==1 && tool.toolID==12">
                            <input type="number" min="0" id="videoWidth_{{$index}}" class="current_file_width custom-input" data-ng-model="tool.width"  data-ng-change="setVideoHeight($index)" />
                            <span class="size_option">X</span>
                            <input type="number" min="0" id="videoHeight_{{$index}}" class="current_file_height custom-input" data-ng-model="tool.height"  data-ng-change="setVideoWidth($index)" />
                            <span class="size_options">pixels</span>
                        </div>
                        <!-- PRINT MEDIA OPTIONS -->
                        <div data-ng-if="(selectedBucket.bucket.bucketTypeID==4)&&(tool.toolID==19 || tool.toolID==20 || tool.toolID==21)">
                            <input type="number" min="0" id="printMediaWidth" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setPrintMediaHeight($index)" /> % of actual size.
                        </div>
                        <!-- IMAGE OPTIONS -->
                        <div data-ng-if="(selectedBucket.bucket.bucketTypeID==2)&&(tool.toolID==13 || tool.toolID==14 || tool.toolID==15 || tool.toolID==16)">
                            <label for="imageWidth" class="custom-label">Width</label>
                            <input type="text" id="imageWidth" class="current_file_width custom-input" data-ng-model="tool.width" data-ng-change="setImageHeight($index)" />
                            <span class="size_option">X</span>
                            <label for="imageHeight" class="custom-label">Height</label>=
                            <input type="text" id="imageHeight" class="current_file_height custom-input" data-ng-model="tool.height" data-ng-change="setImageWidth($index)" />
                            <span class="size_options">pixels</span>
                        </div>
                        <div data-ng-if="buttonWarning && (tool.toolID==17 || tool.toolID==18 || tool.toolID==22 || tool.toolID==23)" class="bg-warning">
                            Output option is required for Image or Print Media types.
                        </div>
                    </div>
                </div>
                <!-- END TOOLS -->
              </div>
              <div class="col-md-4 preview NoPadRight ">
                <!-- IMAGE OR PRINT TYPE -->
                <div data-ng-if="selectedBucket.bucket.bucketTypeID==3">
                    <div class="image_preview">
                        DOCUMENT ICON HERE               
                    </div>
                </div>
                <div data-ng-if="hasFile && (selectedBucket.bucket.bucketTypeID==2||selectedBucket.bucket.bucketTypeID==3)">
                    <div class="image_preview">
                        <img id="litImagePreview" alt="{{selectedAsset.title}}" src="{{selectedAsset.path}}" />                  
                    </div>
                </div>
                <!--END IMAGE OR PRINT TYPE -->
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
            <div class="clearboth">&nbsp;</div>
          </div>
  
          <!-- BEGIN THEMES -->
          <div class="form-group" data-ng-if="(hasFile ||selectedBucket.bucket.bucketTypeID==1)">
              <div class="image_themes">
                  <label id="lblTags" for="txtTags" class="col-sm-2 control-label">THEMES</label>
                  <div class="col-sm-10">

                    Press 'ESC' to enter a new theme.
                            <tags-input  ng-model="tags" display-property="text" addFromAutocompleteOnly="false">
                                <auto-complete source="loadedTags"></auto-complete>
                            </tags-input>
                  </div>
              </div>
          </div>
          <!-- END THEMES -->
  
          <!-- BEGIN FAQ -->
          <div class="form-group" data-ng-if="(hasFile ||selectedBucket.bucket.bucketTypeID==1)">
              <label class="col-sm-2 control-label">Related FAQ</label>
              <div class="col-sm-10">
                  <div data-ng-repeat="question in resourceQuestions" class="form-cnt">
                      {{question.question}}
                      <a data-ng-click="removeFAQ($index)"><span class="glyphicon glyphicon-remove-circle remove-icon"></span></a>
              </div>
          </div>
      </div>
      <div class="form-group" data-ng-if="(hasFile ||selectedBucket.bucket.bucketTypeID==1)">
          <label for="ddlFaq" id="lblFaq" class="col-sm-2 control-label">Add Custom FAQ</label>
          <div class="col-sm-10">
              <select ID="ddlFaq" class="form-control" data-ng-model="includedQuestion.opt" data-ng-options="idx as opt.question for (idx,opt) in allQuestions" data-ng-change="addQuestion()"></select>
          </div>
      </div>
  
      <div class="form-group" data-ng-if="(hasFile ||selectedBucket.bucket.bucketTypeID==1)">
          <div class="col-sm-offset-2 col-sm-10">
              <div class="pull-right">
                  <a id="btnCancel" class="btn btn-primary" href="/">Cancel</a>
                  <a id="btnSave" class="btn btn-default" data-ng-click="saveEdits()">Save</a>
                    <div data-ng-if="saveMessage" class="bg-success">
                        Saving...
                    </div>
              </div>
              <div class="clearboth">&nbsp;</div>
          </div>
      </div>
  </div>
  </div>
<div class="clearboth">&nbsp;</div>

</div>
<!-- PAGE CONTENT ENDS -->
