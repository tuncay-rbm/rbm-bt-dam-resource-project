﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="stats.ascx.cs" Inherits="files_admin_stats" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<%--<inc:module runat="server" ID="login_check"></inc:module>--%>
<asp:Label ID="meta_title" runat="server" Text="STATS" Visible="false" />
<inc:module ID="Script" runat="server">
    <script type="text/javascript" src="/js/controllers/StatsController.js"></script>

</inc:module>

<div class="container" ng-controller="StatsController">
	    <div class="OneColTitle">
            <asp:HiddenField ID="hdPlatform" runat="server" />
            <asp:HiddenField ID="hdPlatformID" runat="server" />
		        <h1>STATS
				        <img src="/images/bg_title.png" class="title_decor" />
		        </h1>
		    <div class="clearboth">&nbsp;</div>
	    </div>
    <div class="container">
    <div class="row">
        <div class="form-group">
            <div class="col-md-8">
                <input id="searchBox" type="text" class="form-control" placeholder="Search Stats..." ng-model="keyword" ng-change="search()" />
            </div>
            <div class="col-md-4">
                <a data-ng-click="clearFilters()" class="btn btn-default">Clear Filters</a>

            </div>
        </div>
    </div>
    <div class="row">
    	<div class="form-group">
			<div class="form-group col-md-3 col-sm-12 col-xs-6 select-month">
				<select class="form-control" id="select_list" ng-options="month.text for month in months" ng-model="filterMonth"></select>
			</div>

			<div class="form-group col-md-3 col-sm-12 col-xs-6 select-year">
				<select class="form-control" id="select_list" ng-options="year.text for year in years" ng-model="filterYear"></select>
			</div>
                                
			<div class="form-group col-md-3 col-sm-12 col-xs-6 select-year">
				<a class="btn btn-default col-sm-12 col-xs-12 col-md-12" ng-click="filterByDate()">FILTER BY DATE</a>
			</div>
            <div class="form-group col-md-3 col-sm-12 col-xs-6 select-year">
    			<span ng-if="filterErr" class="label label-warning">Please select a valid date!</span>
            </div>
		</div>

    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-12">
                <asp:LinkButton runat="server" ID="lnkExport" OnClick="lnkExport_Click" CssClass="bnt btn-default btn-lg">Export Stats</asp:LinkButton>
            </div>
        </div>
    </div>
    </div>

        <table class="table table-hover table-custom" >
            <thead>
                <th ng-click="sort('state')" align="center">STATE<i ng-class="{'icon-chevron-up': isSortUp('state'), 'icon-chevrondown':isSorthown('state')}"></i</th>
                <th ng-click="sort('city')" align="center">CITY<i ng-class="{'icon-chevron-up': isSortUp('city'), 'icon-chevrondown':isSorthown('city')}"></i</th>
                <th ng-click="sort('title')" align="center">PRODUCT<i ng-class="{'icon-chevron-up': isSortUp('title'), 'icon-chevrondown':isSorthown('title')}"></i</th>
                <th ng-click="sort('cnt')" align="center">DOWNLOADS<i ng-class="{'icon-chevron-up': isSortUp('cnt'), 'icon-chevrondown':isSorthown('cnt')}"></i></th>
                <th ng-click="sort('saveDate')" align="center">UPLOADED ON:<i ng-class="{'icon-chevron-up': isSortUp('saveDate'), 'icon-chevrondown':isSorthown('saveDate')}"></i></th>

                <th ng-click="sort('statDate')" align="center">LAST DOWNLOADED ON:<i ng-class="{'icon-chevron-up': isSortUp('statDate'), 'icon-chevrondown':isSorthown('statDate')}"></i></th>

            </thead>
            <tr ng-repeat="stat in pagedItems[currentPage-1] | orderBy:sortField:reverse">
                <td>
                    <span ng-if="stat.state=='-'">Unknown</span>
                    <span ng-if="stat.state!='-'">{{stat.state}}</span>
                </td>
                <td>
                    <span ng-if="stat.city=='-'">Unknown</span>
                    <span ng-if="stat.city!='-'">{{stat.city}}</span>
                </td>
                <td>
                    <span ng-if="stat.title===null">Unknown</span>
                    <span ng-if="stat.title!=null">{{stat.title}}</span>
                </td>
                <td>{{stat.cnt}}</td>
                <td>{{stat.saveDate | date: 'mediumDate'}}</td>

                <td>{{stat.statDate | date: 'mediumDate'}}</td>
            </tr>
		</table>
    <div class="row">
        			<div class="col-2">
				<a class="btn btn-default" ng-if="vsPrevPage" ng-click="prevPage()">
					PREVIOUS PAGE
				</a>
			</div>

			<div class="col-4 col-md-offset-1">

						<label class="control-label" for="select_list">Jump To Page</label>
						<select class="form-control" id="select_list" ng-model="lstPages" ng-options="page.text for page in pages" ng-change="selectPage()"></select>

			</div>

			<div class="col-2">
				<a class="btn btn-default" ng-if="vsNextPage" ng-click="nextPage()">
                    NEXT PAGE
				</a>
			</div>
    </div>
    </div>



