﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="themes.ascx.cs" Inherits="files_admin_themes" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<inc:module runat="server" ID="login_check"></inc:module>
<asp:Label ID="meta_title" runat="server" Text="THEMES" Visible="false" />

        <asp:GridView ID="lstThemes" runat="server" CssClass="table table-hover table-custom" AutoGenerateColumns="False">
	        <RowStyle CssClass="odd"></RowStyle>
	        <AlternatingRowStyle CssClass="even"></AlternatingRowStyle>
			<PagerSettings Mode="Numeric"></PagerSettings>
            <Columns>
	            <asp:BoundField HeaderText="Theme" DataField="theme"  />
				<asp:TemplateField HeaderText="Count">
					<ItemTemplate><%# Convert.ToString(Eval("rating")) + " item(s) have this theme." %></ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton ID="lnkDelete" CssClass="btn btn-danger btn-xs" runat="server" CommandArgument='<%# Eval("theme") %>' OnClick="lnkDelete_Click" OnClientClick="return confirm('Are you sure?')"> <i class="fa fa-trash"></i> </asp:LinkButton>
					</ItemTemplate>
				</asp:TemplateField>
            </Columns>
        </asp:GridView>

