﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="faqs.ascx.cs" Inherits="files_admin_faqs" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<inc:module runat="server" ID="login_check"></inc:module>
<inc:module runat="server" ID="script">
    <script src="/js/ckeditor/ckeditor.js"></script>
</inc:module>
<asp:Label ID="meta_title" runat="server" Text="FAQ" Visible="false" />
    <asp:Panel ID="pnlView" runat="server">

        <div class="OneColTitle">
            <h1>Manage FAQs <img src="/images/bg_title.png" class="title_decor" /> <asp:LinkButton ID="lnkAddCat" runat="server" OnClick="lnkAddCat_Click" CssClass="btn btn-default pull-right"><i class="fa fa-plus-square"></i>New Solution</asp:LinkButton></h1>
            <div class="clearboth">&nbsp;</div>
        </div>
            <asp:GridView runat="server" ID="lstQuestion" AutoGenerateColumns="false"  CssClass="table table-hover table-custom" AllowSorting="true" AllowPaging="true" PageSize="10" OnPageIndexChanging="lstView_PageIndexChanging" OnSorting="lstView_Sorting">
                <Columns>
                    <asp:TemplateField HeaderText="question" SortExpression="question">
                        <ItemTemplate>
                            <%# Eval("question") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="category" SortExpression="category">
                        <ItemTemplate>
                            <%# Eval("category") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="answer" SortExpression="answer">
                        <ItemTemplate>
                            <%# Eval("answer") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEditBucket" runat="server" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary btn-xs" OnClick="lnkEditTag_Click"> <i class="fa fa-pencil"></i> </asp:LinkButton>
                            <asp:LinkButton ID="lnkDeleteBucket" runat="server" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-danger btn-xs" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" OnClick="lnkDeleteTag_Click"> <i class="fa fa-trash"></i> </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server" Visible="false">

        <div class="OneColTitle">
            <h1>Manage FAQs <img src="/images/bg_title.png" class="title_decor" /></h1>
        </div>

        <asp:HiddenField ID="hdID" runat="server" />
        <div class="MarginTopThirty">
            <div class="form-group">
                <asp:Label ID="lblQuestion" runat="server" AssociatedControlID="txtQuestion" CssClass="control-label">Question</asp:Label>
                <asp:TextBox ID="txtQuestion" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblAnswer" runat="server" AssociatedControlID="txtAnswer" CssClass="control-label">Answers</asp:Label>
                <asp:textbox TextMode="MultiLine" ID="txtAnswer" runat="server" CssClass="ckeditor"></asp:textbox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCat" runat="server" AssociatedControlID="ddlCat" CssClass="control-label">Category</asp:Label>
                <asp:DropDownList ID="ddlCat" runat="server" CssClass="form-control">

                </asp:DropDownList>
            </div>
            <div class="form-group">
                <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategorySet" CssClass="control-label">Associated Buckets (Optional)</asp:Label>
                <div>
                    <asp:CheckboxList ID="ddlCategorySet" runat="server" CssClass="checkbox-inline" ></asp:CheckboxList>
                </div>
            </div>

            <div class="MarginTopThirty">
                <div class="pull-right">
                    <asp:Button ID="btnCancelTag" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancelTag_Click" />
                    <asp:Button ID="btnSaveTag" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSaveTag_Click"/>
                </div>
                <div class="clearboth">&nbsp;</div>
            </div>
        </div>
    </asp:Panel>

<script>
    function UserDeleteConfirmation() {
        return confirm("Are you sure you want to delete this FAQ?");
    }
    //$("#accordion").accordion({
    //    collapsible: true
    //});
    //$('.lnkEdit').click(function (event) {
    //    event.preventDefault();
    //    event.stopPropagation();
    //    var id = $(this).attr('data-id');
    //    $.post('/admin/faqs?action=edit&id=' + id, function (response) {

    //    });
    //})
</script>