﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="default.ascx.cs" Inherits="files_admin_login" %>

<div class="LoginBox">
	<asp:Panel DefaultButton="btnLogin" runat="server" GroupingText="LOGIN">
		<div class="form-group">
			<label>Email:</label>
			<asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
		</div>
		<div class="form-group">
			<label>Password:</label>
			<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
		</div>
		<div class="row">
			<div class="col-md-6">
				<asp:LinkButton ID="lnkRemember" runat="server" CssClass="ForgotLink" OnClick="lnkRemember_Click">Forgot Details?</asp:LinkButton>
			</div>
			<div class="col-md-6" align="right">
				<asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-default" OnClick="btnLogin_Click" />
			</div>
			<div class="clearboth">&nbsp;</div>
		</div>
		<div class="form-group">
			<asp:Label ID="lblError" runat="server" Text="Your login or password is wrong, please try again." CssClass="error" Visible="false"></asp:Label>
		</div>
	</asp:Panel>
	<asp:Panel ID="pnlRemember" runat="server" CssClass="form-inline" Visible="false">
		<div class="form-group">
			<label>Please enter your email:</label>
	    	<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
	    	<asp:Button ID="btnRemember" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btnRemember_Click" />
	    	<asp:Label ID="lblRemember" runat="server"></asp:Label>
		</div>
	 	</asp:Panel>
</div>