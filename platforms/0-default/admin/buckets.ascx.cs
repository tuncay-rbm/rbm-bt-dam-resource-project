﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_themes : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindData();
        }
    }

    protected void bindData() {
        rbm.fill(ddlBucketType, rbm.BucketType.list(), "id", "title");
        rbm.fill(ddlCategory, rbm.Categories.listBucketCategories(rbm.getPlatformID()), "id", "title");


        Session["TaskTable"] = rbm.Bucket.listAdmin(rbm.getPlatformID()).Tables[0];
        lstBucket.DataSource = Session["TaskTable"];
        lstBucket.DataBind();
    }

    protected void clearForm(){
        cblTools.Items.Clear();
        hdID.Value = "";
        txtTitle.Text = "";
        txtDescription.Text = "";
        hdBucketTitle.Value = "";
    }

    protected void toggle(string panel)
    {
        if(panel=="view")
        {
            pnlEdit.Visible = false;
            pnlView.Visible = true;
        }
        else if(panel=="edit")
        {
            pnlEdit.Visible = true;
            pnlView.Visible = false;
        }
    }

    protected void lnkAddCat_Click(object sender, EventArgs e)
    {

        getTools(ddlBucketType.SelectedValue);
        toggle("edit");
    }

    protected void lnkEditBucket_Click(object sender, EventArgs e)
    {
        rbm.Bucket.stBucket obj = rbm.Bucket.getData(rbm.getCommandArgument(sender));
        hdID.Value = obj.id;
        hdBucketTitle.Value = obj.title;
        txtTitle.Text = obj.title;
        txtDescription.Text = obj.description;
        ddlBucketType.SelectedValue = obj.bucketTypeID;
        ddlCategory.SelectedValue = obj.categoryID;
        checkBucketTools(obj);
        toggle("edit");
    }

    protected void getTools(object bucketTypeID)
    {
        DataTable ddlTool = rbm.Bucket.ToolsByBucketType(ddlBucketType.SelectedValue).Tables[0];
        for (int i = 0; i < ddlTool.Rows.Count; i++)
        {
            ListItem item = new ListItem();
            item.Value = rbm.toStr(ddlTool.Rows[i][0]);
            item.Text = rbm.toStr(ddlTool.Rows[i][2]);
            cblTools.Items.Add(item);
        }
    }

    protected void checkBucketTools(rbm.Bucket.stBucket obj)
    {
        DataTable ddlTool = rbm.Bucket.ToolsByBucketType(ddlBucketType.SelectedValue).Tables[0];
        for (int i = 0; i < ddlTool.Rows.Count; i++)
        {
            ListItem item = new ListItem();
            item.Value = rbm.toStr(ddlTool.Rows[i][0]);
            item.Text = rbm.toStr(ddlTool.Rows[i][2]);
            item.Selected = rbm.Bucket.SelectedTools(obj.id, item.Value);
            cblTools.Items.Add(item);
        }
    }

    protected void lnkDeleteBucket_Click(object sender, EventArgs e)
    {
        rbm.Bucket.disableBucket(rbm.getCommandArgument(sender));
        bindData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        toggle("view");
        clearForm();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        rbm.Bucket.stBucket obj = new rbm.Bucket.stBucket();
        obj.id = hdID.Value;
        obj.title = txtTitle.Text;
        obj.description = txtDescription.Text;
        obj.saveDate = DateTime.Now.ToString();
        obj.bucketTypeID = ddlBucketType.SelectedValue;
        obj.categoryID = ddlCategory.SelectedValue;
        obj.platformID = rbm.getPlatformID();
        if (obj.id=="")
        {
            obj.id = rbm.Bucket.add(obj);
            
        }
        else
        {
            rbm.Bucket.update(obj);
            if (obj.title != hdBucketTitle.Value)
            {
                var platform = rbm.Platform.getData(rbm.getPlatformID());
                string sourceDir = "/_resources/" + platform.platform + "/" + hdBucketTitle.Value;
                string destDir = "/_resources/" + platform.platform + "/" + obj.title;

                try
                {
                    System.IO.Directory.Move(Server.MapPath(sourceDir), Server.MapPath(destDir));
                }
                catch (Exception)
                {

                }

            }
        }
        updateBucketTools(obj.id);
        clearForm();
        toggle("view");
        bindData();
    }

    protected void updateBucketTools(string bucketID)
    {
        rbm.Bucket.ClearBucketTools(bucketID);
        for (int i = 0; i < cblTools.Items.Count; i++)
        {
            if (cblTools.Items[i].Selected)
            {
                rbm.Bucket.AddBucketTools(bucketID, cblTools.Items[i].Value);
            }
        }
    }

    protected void lstBucket_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = Session["TaskTable"] as DataTable;
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
        }
        lstBucket.DataSource = Session["TaskTable"];
        lstBucket.DataBind();
    }

    private string GetSortDirection(string column)
    {
        string sortDirection = "ASC";
        string sortExpression = ViewState["SortExpression"] as string;
        if (sortExpression != null)
        {
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = column;
        return sortDirection;
    }

    protected void lstView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
            //gridview
        Session["TaskTable"] = rbm.Bucket.listAdmin(rbm.getPlatformID()).Tables[0];
            lstBucket.DataSource = Session["TaskTable"];
            lstBucket.PageIndex = e.NewPageIndex;
            lstBucket.DataBind();
        
    }

    protected void ddlBucketType_SelectedIndexChanged(object sender, EventArgs e)
    {
        cblTools.Items.Clear();
        getTools(ddlBucketType.SelectedValue);
    }
    protected void lnkEnableBucket_Click(object sender, EventArgs e)
    {
        rbm.Bucket.enableBucket(rbm.getCommandArgument(sender));
        bindData();
    }
}