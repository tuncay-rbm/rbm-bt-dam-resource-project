﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="platforms.ascx.cs" Inherits="files_admin_platforms" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<inc:module runat="server" ID="login_check"></inc:module>
<inc:module runat="server" ID="script">
    <script src="/js/ckeditor/ckeditor.js"></script>
</inc:module>
<asp:Label ID="meta_title" runat="server" Text="Platforms" Visible="false" />

<asp:Panel ID="pnlView" runat="server">
	<div class="OneColTitle">
		<h1>PLATFORMS
				<img src="/images/bg_title.png" class="title_decor" />
			<asp:LinkButton ID="lnkAddCat" runat="server" OnClick="lnkAddCat_Click" CssClass="btn btn-default pull-right"><i class="fa fa-plus-square"></i>New Platform</asp:LinkButton>
		</h1>
		<div class="clearboth">&nbsp;</div>
	</div>

	<div id="accordion">
		<asp:GridView runat="server" ID="lstView" AutoGenerateColumns="false" CssClass="table table-hover table-custom" AllowPaging="true" PageSize="10" OnPageIndexChanging="lstView_PageIndexChanging" OnSorting="lstView_Sorting">
			<Columns>
				<asp:TemplateField HeaderText="Platform">
					<ItemTemplate>
						<%# Eval("platform") %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="URL">
					<ItemTemplate>
						<a href="<%# Eval("url") %>" target="_blank"><%# Eval("url") %></a>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Manage Platform">
					<ItemTemplate>
						<a href="<%# Eval("url") %>" target="_blank">Manage Now</a>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Edit">
					<ItemTemplate>
						<asp:LinkButton ID="lnkEditBucket" runat="server" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary btn-xs" OnClick="lnkEditBucket_Click"> <i class="fa fa-pencil"></i> </asp:LinkButton>
						<asp:LinkButton ID="lnkDeleteBucket" runat="server" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('Warning! Are you sure you want to delete this platform?');" OnClick="lnkDeleteBucket_Click" Visible="False"> <i class="fa fa-trash"></i> </asp:LinkButton>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</div>
</asp:Panel>

<asp:Panel ID="pnlEdit" runat="server" Visible="false">
	<div class="OneColTitle">
		<h1>PLATFORMS<img src="/images/bg_title.png" class="title_decor" /></h1>
	</div>
	<asp:HiddenField ID="hdID" runat="server" Value="0" />
	<div class="form-horizontal MarginTopThirty row">
		<div class="form-group">
			<asp:Label ID="lblPlatform" runat="server" AssociatedControlID="txtPlatform" CssClass="col-sm-2 control-label">Platform Title:</asp:Label>
			<div class="col-sm-10">
				<asp:TextBox ID="txtPlatform" runat="server" CssClass="form-control"></asp:TextBox>
			</div>
		</div>
			
			<div class="form-group">
				<label Class="col-sm-2 control-label">Platform Domain</Label>
				<div class="col-sm-10">
					<input type="textbox" class="form-control" />
				</div>
			</div>
			
		<div class="form-group">
			<asp:Label ID="lblPath" runat="server" AssociatedControlID="txtPath" CssClass="col-sm-2 control-label">Platform Path:</asp:Label>
			<div class="col-sm-5">
				<asp:TextBox ID="txtPath" runat="server" CssClass="form-control"></asp:TextBox>
			</div>
		</div>
		<div class="form-group">
			<asp:Label ID="lblURL" runat="server" AssociatedControlID="txtURL" CssClass="col-sm-2 control-label">Platform URL:</asp:Label>
			<div class="col-sm-10">
				<asp:TextBox ID="txtURL" runat="server" CssClass="form-control"></asp:TextBox>
			</div>
		</div>
		<div class="form-group">
			<asp:Label ID="lblFreshOn" runat="server" AssociatedControlID="ddlFresh" CssClass="col-sm-2 control-label">Items are "NEW" if less than:</asp:Label>
			<div class="col-sm-10">
				<asp:DropDownList ID="ddlFresh" runat="server" CssClass="form-control">
					<asp:ListItem Value="7" Text="1 week old"></asp:ListItem>
					<asp:ListItem Value="14" Text="2 weeks old"></asp:ListItem>
					<asp:ListItem Value="21" Text="3 weeks old"></asp:ListItem>
					<asp:ListItem Value="28" Text="4 weeks old"></asp:ListItem>
					<asp:ListItem Value="42" Text="6 weeks old"></asp:ListItem>
					<asp:ListItem Value="56" Text="8 weeks old"></asp:ListItem>
					<asp:ListItem Value="84" Text="12 weeks old"></asp:ListItem>
				</asp:DropDownList>
			</div>
		</div>
		<div class="form-group">
			<asp:Label ID="lblContactLink" runat="server" AssociatedControlID="txtContactUs" CssClass="col-sm-2 control-label">Contact Us Link:</asp:Label>
			<div class="col-sm-10">
				<asp:TextBox ID="txtContactUs" runat="server" CssClass="form-control"></asp:TextBox>
			</div>
		</div>
		<div class="form-group">
			<asp:Label ID="lblHeaderText" runat="server" AssociatedControlID="txtHeaderText" CssClass="col-sm-2 control-label">Header Text:</asp:Label>
			<div class="col-sm-10">
				<ckeditor:ckeditorcontrol id="txtHeaderText" runat="server" height="300"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">CSS Design File</label>
			<div class="col-sm-10">
				<asp:FileUpload ID="flCss" runat="server" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Logo File</label>
			<div class="col-sm-10">
				<asp:FileUpload ID="flLogo" runat="server" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Current CSS File</label>
			<div class="col-sm-10">
				<asp:Literal ID="lnkCss" runat="server"/>
				<asp:Literal ID="ltWarning" runat="server"/>
			</div>
		</div>

		<div class="MarginTopThirty">
			<div class="pull-right">
				<asp:Button ID="btnCancelTag" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancelTag_Click" />
				<asp:Button ID="btnSaveTag" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSaveTag_Click" />
			</div>
			<div class="clearboth">&nbsp;</div>
		</div>

		<a name="pageList"></a>
		<asp:Panel ID="pnlPages" runat="server">
			<h3 id="lblPagesHeader" style="margin-top: 20px" runat="server">Pages</h3>
			<asp:Repeater ID="lstPages" runat="server">
				<HeaderTemplate>
					<table class="table table-hover table-custom" cellspacing="0" rules="all" border="1" id="lstView" style="border-collapse: collapse;">
						<tbody class="sortable">
							<tr>
								<th scope="col">Order</th>

								<th scope="col">Page Name</th>
								<th scope="col">Enabled</th>
								<th scope="col">Edit</th>
							</tr>
				</HeaderTemplate>
				<ItemTemplate>
							<tr class="pages" data-pageid="<%# Eval("id") %>" data-ordr="<%# Eval("ordr") %>">
                                <td><%# Eval("ordr") %></td>
								<td><%# Eval("title") %></td>
								<td><%# getStatus(Eval("status")) %></td>
								<td>
									<asp:LinkButton ID="lnkEditPage" CommandArgument='<%# Eval("id") %>' runat="server" CssClass="btn btn-primary btn-xs" OnClick="lnkEditPage_OnClick"><i class="fa fa-pencil"></i></asp:LinkButton>
								</td>
							</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
			</asp:Repeater>
		</asp:Panel>
	<script>
	    $(function () {
	        $('.sortable').sortable({
	            stop: function (event, ui) {
	                $('.pages').each(function (index) {
	                    $(this).attr('data-ordr', index);
	                    $.post('/api.ashx?action=order_pages&id=' + $(this).attr('data-pageid') + '&ordr=' + index, function (data) {
	                        console.log('ordering', data);
	                    });
	                });
	            }
	        });
	    })

</script>	
		<a name="editPage"></a>
		<asp:Panel ID="pnlEditPage" runat="server" Visible="False">
			<div class="form-horizontal MarginTopThirty row">
				<asp:HiddenField ID="hdPageID" runat="server" />
				<div class="form-group">
					<label class="col-sm-2 control-label" for="txtPageTitle">Page Title</label>
					<div class="col-sm-10">
						<asp:TextBox ID="txtPageTitle" runat="server" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="lstStatus">Enabled</label>
					<div class="col-sm-10">
						<asp:RadioButtonList ID="lstStatus" runat="server">
							<asp:ListItem Value="0">No</asp:ListItem>
							<asp:ListItem Value="1">Yes</asp:ListItem>
						</asp:RadioButtonList>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Page Content</label>
					<div class="col-sm-10">
						<asp:Label ID="lblNotEditable" runat="server" Text="Not available" style="color: gray" Visible="False"/>
						<ckeditor:ckeditorcontrol id="ckContent" runat="server" height="500" Visible="False" />
					</div>
				</div>

				<div class="MarginTopThirty">
					<div class="pull-right">
						<asp:Button ID="btnCancelPage" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancelPage_OnClick" />
						<asp:Button ID="btnSavePage" runat="server" Text="Save" CssClass="btn btn-default" OnClick="btnSavePage_OnClick" />
					</div>
					<div class="clearboth">&nbsp;</div>
				</div>
			</div>

		</asp:Panel>
	</div>
</asp:Panel>
