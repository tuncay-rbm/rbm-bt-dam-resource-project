﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_stats : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdPlatformID.Value = rbm.getPlatformID();
        var obj = rbm.Platform.getData(rbm.getPlatformID());
        hdPlatform.Value = obj.platform;
    }


    protected void lnkExport_Click(object sender, EventArgs e)
    {
        DataTable dt = rbm.Resources.exportStats(rbm.getPlatformID()).Tables[0];
        //create spreadsheet
        string name = DateTime.Now.ToString("f");
        name = (string)name.Replace(",", "_");
        name = (string)name.Replace(":", "_");
        var xlsx = new XLWorkbook();
        xlsx.AddWorksheet("current", 1);
        
        //create a header row
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            IXLColumn col = xlsx.Worksheet(1).Column(i+1);
            col.Width = 20;
            IXLCell cll = xlsx.Worksheet(1).Cell(1, i + 1);
            cll.Value = rbm.toStr(dt.Columns[i].ColumnName).ToUpper();
            cll.Style.Fill.BackgroundColor = XLColor.Black;
            cll.Style.Font.FontName = "Arial";
            cll.Style.Font.FontSize = 11;
            cll.Style.Font.FontColor = XLColor.White;
            cll.Style.Font.Bold = true;
            cll.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cll.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cll.Style.Border.OutsideBorder = XLBorderStyleValues.Hair;
        }
        //fill spreadsheet with products
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                IXLCell cll = xlsx.Worksheet(1).Cell(2+i,1+j);
                cll.Value = rbm.toStr(dt.Rows[i][j]);
                cll.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                cll.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                cll.Style.Border.OutsideBorder = XLBorderStyleValues.Hair;
            }
        }

        //save the spreadsheet
        string path = "/export";
        if (!Directory.Exists(HttpContext.Current.Server.MapPath(path))) {
            Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
        }
        string excel_file = "/export/_Export_" + name + ".xlsx";
        xlsx.SaveAs(HttpContext.Current.Server.MapPath(excel_file));
        //return the file
        Response.Redirect(excel_file);
    }
}