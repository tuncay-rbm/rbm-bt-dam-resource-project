﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_categories : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindData();
        }
    }
    protected void bindData() {

        //gridview
        Session["TaskTable"] = rbm.Categories.list(rbm.getPlatformID()).Tables[0];
        lstView.DataSource = Session["TaskTable"];
        lstView.DataBind();
    }
    protected void clearForm(){
        hdID.Value = "";
        txtPlatform.Text = "";
    }
    protected void toggle(string panel)
    {
        if(panel=="view")
        {
            pnlEdit.Visible = false;
            pnlView.Visible = true;
        }
        else if(panel=="edit")
        {
            pnlEdit.Visible = true;
            pnlView.Visible = false;
        }
    }
    protected void lnkAddCat_Click(object sender, EventArgs e)
    {
        toggle("edit");
    }
    protected void lnkEditBucket_Click(object sender, EventArgs e)
    {
        var obj = rbm.Categories.getData(rbm.getCommandArgument(sender));
        hdID.Value = obj.id;
        txtPlatform.Text = obj.title;
        ddlType.SelectedValue = obj.type;
        toggle("edit");
    }
    protected void lnkDeleteBucket_Click(object sender, EventArgs e)
    {
        rbm.Categories.delete(rbm.getCommandArgument(sender));
        bindData();
    }
    protected void btnCancelTag_Click(object sender, EventArgs e)
    {
        toggle("view");
        clearForm();
    }

    protected void btnSaveTag_Click(object sender, EventArgs e)
    {
        var obj = new rbm.Categories.stCategories();
        obj.id = hdID.Value;
        obj.title = txtPlatform.Text;
        obj.type = ddlType.SelectedValue;
        obj.platformID = rbm.getPlatformID();
        if (obj.id == "")
        {
            rbm.Categories.add(obj);
        }
        else
        {
            rbm.Categories.update(obj);
        }

        clearForm();
        toggle("view");
        bindData();
    }

    protected void lstView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = Session["TaskTable"] as DataTable;
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
        }
        lstView.DataSource = Session["TaskTable"];
        lstView.DataBind();
    }

    private string GetSortDirection(string column)
    {
        string sortDirection = "ASC";
        string sortExpression = ViewState["SortExpression"] as string;
        if (sortExpression != null)
        {
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = column;
        return sortDirection;
    }

    protected void lstView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
            //gridview
        Session["TaskTable"] = rbm.Categories.list(rbm.getPlatformID()).Tables[0];
        lstView.DataSource = Session["TaskTable"];
        //lstView.PageIndex = e.NewPageIndex;
        lstView.DataBind();
        
    }

    protected void ddlCategorySort_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlCategorySort.SelectedValue) { 
            case "":
                Session["TaskTable"] = rbm.Categories.list(rbm.getPlatformID()).Tables[0];
                lstView.DataSource = Session["TaskTable"];
                lstView.DataBind();
                break;
            case "bucket":
                Session["TaskTable"] = rbm.Categories.listBucketCategories(rbm.getPlatformID()).Tables[0];
                lstView.DataSource = Session["TaskTable"];
                lstView.DataBind();
                break;
            case "faq":
                Session["TaskTable"] = rbm.Categories.listFaqCategories(rbm.getPlatformID()).Tables[0];
                lstView.DataSource = Session["TaskTable"];
                lstView.DataBind();
                break;
        }
    }
}