﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class files_admin_faqs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            bindData();
        }
        else if (!rbm.isNull(rbm._get("action"))) {
            switch (rbm._get("action")) { 
                case "edit":
                    edit(rbm._get("id"));
                    break;
            }

        }
    }
    protected void bindData() {

        rbm.fill(ddlCat, rbm.Categories.listFaqCategories(rbm.getPlatformID()), "id", "title");
        //gridview
        Session["TaskTable"] = rbm.FrequentQuestions.listAdmin(rbm.getPlatformID()).Tables[0];
        lstQuestion.DataSource = Session["TaskTable"];
        lstQuestion.DataBind();
    }
    protected void clearForm(){
        hdID.Value = "";
        txtQuestion.Text = "";
        txtAnswer.Text = "";
        ddlCategorySet.Items.Clear();
    }
    protected void toggle(string panel)
    {
        if(panel=="view")
        {
            pnlEdit.Visible = false;
            pnlView.Visible = true;
        }
        else if(panel=="edit")
        {
            pnlEdit.Visible = true;
            pnlView.Visible = false;
        }
    }
    protected void lnkAddCat_Click(object sender, EventArgs e)
    {
        fillCategoryCheckbox();
        toggle("edit");
    }

    protected void edit(string id)
    {
        toggle("edit");

        rbm.FrequentQuestions.stFrequentQuestion obj = rbm.FrequentQuestions.getData(rbm.getCommandArgument(id));
        hdID.Value = obj.id;
        txtAnswer.Text = obj.answer;
        txtQuestion.Text = obj.question;
        ddlCat.SelectedValue = obj.categoryID;
        checkCategoryCheckbox(obj.id);
    }
    protected void lnkEditTag_Click(object sender, EventArgs e)
    {
        toggle("edit");

        rbm.FrequentQuestions.stFrequentQuestion obj = rbm.FrequentQuestions.getData(rbm.getCommandArgument(sender));
        hdID.Value = obj.id;
        txtAnswer.Text = obj.answer;
        txtQuestion.Text = obj.question;
        ddlCat.SelectedValue = obj.categoryID;

        checkCategoryCheckbox(obj.id);
    }
    protected void lnkDeleteTag_Click(object sender, EventArgs e)
    {
        rbm.FrequentQuestions.delete(rbm.getCommandArgument(sender));
        rbm.FrequentQuestionsAssoc.clearAssociations(rbm.getCommandArgument(sender));
        bindData();
    }
    protected void btnCancelTag_Click(object sender, EventArgs e)
    {
        toggle("view");
        clearForm();
    }

    protected void btnSaveTag_Click(object sender, EventArgs e)
    {
        rbm.FrequentQuestions.stFrequentQuestion obj = new rbm.FrequentQuestions.stFrequentQuestion();
        obj.id = hdID.Value;
        obj.question = txtQuestion.Text;
        obj.answer = txtAnswer.Text;
        obj.categoryID = ddlCat.SelectedValue;
        obj.platformID = rbm.getPlatformID();
        string questionID = "";
        if (obj.id == "")
        {
            questionID = rbm.FrequentQuestions.add(obj);
        }
        else
        {
            questionID = obj.id;
            rbm.FrequentQuestions.update(obj);
        }

        updateBucketQuestionAssociations(questionID);

        clearForm();
        toggle("view");
        bindData();

    }

    protected void lstView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = Session["TaskTable"] as DataTable;
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
        }
        lstQuestion.DataSource = Session["TaskTable"];
        lstQuestion.DataBind();
    }

    private string GetSortDirection(string column)
    {
        string sortDirection = "ASC";
        string sortExpression = ViewState["SortExpression"] as string;
        if (sortExpression != null)
        {
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = column;
        return sortDirection;
    }

    protected void lstView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
            //gridview
        Session["TaskTable"] = rbm.FrequentQuestions.listAdmin(rbm.getPlatformID()).Tables[0];
        lstQuestion.DataSource = Session["TaskTable"];
        //lstQuestion.PageIndex = e.NewPageIndex;
        lstQuestion.DataBind();
        
    }

    protected void fillCategoryCheckbox()
    {
        DataTable ddlCat = rbm.Bucket.list(rbm.getPlatformID()).Tables[0];
        for (int i = 0; i < ddlCat.Rows.Count; i++)
        {
            ListItem item = new ListItem();
            item.Value = rbm.toStr(ddlCat.Rows[i][0]);
            item.Text = rbm.toStr(ddlCat.Rows[i][1]);
            ddlCategorySet.Items.Add(item);
        }
    }

    protected void checkCategoryCheckbox(string questionID)
    {
        DataTable ddlCat = rbm.Bucket.list(rbm.getPlatformID()).Tables[0];
        string isChecked = "";
        for (int i = 0; i < ddlCat.Rows.Count; i++)
        {
            ListItem item = new ListItem();
            item.Value = rbm.toStr(ddlCat.Rows[i][0]);
            item.Text = rbm.toStr(ddlCat.Rows[i][1]);
            isChecked = rbm.FrequentQuestionsAssoc.bucketIsChecked(rbm.toStr(ddlCat.Rows[i][0]), questionID);
            item.Selected = isChecked != "" ? true : false;
            ddlCategorySet.Items.Add(item);
        }

    }
    
    protected void updateBucketQuestionAssociations(string questionID)
    {
        rbm.FrequentQuestionsAssoc.clearAssociations(questionID);
        foreach (ListItem item in ddlCategorySet.Items)
        {
            if (item.Selected)
            {
                rbm.FrequentQuestionsAssoc.stFrequentQuestionAssoc obj = new rbm.FrequentQuestionsAssoc.stFrequentQuestionAssoc();
                obj.bucketID = item.Value;
                obj.questionID = questionID;
                rbm.FrequentQuestionsAssoc.add(obj);
            }
        }
    }



}