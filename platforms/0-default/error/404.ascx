﻿<%@ Control Language="C#" ClassName="home" %>
<!-- 404 Error -->

<h3>Page Not Found</h3>
<p><a href="/">Click Here</a> if you are not redirected to our homepage within 10 seconds.</p>

<script>
	$(document).ready(function() {
		setTimeout(function() {window.location.replace("/")}, 10000);
	});
</script>