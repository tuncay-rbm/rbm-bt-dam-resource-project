﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register TagPrefix="inc" Namespace="DAM" Assembly="App_Code" %>

<!doctype html>
<html id="ng-app" ng-app="DAM">
<head runat="server">
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Baker and Taylor, custom reach, marketing solutions, cutomreach, Baker and Taylor Assets, Axis 360, Baker and Taylor Library">
    <meta name="author" content="Right Brain Media">

    <title>Baker &amp; Taylor - DAM</title>

    <!-- CSS -->
	<link href="/styles/bootstrap.min.css" rel="stylesheet">
	<link href="/styles/rbm_style.css" rel="stylesheet">
	<link href="/styles/custom_style.css" rel="stylesheet">

	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css">
	<![endif]-->

	<!-- CUSTOM FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
    <link rel="icon" href="/images/favicon.ico">

    <!-- SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>  
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap-tpls-0.12.0.js"></script>
    <script type="text/javascript" src="/js/angular-file-upload.min.js"></script>
    <script type="text/javascript" src="/js/ng-tags-input.min.js"></script>
    <script type="text/javascript" src="/js/slider.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.min.js"></script>
    <script type="text/javascript" src="/js/directives/ngClip.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>

    <script type="text/javascript" src="/js/app.js"></script>

    <asp:PlaceHolder ID="MetaPlaceHolder" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="css" runat="server"></asp:PlaceHolder>
</head>
<body runat="server">

    <form id="frm" runat="server">
		<asp:PlaceHolder ID="header" runat="server" />
	    <asp:PlaceHolder ID="content" runat="server"/>
	    <asp:PlaceHolder ID="footer" runat="server"/>
    </form>

    <!-- HACKS -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>

</html>