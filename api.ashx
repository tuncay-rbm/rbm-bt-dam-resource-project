﻿<%@ WebHandler Language="C#" Class="api" %>

using System;
using System.Web;

public class api : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context)
    {
	    string platformName = "";
	    string refer = HttpContext.Current.Request.UrlReferrer.ToString();
	    if (!rbm.isNull(refer))
	    {
		    string key = "platformName=";
		    if (refer.Contains(key))
		    {
			    int p1 = refer.IndexOf(key) + key.Length;
			    platformName = refer.Substring(p1, refer.Length - p1).Replace("/","");
		    }
		    else
		    {
				Uri url = new Uri(refer);
			    platformName = url.Segments[1].Replace("/","");
		    }
	    }
		
		rbm.log("API Platform: " + platformName);

	    string action = rbm._get("action");
        string filename = "";
		rbm.log("API Action: " + action);
        
        
        if (!rbm.isNull(HttpContext.Current.Request.Files["file"]))
        {
            filename = saveFile(HttpContext.Current.Request.Form["platform"],HttpContext.Current.Request.Form["bucket"]);
        }
        switch (action)
        {
            case "save_file":
                if (filename != "")
                {
                    string path;
                    try
                    {
                        path = "_resources/" + HttpContext.Current.Request.Form["platform"] + "/" + HttpContext.Current.Request.Form["bucket"] + "/";
                        rbm.echo(getDimensions(path,filename));
                    }
                    catch (Exception)
                    {
                        path = "_resources/" + HttpContext.Current.Request.Form["platform"] + "/" + HttpContext.Current.Request.Form["bucket"] + "/";
                        rbm.echo(getDimensions(path, filename));
                    }
                }
                else {
                    rbm.echo("{\"err\":\"Upload Error\"}");                
                }
                rbm.die();
                break;
            case "get_file_data":
                var path_filename = rbm.Resources.getPathAndFile(rbm._get("resourceID"));
                try
                {
                    string[] arr = path_filename.Split('|');
                    rbm.echo(getDimensions(arr[0], arr[1]));
                }
                catch (Exception)
                {
                    rbm.echo("[]");
                }                
                rbm.die();
                break;
            case "order_pages":
                rbm.Platform.setPageOrder(rbm._get("id"), rbm._get("ordr"));
                rbm.echo("reordering pages");
                rbm.die();
                break;
            case "delete_resource":
                rbm.Resources.delete(rbm._get("resourceID"));
                rbm.ResourceTools.clearTools(rbm._get("resourceID"));
                rbm.ResourceQuestions.clearResourceQuestions(rbm._get("resourceID"));
                rbm.Tags.clearTags(rbm._get("resourceID"));
                rbm.die();
                break;
            case "save_asset":
                context.Response.ContentType = "application/json";
                var resource = (rbm.Resources.stResources)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpContext.Current.Request.Form["data"], typeof(rbm.Resources.stResources));
                resource = rbm.Resources.add(resource);
                rbm.echo(rbm.obj2json(rbm.Resources.getData(resource.id)));
                rbm.die();
                break;
            case "update_asset":
                context.Response.ContentType = "application/json";
                var updated = (rbm.Resources.stResources)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpContext.Current.Request.Form["data"], typeof(rbm.Resources.stResources));
                rbm.Resources.update(updated);
                rbm.echo(rbm.obj2json(rbm.Resources.getData(updated.id)));
                rbm.die();
                break;
            case "clear_resource_tools":
                context.Response.ContentType = "application/json";
                rbm.ResourceTools.clearTools(rbm._get("resourceID"));
                rbm.die();
                break;
            case "clear_resource_questions":
                context.Response.ContentType = "application/json";
                rbm.ResourceQuestions.clearResourceQuestions(rbm._get("resourceID"));
                rbm.die();
                break;
            case "clear_resource_tags":
                context.Response.ContentType = "application/json";
                rbm.Tags.clearTags(rbm._get("resourceID"));
                rbm.die();
                break;
            case "list_resource_tags":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Tags.list(rbm._get("resourceID"))));
                rbm.die();
                break;
            case "save_resource_tools":
                context.Response.ContentType = "application/json";
                var tools = (rbm.ResourceTools.stResourceTools)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpContext.Current.Request.Form["data"], typeof(rbm.ResourceTools.stResourceTools));
                rbm.ResourceTools.add(tools);
                rbm.die();
                break;
            case "save_resource_questions":
                context.Response.ContentType = "application/json";
                var questions = (rbm.ResourceQuestions.stResourceQuestions)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpContext.Current.Request.Form["data"], typeof(rbm.ResourceQuestions.stResourceQuestions));
                rbm.ResourceQuestions.add(questions);
                rbm.die();
                break;
            case "save_resource_tags":
                context.Response.ContentType = "application/json";
                var tags = (rbm.Tags.stTags)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpContext.Current.Request.Form["data"], typeof(rbm.Tags.stTags));
                rbm.Tags.add(tags, rbm.getPlatformID(platformName));
                rbm.die();
                break;
            case "list":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Resources.list(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "get_platform_title":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.obj2json(rbm.Platform.getData()));
                rbm.die();
                break;
            case "edit_asset":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Resources.list(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "list_stats":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Resources.resourceStats(rbm.getPlatformID(platformName))));
                break;
            case "list_buckets":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Bucket.list(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "list_bucket_tools":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Bucket.ToolsByBucketID(rbm._get("bucketID"))));
                rbm.die();
                break;
            case "list_platforms":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Platform.list()));
                rbm.die();
                break;
            case "list_resource_tools":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.ResourceTools.ListResourceTools(rbm._get("resourceID"), rbm._get("bucketID"))));                
                rbm.die();
                break;
            case "list_resource_faqs":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.ResourceQuestions.listResourceQuestions(rbm._get("resourceID"), rbm._get("bucketID"))));
                rbm.die();
                break;
            case "list_faqs":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.FrequentQuestions.list(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "listFaqs":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.FrequentQuestions.list(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "listFaqCategories":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Categories.listFaqCategories(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "listPlatforms":
                context.Response.ContentType = "application/json";

                rbm.die();
                break;
            case "listBuckets":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.FilterOptions.listBuckets(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "listBucketsByCategory":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.FilterOptions.listBucketsByCategory(rbm._get("categoryID"))));
                rbm.die();
                break;
            case "listCategories":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.FilterOptions.listCategories(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "listThemes":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Tags.getThemes(rbm.getPlatformID(platformName))));                
                rbm.die();
                break;
            case "list_autocomplete_tags":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.ds2json(rbm.Tags.listForAutocomplete(rbm.getPlatformID(platformName))));
                rbm.die();
                break;
            case "getAsset":
                context.Response.ContentType = "application/json";
                rbm.echo(rbm.obj2json(rbm.Asset.getAsset(rbm._get("id"))));
                rbm.die();
                break;
            case "downloadPrintJPG":
                rbm.Resources.countDownload(rbm._get("resourceID"));
                rbm.stats.parseAndSave(rbm.getPlatformID(platformName), rbm._get("resourceID"), GetUser_IP());
                //var file_img_path = rbm.CustomTasks.PdfToPng(rbm._get("resourceID"), rbm._get("size"));
                var file_img_path = rbm.CustomTasks.DownloadResource(rbm._get("resourceID"), rbm._get("size"), "img");     
                try
                {
                    System.Net.WebClient req = new System.Net.WebClient();
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/octet-stream";
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + file_img_path.filename + "\"");
                    response.Headers.Add("x-filename", file_img_path.filename);
                    response.TransmitFile(context.Server.MapPath(file_img_path.path));
                    response.Flush();
                    response.End();
                }
                catch (Exception df_ex)
                {
                    rbm.echo(df_ex.ToString());
                }    
                rbm.die();
                break;
            case "downloadPrintPDF":
                rbm.Resources.countDownload(rbm._get("resourceID"));
                rbm.stats.parseAndSave(rbm.getPlatformID(platformName), rbm._get("resourceID"), GetUser_IP());
                var file_pdf_path = rbm.CustomTasks.DownloadResource(rbm._get("resourceID"), rbm._get("size"), "pdf");           
                //var file_pdf_path = rbm.CustomTasks.ResizePdf(rbm._get("resourceID"), rbm._get("size"));
                try
                {
                    System.Net.WebClient req = new System.Net.WebClient();
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/octet-stream";
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + file_pdf_path.filename + "\"");
                    response.Headers.Add("x-filename", file_pdf_path.filename);
                    response.TransmitFile(context.Server.MapPath(file_pdf_path.path));
                    response.Flush();
                    response.End();
                }
                catch (Exception df_ex)
                {
                    rbm.echo(df_ex.ToString());
                }    
                rbm.die();
                break;
            case "downloadImageJPG":
                rbm.Resources.countDownload(rbm._get("resourceID"));
                rbm.stats.parseAndSave(rbm.getPlatformID(platformName), rbm._get("resourceID"), GetUser_IP());
                           
                var file_and_path = rbm.CustomTasks.ResizeImage(rbm._get("resourceID"), rbm._get("size"));
                try
                {
                    System.Net.WebClient req = new System.Net.WebClient();
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/octet-stream";
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + file_and_path.filename + "\"");
                    response.Headers.Add("x-filename", file_and_path.filename);
                    response.TransmitFile(context.Server.MapPath(file_and_path.path));
                    response.Flush();
                    response.End();
                }
                catch (Exception df_ex)
                {
                    rbm.echo(df_ex.ToString());
                }    
                rbm.die();
                break;
            case "downloadImagePDF":
                rbm.Resources.countDownload(rbm._get("resourceID"));
                rbm.stats.parseAndSave(rbm.getPlatformID(platformName), rbm._get("resourceID"), GetUser_IP());
                             
                var file_an_path = rbm.CustomTasks.NewPngToPdf(rbm._get("resourceID"), rbm._get("size"));
                try
                {
                    System.Net.WebClient req = new System.Net.WebClient();
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/octet-stream";
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + file_an_path.filename + "\"");
                    response.Headers.Add("x-filename", file_an_path.filename);
                    response.TransmitFile(context.Server.MapPath(file_an_path.path));
                    response.Flush();
                    response.End();
                }
                catch (Exception df_ex)
                {
                    rbm.echo(df_ex.ToString());
                }    
                rbm.die();
                break;
            case "downloadFile":
                rbm.Resources.countDownload(rbm._get("resourceID"));
                rbm.stats.parseAndSave(rbm.getPlatformID(platformName), rbm._get("resourceID"), GetUser_IP());
                
                var download_file = rbm.Resources.getPathAndFile(rbm._get("resourceID"));
                try
                {
                    string[] arr = download_file.Split('|');
                    var df_path = arr[0];
                    var df_filename = arr[1];
                    System.Net.WebClient req = new System.Net.WebClient();
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/octet-stream";
                    response.AddHeader("Content-Disposition", "attachment;filename=\"" + df_filename + "\"");
                    response.Headers.Add("x-filename", df_filename);
                    response.TransmitFile(context.Server.MapPath(df_path + df_filename));
                    response.Flush();
                    response.End();
                }
                catch (Exception df_ex)
                {
                    rbm.echo(df_ex.ToString());
                }    
                rbm.die();
                break;
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    protected string saveFile(string platform,string bucket)
    {
        string dir = "_resources/" + platform + "/" + bucket + "/";
        HttpPostedFile file = HttpContext.Current.Request.Files["file"];
        string new_name = DateTime.Now.Second.ToString() + "_" + file.FileName;
        string sDirectory = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, dir);
        if (!System.IO.Directory.Exists(sDirectory)) System.IO.Directory.CreateDirectory(sDirectory);
        //refreshPage = System.IO.File.Exists(HttpContext.Current.Server.MapPath("/" + imagePath)) == true ? true : false;
        try
        {
            file.SaveAs(sDirectory + new_name);
            file.InputStream.Close();
        }
        catch (Exception)
        {
            try
            {
                new_name = DateTime.Now.Second.ToString() + "_" + file.FileName;
                file.SaveAs(sDirectory + new_name);
                file.InputStream.Close();
            }
            catch (Exception)
            {
                file.InputStream.Close();
            }

        }

        return new_name;
    }

    protected string getDimensions(string path, string filename) 
    {
        try
        {
            rbm.CustomTasks.stFileDims dims = rbm.CustomTasks.getDimensions(path + "/" + filename);
            rbm.CustomTasks.stFileData obj = new rbm.CustomTasks.stFileData();
            obj.filename = filename;
            obj.filesize = rbm.CustomTasks.getFileSize(path + "/" + filename);
            obj.extension = System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(path + "/" + filename));            
            obj.width = dims.width;
            obj.height = dims.height;
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
        catch (Exception)
        {
            rbm.CustomTasks.stFileData obj = new rbm.CustomTasks.stFileData();
            obj.filename = filename;
            obj.filesize = rbm.CustomTasks.getFileSize(path + "/" + filename);
            obj.extension = System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(path + "/" + filename));
            obj.width = "";
            obj.height = "";
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
    }

    protected string GetUser_IP()
    {
        string VisitorsIPAddr = string.Empty;
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        }
        return VisitorsIPAddr;
    }
}