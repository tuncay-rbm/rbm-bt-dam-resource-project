﻿DAM.controller('LibraryController', function ($scope, $http, $location, $sce, $modal, $log, $window, FileUploader) {
    $scope.loadingImageSrc = '/images/loading2.gif';
    $scope.allMyAssets = [];
    $scope.categories = [];
    $scope.allMyThemes = [];

    function checkLoading() {
        return !$scope.updateBrowser && $scope.allMyAssets.length > 0 && $scope.categories.length > 0 && $scope.allMyThemes.length > 0;
    }
    checkLoading();
    //BROWSER DETECTION
    function get_browser_info() {
        var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return { name: 'IE', version: (tem[1] || '') };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/)
            if (tem != null) { return { name: 'Opera', version: tem[1] }; }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
        return {
            name: M[0],
            version: M[1]
        };
    }
    $scope.updateBrowser = false;
    $scope.browserInfo = get_browser_info();
    if ($scope.browserInfo.name === 'IE' && $scope.browserInfo.version < 9) {
        $scope.updateBrowser = true;
        jQuery('#SliderContainer').hide();
        jQuery('.navigation').hide();
    }

    $scope.platform_title = jQuery('#hdPlatform').val();
    $scope.platform_id = jQuery('#hdPlatformID').val();
    $scope.platform_url = jQuery('#hdPlatformUrl').val();
    //DELETE CONFIRMATION MODAL
    //console.log('here!', $scope.platform_title, $scope.platform_id, $scope.platform_url);
    $scope.deleteResource = function (asset, index) {
        //console.log('deleting', asset,index);
        $scope.deletingAsset = asset;
        $scope.deletingIndex = index;
        $scope.deleteModal('lg', asset, $scope.platform_url);
    };
    $scope.deleteModal = function (size, asset, platform_url) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/ConfirmModal.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.asset = asset;
                $scope.getFileData = function (asset) {
                    var url = '/api.ashx',
                    action = '?action=get_file_data&resourceID=' + asset.id;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.fileData = data;
                        ////console.log('fileData', $scope.fileData, $scope.fileData);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getFileData(asset);
                $scope.close = function () {
                    $scope.assetDeleted = false;

                    modalInstance.close();
                }
                //video links
                $scope.trustLink = function (link) {
                    $scope.trustedLink = $sce.trustAsResourceUrl(link);
                };
                $scope.trustLink($scope.asset.link);
                $scope.assetDeleted = false;
                $scope.deleteAsset = function (asset) {
                    var url = '/api.ashx',
                    action = '?action=delete_resource&resourceID=' + asset.id;
                    $http({
                        method: 'POST',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.assetDeleted = true;
                        modalInstance.close();
                        //$window.location.href = '/' + platform_url;

                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
            },

            size: size,
            resolve: {

                asset: function () {
                    return asset;
                }
            }
        });

        modalInstance.result.then(function (asset) {

            $scope.returnFromDelete();
        }, function () {
        });
    };
    $scope.returnFromDelete = function () {
        list();
    }
    $scope.setUserID = function (userID) {
        $scope.userID = userID;
        ////console.log('userID', userID);
    }
    $scope.displayCountOptions = [{ label: 'Items Per Page:25', value: 25 }, { label: '50', value: 50 }, { label: '100', value: 100 }];
    $scope.selectSortOptions = [{ label: 'Recently Added', value: 0 }, { label: 'Popularity', value: 1 }, { label: 'Alphabetically', value: 2 }];
     
    $scope.myMethod = function (pageNo, perPage, totalItems) {

        var x = pageNo;
        var y = perPage;
        var ofVal = x * y > totalItems ? totalItems : x * y;
        if (totalItems < perPage) {
            $scope.pagerMessage = 'Items ' + (((x * y) - y) + 1) + '-' + ofVal + ' of ' + totalItems;

        } else {
            $scope.pagerMessage = 'Items ' + (((x * y) - y) + 1) + '-' + ofVal + ' of ' + totalItems;
        }
        //console.log(pageNo);
        //console.log('Items ' + (((x*y)-y)+1) + '-' + x*y + ' of ' + totalItems);
    };

    $scope.loadTags = function () {
        var autoTags = [];
        var url = '/api.ashx',
        action = '?action=list_autocomplete_tags'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.loadedTags = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.loadTags();

    $scope.watchSelectSort = function (val) {
        ////console.log('$scope.selectSort', val);
        switch (val) {
            case 0:
                $scope.sort('saveDate');
                $scope.sort('saveDate');

                break;
            case 1:
                $scope.sort('downloads');
                break;
            case 2:
                $scope.sort('title');
                break;
        }
    };

    $scope.setInitialSort = function (val) {
        $scope.sort(val);
        $scope.sort(val);
    }
    $scope.editMode = false;
    $scope.editAsset = function (asset) {        

        $scope.editMode = true;
        $scope.loadTags();
        $scope.listBuckets();
        $scope.listPlatforms();
        $scope.listTags(asset);
        $scope.getResourceTools(asset);
        $scope.getResourceQuestions(asset);
        $scope.getAllQuestions();
        $scope.trustLink(asset.link);
        $scope.selectedAsset = asset;
        $scope.getFileData(asset);
        $scope.uploader = new FileUploader({
            url: 'api.ashx',
            removeAfterUpload: true,
            onSuccessItem: function (item, response, status, headers) {
                list();
            },
            //formData: [{ action: 'save_file', platform: $scope.platform_title, bucket: $scope.selectedAsset.bucket }],
            formData: [],

            queueLimit: 1,
        });
        //VIDEO TOOLS
        $scope.setVideoHeight = function (idx) {
            $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * 0.5625);
        }
        $scope.setVideoWidth = function (idx) {
            $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * 1.777);
        }
        //PRINT MEDIA TOOLS
        $scope.setPrintMediaHeight = function (idx) {
            var wid = $scope.resourceTools[idx].width;
            if (wid > 100) { $scope.resourceTools[idx].width = 100 };
        }
        //IMAGE TOOLS
        $scope.setImageHeight = function (idx) {
            var height_to_width = $scope.fileData.height / $scope.fileData.width;
            $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * height_to_width);
        }
        $scope.setImageWidth = function (idx) {
            var width_to_height = $scope.fileData.width / $scope.fileData.height;
            $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * width_to_height);
        }
        $scope.checkForOutput = function (tool, bucketTypeID) {

            ////console.log('checking for output', tool);
        };
        ////console.log(asset);
    };
    $scope.getFileData = function (asset) {
        var url = '/api.ashx',
        action = '?action=get_file_data&resourceID=' + asset.id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.fileData = data;
            ////console.log('fileData', $scope.fileData, $scope.fileData);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.listTags = function (asset) {
        var url = '/api.ashx',
        action = '?action=list_resource_tags&resourceID=' + asset.id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.resourceTags = data;
            $scope.tags = [];
            if ($scope.resourceTags.length) {
                for (var i = 0; i < $scope.resourceTags.length; i++) {
                    var obj = {text:$scope.resourceTags[i].tag}
                    $scope.tags.push(obj);
                }
            }
            ////console.log('tags', $scope.resourceTags, $scope.tags);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    //video links
    $scope.trustLink = function (link) {
        $scope.trustedLink = $sce.trustAsResourceUrl(link);
    };
    $scope.deleteAsset = function (asset) {
        var url = '/api.ashx',
        action = '?action=delete_resource&resourceID=' + asset.id;
        $http({
            method: 'POST',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            list();
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getResourceTools = function (asset) {
        var url = '/api.ashx',
            action = '?action=list_resource_tools&resourceID=' + asset.id + '&bucketID=' + asset.bucketID;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.resourceTools = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.getResourceQuestions = function (asset) {
        var url = '/api.ashx',
            action = '?action=list_resource_faqs&resourceID=' + asset.id + '&bucketID=' + asset.bucketID;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.resourceQuestions = data;
            if ($scope.resourceQuestions.length) {
            } else { $scope.resourceQuestions=[]; }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.getAllQuestions = function () {
        var url = '/api.ashx',
            action = '?action=list_faqs';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.allQuestions = data;
            $scope.includedQuestion = $scope.allQuestions[0];
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.addQuestion = function () {
        var idx = $scope.includedQuestion.opt;
        $scope.includedQuestion = $scope.allQuestions[idx];
        var addQuestion = {
            questionID: $scope.includedQuestion.id,
            question: $scope.includedQuestion.question,
            selected:true
        }
        $scope.resourceQuestions.push(addQuestion);
        ////console.log('questions', $scope.resourceQuestions, idx);
    }
    $scope.removeFAQ = function (idx) {
        $scope.resourceQuestions.splice(idx, 1);

        ////console.log('removing', $scope.resourceQuestions);
    };
    $scope.setHeight = function (idx) {
        $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * 0.5625);
    }
    $scope.setWdith = function (idx) {
        $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * 1.777);
    }
    $scope.cancelEditMode = function () {
        $scope.editMode = false;
    }
    $scope.saveEdits = function () {
        var description = $scope.selectedAsset.description == 'undefined' || $scope.selectedAsset.description == undefined ? '' : $scope.selectedAsset.description;
        $scope.selectedAsset.description = description;
        $scope.selectedAsset.editDate = "";
        $scope.selectedAsset.editBy = $scope.userID;
        ////console.log('saving...', $scope.selectedAsset, $scope.resourceTools, $scope.resourceQuestions);
        jsonObject = $scope.selectedAsset;
        json = angular.toJson(jsonObject);
        ////console.log('json', json);
        var url = '/api.ashx',
        action = '?action=update_asset';
        $http({
            method: 'POST',
            url: url + action,
            data: "data=" + encodeURIComponent(json),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            $scope.savedAsset = data;
            $scope.saveTools($scope.savedAsset.id);
            $scope.saveFaq($scope.savedAsset.id);
            $scope.saveTags($scope.savedAsset.id);
            $scope.editMode = false;
            $scope.selectedAsset = {};
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    }
    $scope.saveTools = function (id) {
        var url = '/api.ashx?action=clear_resource_tools&resourceID=' + id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.resourceTools.length) {
                for (var i = 0; i < $scope.resourceTools.length; i++) {
                    if ($scope.resourceTools[i].selected) {
                        var tool = {
                            id: '',
                            toolID: $scope.resourceTools[i].toolID,
                            resourceID: id,
                            width: $scope.resourceTools[i].width,
                            height: $scope.resourceTools[i].height,
                            isTrue: true,
                        };
                        json = angular.toJson(tool);
                        ////console.log('tool', json);
                        var url = '/api.ashx',
                        action = '?action=save_resource_tools';
                        $http({
                            method: 'POST',
                            url: url + action,
                            data: "data=" + json,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).
                        success(function (data, status, headers, config) {
                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                    }
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    };
    $scope.saveFaq = function (id) {
        var url = '/api.ashx?action=clear_resource_questions&resourceID=' + id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.resourceQuestions.length) {
                for (var i = 0; i < $scope.resourceQuestions.length; i++) {
                    var faq = {
                        id: '',
                        questionID: $scope.resourceQuestions[i].questionID,
                        resourceID: id
                    };
                    json = angular.toJson(faq);
                    ////console.log('faq', json);
                    var url = '/api.ashx',
                    action = '?action=save_resource_questions';
                    $http({
                        method: 'POST',
                        url: url + action,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    };
    $scope.saveTags = function (id) {
        var url = '/api.ashx?action=clear_resource_tags&resourceID=' + id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.resourceTools.length) {
                for (var i = 0; i < $scope.tags.length; i++) {
                    var tag = {
                        id: '',
                        resourceID: id,
                        tag: $scope.tags[i].text,
                    };
                    json = angular.toJson(tag);
                    ////console.log('tag', json);
                    var url = '/api.ashx',
                    action = '?action=save_resource_tags';
                    $http({
                        method: 'POST',
                        url: url + action,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {

                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };

    $scope.listBuckets = function () {
        var url = '/api.ashx',
            action = '?action=list_buckets';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.buckets = data;
            $scope.loadingData = checkLoading();

        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.listPlatforms = function () {
        var url = '/api.ashx',
            action = '?action=list_platforms';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.platforms = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.currentPage = 1;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };


    $scope.preview = function (asset) {
        $scope.modalUpdate('lg', asset, 'preview');

    };
    $scope.previewDownload = function (asset) {
        $scope.modalUpdate('lg', asset, 'download');

    };
    $scope.searchAllThemes = function () {
        ////console.log('themeOptions', $scope.themeOptions);
        $scope.themeModal('lg', $scope.themeOptions);
    };
    $scope.themeModal = function (size, themes) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/ThemeModal.html',
            controller: function ($scope, $modalInstance, $sce, theme) {
                $scope.themes = theme;
                ////console.log('themes', $scope.themes, theme);
                $scope.close = function () { modalInstance.close(); }
            },

            size: size,
            resolve: {
                theme: function () {
                    return themes;
                }
            }
        });

        modalInstance.result.then(function (themes) {
            $scope.selected = themes;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    //Open a modal window to View a single asset    
    $scope.modalUpdate= function (size, selectedAsset,tab) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/AssetModal.html',
            controller: function ($scope, $modalInstance,$sce, asset) {
                $scope.asset = asset;
                $scope.setTab = function (tab) {
                    switch (tab) {
                        case 'preview':
                            $scope.previewTab = true;
                            $scope.downloadTab = false;

                            break;
                        case 'download':
                            $scope.previewTab = false;
                            $scope.downloadTab = true;

                            break;
                    }

                };
                $scope.setTab(tab);
                //console.log('$scope.asset', $scope.asset);
                //video links
                $scope.trustLink = function (link) {
                    $scope.trustedLink = $sce.trustAsResourceUrl(link);
                };
                $scope.trustLink($scope.asset.link);
                $scope.getPlatform = function () {
                    var url = '/api.ashx',
                    action = '?action=get_platform_title';
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.platformInfo = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });

                };
                $scope.getPlatform();
                //FAQS
                $scope.getResourceQuestions = function (asset) {
                    var url = '/api.ashx',
                        action = '?action=list_resource_faqs&resourceID=' + asset.id + '&bucketID=' + asset.bucketID;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.resourceQuestions = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getResourceQuestions(asset);
                //TOOLS
                $scope.getResourceTools = function (asset) {
                    var url = '/api.ashx',
                        action = '?action=list_resource_tools&resourceID=' + asset.id + '&bucketID=' + asset.bucketID;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.resourceTools = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getResourceTools(asset);
                //FILE DATA
                $scope.getFileData = function (asset) {
                    var url = '/api.ashx',
                    action = '?action=get_file_data&resourceID=' + asset.id;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.fileData = data;
                        ////console.log('file data', $scope.fileData);
                        $scope.newDim = {};
                        $scope.newDim.width = 150;
                        
                        $scope.$watchCollection('newDim', function () {
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            ////console.log('watching neDim model', $scope.newDim.width);
                        });
                        $scope.selectedImageTool = null;
                        $scope.watchImageOption = function (tool) {
                            $scope.newDim.width = tool.width;
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            ////console.log('watching radio', $scope.newDim.width);
                        };
                        $scope.$watchCollection('selectedImageTool', function () {
                            $scope.newDim.width = $scope.selectedImageTool;
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            ////console.log('watching width', $scope.newDim.width);
                        });
                        //PRINT MEDIA DIMENSIONS
                        $scope.printDim = { width: 100 };
                        $scope.watchPrintOption = function (tool) {
                            $scope.printDim.width = tool.width;
                            ////console.log('watching print radio', $scope.printDim.width);
                        };

                        //VIDEO DIMENSIONS
                        $scope.showCustomVideoSize = false;
                        $scope.videoDim = { width:560, height: 315};
                        $scope.watchVideoSize = function (tool) {
                            if (tool.toolID == 12) { $scope.showCustomVideoSize = true;}
                            $scope.videoDim.width = tool.width;
                            $scope.videoDim.height = tool.height;
                            $scope.embedCode = '<iframe width="' + $scope.videoDim.width + '" height="' + $scope.videoDim.height + '" src="' + $scope.trustedLink + '" frameborder="0" allowfullscreen></iframe>';

                            ////console.log('watching video radio', $scope.videoDim.width, $scope.videoDim.height);
                        };
                        $scope.newVideo = { width: '', height: '' };
                        $scope.setVideoHeight = function (maxWidth) {
                            if ($scope.newVideo.width > maxWidth)
                            {
                                $scope.newVideo.width = maxWidth;
                            }
                            $scope.newVideo.height = Math.round($scope.newVideo.width * 0.5625);
                            $scope.watchVideoSize($scope.newVideo);
                        }
                        $scope.setVideoWidth = function (maxHeight) {
                            if ($scope.newVideo.height > maxHeight) {
                                $scope.newVideo.height = maxHeight;
                            }
                            $scope.newVideo.width = Math.round($scope.newVideo.height * 1.777);
                            $scope.watchVideoSize($scope.newVideo);

                        }
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getFileData(asset);
                //resizer
                $scope.setImageHeight = function (idx) {
                    ////console.log(idx);
                    var height_to_width = $scope.fileData.height / $scope.fileData.width;
                    $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * height_to_width);
                    $scope.newDim.height = Math.round($scope.resourceTools[idx].width * height_to_width);
                }
                $scope.setImageWidth = function (idx) {
                    var width_to_height = $scope.fileData.width / $scope.fileData.height;
                    $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * width_to_height);
                    $scope.newDim.width = Math.round($scope.resourceTools[idx].height * width_to_height);
                }
                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
                //CLIPBOARD            
                $scope.close = function () { modalInstance.close(); }
                $scope.embedCode = '<iframe width="560" height="315" src="' + $scope.trustedLink + '" frameborder="0" allowfullscreen></iframe>';

                $scope.getTextToCopy = function () {
                    return $scope.embedCode;
                }
                $scope.doSomething = function () {
                    ////console.log("NgClip...");
                }
                $scope.downloadPrintJPG = function (asset, width) {
                    //print types are percentage values
                    var size = width / 100;
                    ////console.log('size and width', size, width);
                    return window.location = '/api.ashx?action=downloadPrintJPG&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadPrintPDF = function (asset, width) {
                    //print types are percentage values
                    var size = width / 100;
                    ////console.log('size and width', size, width);
                    return window.location = '/api.ashx?action=downloadPrintPDF&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadImageJPG = function (asset, width) {
                    var size = width / $scope.fileData.width;
                    return window.location = '/api.ashx?action=downloadImageJPG&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadImagePDF = function (asset, width) {
                    var size = width / $scope.fileData.width;
                    return window.location = '/api.ashx?action=downloadImagePDF&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadFile = function (asset) {
                    return window.location = '/api.ashx?action=downloadFile&resourceID=' + asset.id;
                };
            },

            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            $scope.selected = selectedAsset;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    function init() {
        list();
        categoryFilter();
        themeFilter();
        
    }
    init();
    function list() {
        $scope.allMyAssets = [];

        var url = '/api.ashx',
        action = '?action=list'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.assets = data;
            for (var i = 0; i < $scope.assets.length; i++) {
                var lastFour = $scope.assets[i].filename.substr($scope.assets[i].filename.length - 4);
                switch (lastFour) {
                    case '.pdf':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case '.doc':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case 'docx':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case '.ppt':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case '.xls':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case 'xlsx':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                    case '.txt':
                        $scope.assets[i].path = '/images/file_icons/icon_pdf.png';
                        break;
                }
            }
            for (var i = 0; i < $scope.assets.length; i++) {
                $scope.allMyAssets.push($scope.assets[i]);
            }
            $scope.loadingData = checkLoading();
            //console.log($scope.currentPage, $scope.displayCount.value, $scope.assets.length);

            ////console.log('$scope.allMyAssets', $scope.allMyAssets);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getAsset = function (id) {
        $scope.modal = {};
        var url = '/api.ashx',
        action = '?action=getAsset',
        id = '&id=' + id,
        data = action + id;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.selectedAsset = data;
            ////console.log('$scope.modal', $scope.modal);
            ////console.log('$scope.modal', $scope.modal.faqs);
            $scope.modalUpdate('lg', $scope.selectedAsset);

            //$("#modal").dialog('open');
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });        
    }

    $scope.renderHtml = function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };

    //PAGING & SORTING
    $scope.sortField = undefined;
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSorthown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };

    //PLATFORM RADIO FILTERS
    function platformFilter() {
        var url = '/api.ashx',
        action = '?action=listPlatforms'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.platformOptions = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.platformSelection = [];
    $scope.selectedPlatforms = function() {        
    };
    $scope.watchPlatform = function () {
        ////console.log('platform ', $scope.platformOptions.Options.selected);
    };
    $scope.allPlatforms = function () {
        for (var i = 0; i < platformOptions.Options.length; i++) {
            $scope.platformOptions.Options[i].selected = false;
        }
    };

    //BUCKET CHECKBOX FILTER
    $scope.allMyBuckets = [];
    $scope.allMyBuckets2 = [];
    $scope.hasCategories = false;
    function categoryFilter() {
        var url = '/api.ashx',
        action = '?action=listCategories',
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            var stuff = data;
            if (stuff.length) {
                $scope.categories = data;
                $scope.hasCategories = true;
            }
            $scope.loadingData = checkLoading;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }

    getBuckets();
    function getBuckets() {
        var url = '/api.ashx',
        action = '?action=listBuckets',
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.searchBuckets = data;
            for (var i = 0; i < $scope.searchBuckets.length; i++) {
                $scope.allMyBuckets.push($scope.searchBuckets[i]);
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    
    $scope.allBuckets = true;
    $scope.filterBuckets = function (a) {
        for (buc in $scope.allMyBuckets) {
            var t = $scope.allMyBuckets[buc];
            if (t.selected && a.bucket.indexOf(t.title) > -1) {
                $scope.allBuckets = false;
                return true;
            }
            if ($scope.allBuckets===true)
            { return true; }
        }
       ////console.log('show all', $scope.allBuckets);
    };
    $scope.watchBucketSet = function (category) {
        ////console.log('watchBucketSet', category);
        if (category.selected) {
            for (var i = 0; i < $scope.allMyBuckets.length; i++) {
                var bucketCategoryID = $scope.allMyBuckets[i].bucketCategoryID;
                if (bucketCategoryID == category.categoryID) {
                    $scope.allMyBuckets[i].selected = true;
                    $scope.watchBucket($scope.allMyBuckets[i]);
                }
            }
        } else {
            for (var i = 0; i < $scope.allMyBuckets.length; i++) {
                var bucketCategoryID = $scope.allMyBuckets[i].bucketCategoryID;
                if (bucketCategoryID == category.categoryID) {
                    $scope.allMyBuckets[i].selected = false;
                }
            }
            $scope.allBuckets = true;
        }
    }
    $scope.watchBucket = function (bucket) {
        var check = true;
        for (var i = 0; i < $scope.allMyBuckets.length; i++) {
            ////console.log('watching', $scope.allMyBuckets[i].title, $scope.allMyBuckets[i].selected);

            if ($scope.allMyBuckets[i].selected) {
                $scope.allBuckets = false;
                check = false;
                break;
            }
        }
        $scope.allBuckets = check;
    };
    $scope.watchForAllBuckets = function () {
        if ($scope.allBuckets) {
            for (var i = 0; i < $scope.categories.length; i++) {
                $scope.categories[i].selected = false;
            }
        }
    }


    //THEME CHECKBOX FILTER
    $scope.allMyThemes = [];
    function themeFilter() {
        var url = '/api.ashx',
        action = '?action=listThemes'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.themeOptions = data;
            for (var i = 0; i < $scope.themeOptions.length; i++) {
                $scope.allMyThemes.push($scope.themeOptions[i]);
            }
            $scope.loadingData = checkLoading();
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.allThemes = true;
    $scope.filterThemes = function (a) {
        for (opt in $scope.allMyThemes) {
            var t = $scope.allMyThemes[opt];

                if (t.selected && a.tags.toLowerCase().indexOf(t.theme.toLowerCase()) > -1) {
                    $scope.allThemes = false;
                    return true;
                }
                if ($scope.allThemes === true)
                { return true; }
            
        }
        ////console.log('show themes', $scope.allThemes);
    };
    $scope.watchTheme = function (opt) {
        var check = true;
        for (var i = 0; i < $scope.allMyThemes.length; i++) {
            ////console.log('themey', $scope.allMyThemes[i].selected);

            if ($scope.allMyThemes[i].selected) {
                $scope.allThemes = false;
                check = false;
                break;
            }
        }
        $scope.allThemes = check;
    };

});

DAM.directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});
