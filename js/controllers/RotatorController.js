﻿DAM.controller('RotatorController', function ($scope, $http, $location,$sce, $modal, $log, $window) {
    //console.log('rotator');
    //ROTATOR MODAL
    $scope.viewResource = function (resourceID, bucketID) {
        jQuery('.carousel').slick('slickPause');
        $scope.getRotatorAsset = function (resourceID) {
            var url = '/api.ashx',
            action = '?action=getAsset&id=' + resourceID;
            $http({
                method: 'GET',
                url: url + action,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                $scope.rotatorAsset = data;
                $scope.resourceModal('lg', $scope.rotatorAsset);
            }).
            error(function (data, status, headers, config) {
                $scope.error = true;
            });
        };
        $scope.getRotatorAsset(resourceID);

    };
    $scope.resourceModal = function (size, asset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/RotatorModal.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
               
                $scope.asset = asset;
                //console.log('rotator modal', $scope.asset);

                //FILE DATA
                $scope.getFileData = function (asset) {
                    var url = '/api.ashx',
                    action = '?action=get_file_data&resourceID=' + asset.id;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.fileData = data;
                        //console.log('file data', $scope.fileData);
                        $scope.newDim = {};
                        $scope.newDim.width = 150;

                        $scope.$watchCollection('newDim', function () {
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            //console.log('watching neDim model', $scope.newDim.width);
                        });
                        $scope.selectedImageTool = null;
                        $scope.watchImageOption = function (tool) {
                            $scope.newDim.width = tool.width;
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            //console.log('watching radio', $scope.newDim.width);
                        };
                        $scope.$watchCollection('selectedImageTool', function () {
                            $scope.newDim.width = $scope.selectedImageTool;
                            $scope.newDim.height = Math.round($scope.newDim.width * ($scope.fileData.height / $scope.fileData.width));
                            //console.log('watching width', $scope.newDim.width);
                        });
                        //PRINT MEDIA DIMENSIONS
                        $scope.printDim = { width: 100 };
                        $scope.watchPrintOption = function (tool) {
                            $scope.printDim.width = tool.width;
                            //console.log('watching print radio', $scope.printDim.width);
                        };

                        //VIDEO DIMENSIONS
                        $scope.showCustomVideoSize = false;
                        $scope.videoDim = { width: 560, height: 315 };
                        $scope.watchVideoSize = function (tool) {
                            if (tool.toolID == 12) { $scope.showCustomVideoSize = true; }
                            $scope.videoDim.width = tool.width;
                            $scope.videoDim.height = tool.height;
                            $scope.embedCode = '<iframe width="' + $scope.videoDim.width + '" height="' + $scope.videoDim.height + '" src="' + $scope.trustedLink + '" frameborder="0" allowfullscreen></iframe>';

                            //console.log('watching video radio', $scope.videoDim.width, $scope.videoDim.height);
                        };
                        $scope.newVideo = { width: '', height: '' };
                        $scope.setVideoHeight = function (maxWidth) {
                            if ($scope.newVideo.width > maxWidth) {
                                $scope.newVideo.width = maxWidth;
                            }
                            $scope.newVideo.height = Math.round($scope.newVideo.width * 0.5625);
                            $scope.watchVideoSize($scope.newVideo);
                        }
                        $scope.setVideoWidth = function (maxHeight) {
                            if ($scope.newVideo.height > maxHeight) {
                                $scope.newVideo.height = maxHeight;
                            }
                            $scope.newVideo.width = Math.round($scope.newVideo.height * 1.777);
                            $scope.watchVideoSize($scope.newVideo);

                        }
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getFileData(asset.asset);
                $scope.setTab = function (tab) {
                    switch (tab) {
                        case 'preview':
                            $scope.previewTab = true;
                            $scope.downloadTab = false;

                            break;
                        case 'download':
                            $scope.previewTab = false;
                            $scope.downloadTab = true;

                            break;
                    }

                };
                //$scope.setTab('preview');
                ////console.log('$scope.asset', $scope.asset);
                //video links
                $scope.trustLink = function (link) {
                    $scope.trustedLink = $sce.trustAsResourceUrl(link);
                };
                //$scope.trustLink($scope.asset.link);

                //FAQS
                $scope.getResourceQuestions = function () {
                    var url = '/api.ashx',
                        action = '?action=list_resource_faqs&resourceID=' + asset.asset.id + '&bucketID=' + asset.asset.bucketID;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.resourceQuestions = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                //TOOLS
                $scope.getResourceTools = function () {

                    var url = '/api.ashx',
                        action = '?action=list_resource_tools&resourceID=' + asset.asset.id + '&bucketID=' + asset.asset.bucketID;
                    $http({
                        method: 'GET',
                        url: url + action,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.resourceTools = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.getResourceQuestions();
                $scope.getResourceTools();
                //resizer
                $scope.setImageHeight = function (idx) {
                    //console.log(idx);
                    var height_to_width = $scope.fileData.height / $scope.fileData.width;
                    $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * height_to_width);
                    $scope.newDim.height = Math.round($scope.resourceTools[idx].width * height_to_width);
                }
                $scope.setImageWidth = function (idx) {
                    var width_to_height = $scope.fileData.width / $scope.fileData.height;
                    $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * width_to_height);
                    $scope.newDim.width = Math.round($scope.resourceTools[idx].height * width_to_height);
                }
                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
                //CLIPBOARD            
                $scope.close = function () { modalInstance.close(); }
                $scope.embedCode = '<iframe width="560" height="315" src="' + $scope.trustedLink + '" frameborder="0" allowfullscreen></iframe>';

                $scope.getTextToCopy = function () {
                    return $scope.embedCode;
                }
                $scope.doSomething = function () {
                    //console.log("NgClip...");
                }
                $scope.downloadPrintJPG = function (asset, width) {
                    //print types are percentage values
                    var size = width / 100;
                    //console.log('size and width', size, width);
                    return window.location = '/api.ashx?action=downloadPrintJPG&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadPrintPDF = function (asset, width) {
                    //print types are percentage values
                    var size = width / 100;
                    //console.log('size and width', size, width);
                    return window.location = '/api.ashx?action=downloadPrintPDF&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadImageJPG = function (asset, width) {
                    var size = width / $scope.fileData.width;
                    return window.location = '/api.ashx?action=downloadImageJPG&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadImagePDF = function (asset, width) {
                    var size = width / $scope.fileData.width;
                    return window.location = '/api.ashx?action=downloadImagePDF&resourceID=' + asset.id + '&size=' + size;
                };
                $scope.downloadFile = function (asset) {
                    return window.location = '/api.ashx?action=downloadFile&resourceID=' + asset.id;
                };
            },

            size: size,
            resolve: {
                asset: function () {
                    return asset;
                }
            }
        });

        modalInstance.result.then(function (asset) {
            $scope.asset = asset;
        }, function () {
            $scope.list();
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
});
