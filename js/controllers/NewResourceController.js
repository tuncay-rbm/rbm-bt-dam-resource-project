﻿DAM.controller('NewResourceController', function ($scope, $http, $location, $sce, $modal, $log, $window, FileUploader, $timeout) {

    $scope.platform_title = jQuery('#hdPlatform').val();
    $scope.platform_id = jQuery('#hdPlatformID').val();
    $scope.platform_url = jQuery('#hdPlatformUrl').val();
    //DELETE CONFIRMATION MODAL
    //console.log('here!', $scope.platform_title, $scope.platform_id);
    $scope.setUserID = function (userID) {
        $scope.userID = userID;
        //console.log('userID', userID);
    }
    $scope.tags = [];
    $scope.loadTags = function () {
        var autoTags = [];
        var url = '/api.ashx',
        action = '?action=list_autocomplete_tags'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.loadedTags = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.loadTags();
    //LOAD RESOURCE OPTIONS
    $scope.listBuckets = function () {
        var url = '/api.ashx',
            action = '?action=list_buckets';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.buckets = data;
            $scope.selectedBucket = $scope.buckets[0];
        }).
        error(function (data, status, headers, config) { 
            $scope.error = true;
        });
    }
    $scope.listBucketTools = function () {
        var url = '/api.ashx',
            action = '?action=list_bucket_tools&bucketID=' + $scope.selectedBucket.bucket.id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.bucketTools = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.listPlatforms = function () {
        var url = '/api.ashx',
            action = '?action=list_platforms';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.platforms = data;
            $scope.hasPlatform = true;
            $scope.selectedPlatform = { platform: $scope.platform_title };
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getAllQuestions = function () {
        var url = '/api.ashx',
            action = '?action=list_faqs';
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.allQuestions = data;
            $scope.includedQuestion = $scope.allQuestions[0];
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
        //WATCH OPTION SELECTIONS
    $scope.watchPlatform = function () {
        $scope.hasPlatform = true;
        //console.log($scope.selectedPlatform.platform.platform);
    }
    $scope.watchBucket = function () {
        //console.log('idx', $scope.selectedBucket.bucket.id);
        $scope.uploader = {};
        $scope.hasBucket = true;
        $scope.listBucketTools();
        $scope.getResourceQuestions();
        $scope.getResourceTools();
        $scope.uploader = new FileUploader({
            url: '/api.ashx?action=save_file',
            removeAfterUpload: true,
            //autoUpload: true,
            onSuccessItem: function (item, response, status, headers) {
                $scope.fileData = response;
                $scope.selectedAsset.path = '/_resources/' + $scope.platform_title + '/' + $scope.selectedBucket.bucket.title + '/' + $scope.fileData.filename;
                $scope.hasFile = true;
            },
            formData: [{ "platform": $scope.platform_title, "bucket": $scope.selectedBucket.bucket.title }],
            queueLimit: 1,
        });
    }
    $scope.getResourceQuestions = function (asset) {
        var url = '/api.ashx',
            action = '?action=list_resource_faqs&resourceID=' + '&bucketID=' + $scope.selectedBucket.bucket.id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.resourceQuestions = data;
            if ($scope.resourceQuestions.length) {

            } else { $scope.resourceQuestions = [];}
        
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.getResourceTools = function () {
        var url = '/api.ashx',
            action = '?action=list_resource_tools&resourceID=&bucketID=' + $scope.selectedBucket.bucket.id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            // $scope.resourceTools = data;
            //HANDLE PRE SET WIDTHS
            var tools = data;
            if (tools.length) {
                for (var i = 0; i < tools.length; i++) {
                    switch (tools[i].toolID) {
                        case 1:
                            tools[i].width = 384;
                            tools[i].height = 216;
                            break;
                        case 2:
                            tools[i].width = 640;
                            tools[i].height = 360;
                            break;
                        case 3:
                            tools[i].width = 896;
                            tools[i].height = 504;
                            break;
                        case 12:
                            tools[i].width = 384;
                            tools[i].height = 216;
                            break;
                        case 19:
                            tools[i].width = 25;
                            break;
                        case 20:
                            tools[i].width = 50;
                            break;
                        case 21:
                            tools[i].width = 100;
                            break;
                    }
                }
            }
            $scope.resourceTools = tools;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    //VIDEO TOOLS
    $scope.setVideoHeight = function (idx) {
        $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * 0.5625);
    }
    $scope.setVideoWidth = function (idx) {
        $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * 1.777);
    }
    //PRINT MEDIA TOOLS
    $scope.setPrintMediaHeight = function (idx) {
        var wid = $scope.resourceTools[idx].width;
        if (wid > 100) { $scope.resourceTools[idx].width = 100 };
    }

    //IMAGE TOOLS
    $scope.setImageHeight = function (idx) {
        var height_to_width = $scope.fileData.height / $scope.fileData.width;
        $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * height_to_width);
    }
    $scope.setImageWidth = function (idx) {
        var width_to_height = $scope.fileData.width / $scope.fileData.height;
        $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * width_to_height);
    }


    $scope.listBuckets();
    $scope.listPlatforms();
   // $scope.getResourceTools(asset);

    $scope.getAllQuestions();
   // $scope.trustLink(asset.link);
    $scope.selectedAsset = {};

    //video links
    $scope.trustLink = function (link) {
        $scope.trustedLink = $sce.trustAsResourceUrl(link);
    };

    //TAGS


    $scope.addQuestion = function () {
        var idx = $scope.includedQuestion.opt;
        $scope.includedQuestion = $scope.allQuestions[idx];
        var addQuestion = {
            questionID: $scope.includedQuestion.id,
            question: $scope.includedQuestion.question,
            selected:true
        }
        $scope.resourceQuestions.push(addQuestion);
        //console.log('questions', $scope.resourceQuestions, idx);
    }
    $scope.removeFAQ = function (idx) {
        $scope.resourceQuestions.splice(idx, 1);

        //console.log('removing', $scope.resourceQuestions);
    };
    $scope.setHeight = function (idx) {
        $scope.resourceTools[idx].height = Math.round($scope.resourceTools[idx].width * 0.5625);
    }
    $scope.setWdith = function (idx) {
        $scope.resourceTools[idx].width = Math.round($scope.resourceTools[idx].height * 1.777);
    }
    $scope.cancelEditMode = function () {
        $scope.editMode = false;
    }
    $scope.canSave = false;
    $scope.tool17 = false;
    $scope.tool18 = false;
    $scope.tool22 = false;
    $scope.tool23 = false;
    $scope.watchToolSelection = function (tool) {
        switch (tool.toolID) {
            case 17:
                $scope.tool17 = tool.selected;
                break;
            case 18:
                $scope.tool18 = tool.selected;
            case 22:
                $scope.tool22 = tool.selected;
                break;
            case 23:
                $scope.tool23 = tool.selected;
        }
        if ($scope.tool17 || $scope.tool18 || $scope.tool22 || $scope.tool23) {
            $scope.canSave = true;
        }
        else {
            $scope.canSave = false;

        }

    };
    $scope.saveMessage = false;

    $scope.saveEdits = function () {
        var valid = true;
        if ($scope.selectedBucket.bucket.bucketTypeID == 2 || $scope.selectedBucket.bucket.bucketTypeID == 4) {
            valid = $scope.canSave;
        }
        if (valid) {
            //console.log('SAVING!');
            var description = $scope.selectedAsset.description == 'undefined' || $scope.selectedAsset.description == undefined ? '' : $scope.selectedAsset.description;
            $scope.selectedAsset.description = description;
            $scope.saveMessage = true;
            $scope.selectedAsset.id = "";
            $scope.selectedAsset.bucketID = $scope.selectedBucket.bucket.id;
            $scope.selectedAsset.title = $scope.selectedAsset.title;
            $scope.selectedAsset.description = $scope.selectedAsset.description;
            $scope.selectedAsset.filename = $scope.selectedBucket.bucket.bucketTypeID == 1 ? '':$scope.fileData.filename;
            //$scope.selectedAsset.platformID = $scope.selectedPlatform.platform.id;
            $scope.selectedAsset.platformID = $scope.platform_id;

            $scope.selectedAsset.saveDate = "";
            $scope.selectedAsset.saveBy = $scope.userID;
            $scope.selectedAsset.editDate = "";
            $scope.selectedAsset.editBy = $scope.userID;
            $scope.selectedAsset.downloads = "0";
            $scope.selectedAsset.shares = "0";
            $scope.selectedAsset.bucket = $scope.selectedBucket.bucket.title;
            $scope.selectedAsset.path = '/_resources/' + $scope.selectedPlatform.platform.platform;
            if (!$scope.selectedAsset.link) {
                $scope.selectedAsset.link = '';
            }
            $scope.selectedAsset.linkImage = "";

            //console.log('saving...', $scope.selectedAsset, $scope.resourceTools, $scope.resourceQuestions);
            jsonObject = $scope.selectedAsset;
            json = angular.toJson(jsonObject);
            //console.log('json', json);
            var url = '/api.ashx',
            action = '?action=save_asset';
            $http({
                method: 'POST',
                url: url + action,
                data: "data=" + encodeURIComponent(json),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).
            success(function (data, status, headers, config) {
                $scope.savedAsset = data;
                $scope.saveTools($scope.savedAsset.id);
                $scope.saveFaq($scope.savedAsset.id);
                $scope.saveTags($scope.savedAsset.id);

                //console.log('res', $scope.savedAsset.id);
            }).
            error(function (data, status, headers, config) {
                $scope.error = true;
            });
        } else {
            $scope.buttonWarning = true;
            //console.log('WARNING');
        }


    }
    $scope.saveTools = function (id) {
        var url = '/api.ashx?action=clear_resource_tools&resourceID='+ id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.resourceTools.length) {
                for (var i = 0; i < $scope.resourceTools.length; i++) {
                    if ($scope.resourceTools[i].selected) {
                        var tool = {
                            id: '',
                            toolID: $scope.resourceTools[i].toolID,
                            resourceID: id,
                            width: $scope.resourceTools[i].width,
                            height: $scope.resourceTools[i].height,
                            isTrue: true,
                        };
                        json = angular.toJson(tool);
                        //console.log('tool', json);
                        var url = '/api.ashx',
                        action = '?action=save_resource_tools';
                        $http({
                            method: 'POST',
                            url: url + action,
                            data: "data=" + json,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).
                        success(function (data, status, headers, config) {
                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                    }
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    };
    $scope.saveFaq = function (id) {
        var url = '/api.ashx?action=clear_resource_questions&resourceID=' + id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.resourceQuestions.length) {
                for (var i = 0; i < $scope.resourceQuestions.length; i++) {
                    var faq = {
                        id: '',
                        questionID: $scope.resourceQuestions[i].questionID,
                        resourceID: id
                    };
                    json = angular.toJson(faq);
                    //console.log('faq', json);
                    var url = '/api.ashx',
                    action = '?action=save_resource_questions';
                    $http({
                        method: 'POST',
                        url: url + action,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    };
    $scope.saveTags = function (id) {
        var url = '/api.ashx?action=clear_resource_tags&resourceID=' + id;
        $http({
            method: 'POST',
            url: url,
            //data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            if ($scope.tags.length) {
                for (var i = 0; i < $scope.tags.length; i++) {
                    var tag = {
                        id: '',
                        resourceID: id,
                        tag: $scope.tags[i].text,
                        platformID: $scope.platform_id
                    };
                    json = angular.toJson(tag);
                    //console.log('tag', json);
                    var url = '/api.ashx',
                    action = '?action=save_resource_tags';
                    $http({
                        method: 'POST',
                        url: url + action,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        // $window.location.href = '/';
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
            }
            $scope.spintime();

        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });          
    };
    $scope.spintime = function () {
        $timeout(function () {
            $window.location.href = '/' + $scope.platform_url;
        }, 2000);
    };
});
