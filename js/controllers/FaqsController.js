﻿DAM.controller('FaqsController', function ($scope, $http, $location,$sce, $modal, $log, $window) {
    //console.log('here!');

    //ACCORDION
    $scope.oneAtATime = true; 
    $scope.openAccordion = function (idx) {

    }
    $scope.customQ = false;
    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.setClass = function (categoryID) {
        $scope.activeCategory = categoryID;
    };
    $scope.getClass = function (categoryID) {
        if ($scope.activeCategory == categoryID) {
            return true;
        } else { return false;}
    }
    $scope.filterQuestions = function (questionID) {
        $scope.customQ = true;
        //console.log('filtering', questionID);
        for (var i = 0; i < $scope.faqs.length; i++) {
            if ($scope.faqs[i].id === questionID) {
                $scope.selectedQuestion = $scope.faqs[i];
                //console.log('$scope.faqs', $scope.faqs[i].id);
            }
        }
    }

    //DATA
    function init() {
        list();
        listFaqCategories();
    }
    init();

    function list() {
        var url = '/api.ashx',
        action = '?action=listFaqs'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.faqs = data;
            $scope.selectedQuestion = $scope.faqs[0];
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.openStatus = {
        openQuestion: null
    }
    function listFaqCategories() {
        var url = '/api.ashx',
        action = '?action=listFaqCategories'
        data = action;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.faqCategories = data;
            var target = $window.location.search;
            if (target != '') {
                target = target.replace('?id=', '');
                for (var i = 0; i < $scope.faqs.length; i++) {
                    var match = $scope.faqs[i].id;
                    if (target == match) {
                        var catID = $scope.faqs[i].categoryID;
                        for (var y = 0; y < $scope.faqCategories.length; y++) {
                            var targetCategory = $scope.faqCategories[y].id;
                            if (catID == targetCategory) {
                                $scope.filterQuestionSet($scope.faqCategories[y]);
                                $scope.setClass($scope.faqCategories[y].id);
                                $scope.filterQuestions(target * 1);
                                $scope.openStatus.openQuestion = target * 1;
                                //console.log('target', target);
                                break;
                            }
                        }
                    } 
                }
            } else {
                $scope.filterQuestionSet($scope.faqCategories[0]);
                $scope.setClass($scope.faqCategories[0].id);

            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }

    $scope.filterQuestionSet = function (category) {
        $scope.selectedCategory = category.id;
        $scope.categoryTitle = category.title;
    }
    $scope.selectQuestion = function (idx) {
        $scope.selectedQuestion = $scope.faqs[0];
    };
    $scope.renderHtml = function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };

    //PAGING & SORTING
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.sortField = undefined;
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };
    $scope.changePage = function (newPageNumber) {
        //console.log('changing pages...', newPageNumber);
    };
    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSortDown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };

});
