﻿
DAM.controller('StatsController', function ($scope,$http,$filter) {
    //console.log('here!');
    // Pagination Variables
    $scope.itemsPerPage = 50;
    $scope.currentPage = 1;
    $scope.filteredItems = [];
    $scope.pagedItems = [];
    $scope.dataShow = false;
    $scope.vsPrevPage = false;
    $scope.vsNextPage = true;
    $scope.filterErr = false;

    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
    };
    $scope.nextPage = function () {
        $scope.currentPage = $scope.currentPage + 1;
        if ($scope.currentPage == $scope.pagedItems.length)
            $scope.vsNextPage = false;
        else
            $scope.vsNextPage = true;
        $scope.vsPrevPage = true;


        $scope.lstPages = $scope.pages[$scope.currentPage - 1];
    }
    $scope.prevPage = function () {
        $scope.currentPage = $scope.currentPage - 1;
        if ($scope.currentPage == 0)
            $scope.vsPrevPage = false;
        else
            $scope.vsPrevPage = true;
        $scope.vsNextPage = true;

        $scope.lstPages = $scope.pages[$scope.currentPage - 1];
    }

    $scope.selectPage = function () {
        $scope.currentPage = $scope.lstPages.value;
        if ($scope.currentPage == 0)
            $scope.vsPrevPage = false;
        else
            $scope.vsPrevPage = true;

        if ($scope.currentPage == $scope.pagedItems.length)
            $scope.vsNextPage = false;
        else
            $scope.vsNextPage = true;
    }
    var toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
    }
    var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        if (toType(haystack) != 'string') {
            return String(haystack).indexOf(String(needle)) !== -1;
        } else {
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
        }
    };

    $scope.search = function () {
        $scope.listView = true;

        $scope.filteredItems = $filter('filter')($scope.data, function (item) {
            for (var attr in item) {
                if (searchMatch(item[attr], $scope.keyword))
                    return true;
            }
            return false;
        });

        $scope.currentPage = 1;
        $scope.groupToPages($scope.filteredItems, $scope.itemsPerPage);
    };
    //$scope.list_stats = function () {
    //    var url = '/api.ashx',
    //    action = '?action=list_stats',
    //    data = action;
    //    $http({
    //        method: 'GET',
    //        url: url + data,
    //        headers: { 'Content-Type': 'text/plain' }
    //    }).
    //    success(function (data, status, headers, config) {
    //        $scope.stats = data;
    //        $scope.filteredItems = data;
    //        $scope.groupToPages($scope.filteredItems, $scope.itemsPerPage);
    //        $scope.pages = [];
    //        for (var i = 0; i < $scope.pagedItems.length; i++) {
    //            $scope.pages.push({ value: i + 1, text: i + 1 + ' of ' + $scope.pagedItems.length });
    //        }
    //        $scope.lstPages = $scope.pages[0];

    //        $scope.dataShow = true;
    //    }).
    //    error(function (data, status, headers, config) {
    //        $scope.error = true;
    //    });
    //};
    //$scope.list_stats();
    $http({ method: 'GET', url: '/api.ashx?action=list_stats' }).
    success(function (data, status, headers, config) {
        $scope.data = data;
        $scope.filteredItems = data;
        $scope.groupToPages($scope.filteredItems, $scope.itemsPerPage);
        $scope.pages = [];
        for (var i = 0; i < $scope.pagedItems.length; i++) {
            $scope.pages.push({ value: i + 1, text: i + 1 + ' of ' + $scope.pagedItems.length });
        }
        $scope.lstPages = $scope.pages[0];

        $scope.dataShow = true;
    }).
    error(function (data, status, headers, config) {
        console.log('http error');
    }
);
    //PAGING & SORTING
    $scope.sortField = undefined;
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSorthown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };

    // Filter Months
    $scope.months = [];
    $scope.months.push({ value: "", text: "Month" });
    $scope.months.push({ value: 1, text: "January" });
    $scope.months.push({ value: 2, text: "February" });
    $scope.months.push({ value: 3, text: "March" });
    $scope.months.push({ value: 4, text: "April" });
    $scope.months.push({ value: 5, text: "May" });
    $scope.months.push({ value: 6, text: "June" });
    $scope.months.push({ value: 7, text: "July" });
    $scope.months.push({ value: 8, text: "August" });
    $scope.months.push({ value: 9, text: "September" });
    $scope.months.push({ value: 10, text: "October" });
    $scope.months.push({ value: 11, text: "November" });
    $scope.months.push({ value: 12, text: "December" });
    $scope.filterMonth = $scope.months[0];

    //Filter Years
    $scope.years = [];
    $scope.years.push({ value: "", text: "Year" });
    for (var i = new Date().getFullYear() ; i >= 2010 ; i--) {
        $scope.years.push({ value: i, text: i });
    }
    $scope.filterYear = $scope.years[0];
    $scope.filterByDate = function () {
        if ($scope.filterMonth.value != '' && $scope.filterYear.value != '') {
            $scope.listView = true;//Hide detail view and show the list
            console.log('$scope.listView', $scope.listView);
            $scope.filterErr = false;

            $scope.filteredItems = $filter('filter')($scope.data, function (item) {
                if (new Date(item.statDate).getMonth() + 1 == $scope.filterMonth.value && new Date(item.statDate).getFullYear() == $scope.filterYear.value)
                    return true;
                return false;
            });

            $scope.currentPage = 1;
            $scope.groupToPages($scope.filteredItems, $scope.itemsPerPage);
        } else
            $scope.filterErr = true;

    }
    $scope.clearFilters = function () {
        location.reload();
    }
});
