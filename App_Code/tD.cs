﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Responses;


public partial class rbm
{
	public class stats
	{
		private class stStat
		{
			public string city, state;
			public DateTime statDate;

			public stStat(CityResponse response)
			{
				city = response.City.Name;
				state = response.MostSpecificSubdivision.Name;
				statDate = DateTime.Today;
			}

			public stStat()
			{
			}
		}

		public static void parseAndSave(string platformID, string resourceID, string IP)
		{
			stStat obj;
			try
			{
				var client = new WebServiceClient(Convert.ToInt32(getSett("mindmaxUser")), getSett("mindmaxKey"));

				// Do the lookup
				var response = client.City(IP);
				obj = new stStat(response);
			}
			catch (Exception)
			{
				obj = new stStat{city = "-", state = "-", statDate = DateTime.Today};
			}
			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				using (SqlCommand cmd = new SqlCommand("dbo.saveStat", conn))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.AddWithValue("@platformID", platformID);
					cmd.Parameters.AddWithValue("@resourceID", resourceID);
					cmd.Parameters.AddWithValue("@city", obj.city);
					cmd.Parameters.AddWithValue("@state", obj.state);
					cmd.Parameters.AddWithValue("@statDate", obj.statDate);
					conn.Open();
					cmd.ExecuteNonQuery();
				}
			}

		}
	}
}