﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class kb
{
	public class platform
	{
		public struct stPlatforms
		{
			public string id, title, url, path;
		}

		public static Dictionary<string, stPlatforms> getPlatforms()
		{
			stPlatforms obj;
			Dictionary<string, stPlatforms> dictPlatforms = new Dictionary<string, stPlatforms>();
			using (SqlConnection conn = new SqlConnection(rbm.getConn()))
			{
				using (SqlCommand cmd = new SqlCommand("Select * From platform Order by id", conn))
				{
					conn.Open();
					using (SqlDataReader dr = cmd.ExecuteReader())
					{
						while (dr.Read())
						{
							obj = new stPlatforms {id = Convert.ToString(dr["id"]), title = Convert.ToString(dr["platform"]), url = Convert.ToString(dr["url"]), path = Convert.ToString(dr["path"])};
							dictPlatforms.Add(obj.url, obj);
						}
					}
				}
			}
			return dictPlatforms;
		}
	}
}