﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using ImageResizer;
using Persits.PDF;
using DataTable = System.Data.DataTable;
using System.Drawing.Imaging;
using System.Xml;
using System.Globalization;
using HttpCookie = System.Web.HttpCookie;

public partial class rbm{
	#region common

	public static void log(string msg)
	{
        /*using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("/_log.txt"), true))
        {
            writetext.WriteLine(msg);
        }*/
	}

	public static string getPlatformName()
	{
		return HttpContext.Current.Request.QueryString["platformName"];
	}

	public static string getConn()
    { return ConfigurationManager.ConnectionStrings["sqlConn"].ConnectionString; }
	public static string getSett(string sett)
	{ return ConfigurationManager.AppSettings[sett]; }

	public static string getPath()
	{
		kb.platform.stPlatforms platform = getPlatform();
		return ConfigurationManager.AppSettings["path_for_files"] + "/" + platform.id + "-" + platform.path;
	}

	private static kb.platform.stPlatforms getPlatform(string platformName = "")
	{
		Dictionary<string, kb.platform.stPlatforms> dictPlatforms = (Dictionary<string, kb.platform.stPlatforms>)HttpContext.Current.Application["platforms"] ?? kb.platform.getPlatforms();
		//string dom = HttpContext.Current.Request.Url.Host.ToLower(); //works with subdomain
		string dom = isNull(platformName) ? HttpContext.Current.Request.QueryString["platformName"] : platformName;

		return isNull(dom) ? dictPlatforms.ElementAtOrDefault(0).Value : (dictPlatforms.ContainsKey(dom) ? dictPlatforms[dom] : new kb.platform.stPlatforms { id = "0" });
	}

	public static string getPlatformID(string platformName = "")
	{
		return getPlatform(platformName).id;
	}

	public static string getPath(bool def)
	{
		return ConfigurationManager.AppSettings["path_for_files"] + "/0-default";
	}

	public static string getCss()
	{
		kb.platform.stPlatforms platform = getPlatform();
		return "/css/" + platform.id + "-" + platform.path + ".css";
	}
	
	public static string getLogo()
	{
		kb.platform.stPlatforms platform = getPlatform();
		return "/images/logos/" + platform.id + "-" + platform.path + ".jpg";
	}

	public static string toStr(object obj)
	{ return Convert.ToString(obj); }
	public static Boolean isNull(object obj){
		try
		{ return string.IsNullOrEmpty(Convert.ToString(obj)); }
		catch (Exception)
		{ return false; }
	}
	public static string getUrl(string section, string page = "") {
		return (section == "home" && isNull(page)) ? "/": "/" + section + "/" + page;
	}
	public static void fill(DropDownList lst, DataSet ds, string valueField, string textField){
		lst.DataTextField = textField;
		lst.DataValueField = valueField;
		lst.DataSource = ds;
		lst.DataBind();
	}
    public static void fillCheckbox(CheckBoxList lst, DataSet ds, string valueField, string textField)
    {
        lst.DataTextField = textField;
        lst.DataValueField = valueField;
        lst.DataSource = ds;
        lst.DataBind();
    }
	public static string userFriendlyShortDate(object dt){
		return isNull(toStr(dt)) ? "" : userFriendlyShortDate(Convert.ToDateTime(dt));
	}
	public static string userFriendlyShortDate(DateTime dt){
		return dt.ToString("MMM") + " " + dt.ToString("dd") + ", " + dt.Year;
	}
	public static string userFriendlyShortDateTime(object dt){
		return (isNull(toStr(dt))) ? "": userFriendlyShortDateTime(Convert.ToDateTime(dt));
	}
	public static string userFriendlyShortDateTime(DateTime dt){
        return dt.ToString("MMM") + " " + dt.ToString("dd") + ", " + dt.Year + " " + dt.ToString("hh") + ":" + dt.ToString("mm") + " " + dt.ToString("tt");
	}
	public static string formatDateTime(string dt){
        return formatDateTime(Convert.ToDateTime(dt));
	}
	public static string formatDateTime(DateTime dt){
        return dt.ToString("MM") + "-" + dt.ToString("dd") + "-" + dt.Year;
	}
	public static void writeCookie(string key, string value){
		HttpCookie cookie = new HttpCookie(key);
        cookie.Value = value;
        cookie.Expires = DateTime.Now.AddMonths(3);
        HttpContext.Current.Response.Cookies.Add(cookie);
	}
	public static string readCookie(string key) {
		HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
		return (cookie != null) ? cookie.Value : "";
	}
	public static string sanitize(string input) {
		Regex rgx = new Regex("[^a-zA-Z0-9 @.-]");
		return rgx.Replace(input, "");
	}
	public static string ds2json(DataSet ds){ 
		return (ds.Tables[0].Rows.Count > 0)? JsonConvert.SerializeObject(ds.Tables[0]): "{\"err\":\"No record found\"}";
	}
    public static string obj2json(object obj, bool ignoreNull = true)
    {
        if (!ignoreNull)
        {
            JsonSerializerSettings sett = new JsonSerializerSettings();
            sett.NullValueHandling = NullValueHandling.Include;
            return JsonConvert.SerializeObject(obj, sett);
        }
        return JsonConvert.SerializeObject(obj);
    }

	public static string _get(string param) {
		return HttpContext.Current.Request.QueryString[param];
	}
	public static void echo(string content) {
		HttpContext.Current.Response.Write(content);
	}
	public static void die(){
		HttpContext.Current.Response.End();
	}
	private static object dataFix(string data)
	{ return isNull(data) ? null : data; }
	
	public static string getCommandArgument(object sender){
		return ((LinkButton)sender).CommandArgument;
	}
    public static object getStatus(object status)
    {
        return toStr(status) == "1" ? "Visible" : "Hidden";
    }
    public static object getAdmin(object status)
    {
        return toStr(status) == "1" ? "Yes" : "---";
    }
    public static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static DataTable RemoveNulls(DataTable dt)
    {
        for (int a = 0; a < dt.Rows.Count; a++)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Rows[a][i] == DBNull.Value)
                {
                    dt.Rows[a][i] = "";
                }
            }
        }

        return dt;
    }
    //converts empty strings in table to dbNUll
    public static DataTable makeNulls(DataTable dt)
    {
        for (int a = 0; a < dt.Rows.Count; a++)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (rbm.toStr(dt.Rows[a][i]) == "")
                {
                    dt.Rows[a][i] = DBNull.Value;
                }
            }
        }

        return dt;
    }
    public static string fixPath(object path)
    {
        string _path = rbm.toStr(path);
        _path = _path.Replace(HttpContext.Current.Server.MapPath("/"), "");
        _path = _path.Replace(@"\", "/");
        return "/" + _path;
    }
	#endregion

    public class member
    {
        public struct stMembers
        {
            public string id, name, email, password, status, saveDate;
            public bool isAdmin;
        }
        public static stMembers login(string email, string passWord)
        {
            stMembers obj = new stMembers { id = "0" };
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand comm = new SqlCommand("Select * From members Where status = 1 and email = @email", conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@email", email);
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        if (!dr.Read()) return obj;
                        if (Convert.ToString(dr["passWord"]) != passWord) return obj;
                        obj.id = Convert.ToString(dr["id"]);
                        obj.name = Convert.ToString(dr["name"]);
                        obj.isAdmin = Convert.ToBoolean(dr["isAdmin"]);
                    }
                }
            }
            return obj;
        }
        public static stMembers getMember(string id)
        {
            stMembers obj = new stMembers();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From members Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.name = Convert.ToString(dr["name"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.password = Convert.ToString(dr["password"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                            obj.isAdmin = Convert.ToBoolean(dr["isAdmin"]);
                        }
                    }
                }
            }
            return obj;
        }
        public static Boolean isExist(string email)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand comm = new SqlCommand("Select id From members Where email = @email", conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@email", email);
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        return dr.Read();
                    }
                }
            }
        }
        public static void update(stMembers obj, bool limited)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                StringBuilder Sql = new StringBuilder("Update members Set name = @name, email = @email");
                if (!string.IsNullOrEmpty(obj.password))
                { Sql.Append(", passWord = @passWord"); }
                if (!limited)
                {
                    Sql.Append(", status = @status");
                    Sql.Append(", isAdmin = @isAdmin");
                }

                Sql.Append(" Where id = @id");

                using (SqlCommand comm = new SqlCommand(Sql.ToString(), conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@id", obj.id);
                    comm.Parameters.AddWithValue("@name", obj.name);
                    comm.Parameters.AddWithValue("@email", obj.email);

                    if (!limited)
                    {
                        comm.Parameters.AddWithValue("@status", obj.status);
                        comm.Parameters.AddWithValue("@isAdmin", obj.isAdmin);
                    }
                    if (!string.IsNullOrEmpty(obj.password))
                        comm.Parameters.AddWithValue("@password", obj.password);

                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
        public static stMembers getMemberByEmail(string email)
        {
            stMembers obj = new stMembers();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From members Where email = @email", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@email", email);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.name = Convert.ToString(dr["name"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.password = Convert.ToString(dr["password"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                            obj.isAdmin = Convert.ToBoolean(dr["isAdmin"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void add(stMembers obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand comm = new SqlCommand("Insert Into members (name, email, password, status, saveDate, isAdmin) Values(@name, @email, @password, @status, @saveDate, @isAdmin)", conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@name", obj.name);
                    comm.Parameters.AddWithValue("@email", obj.email);
                    comm.Parameters.AddWithValue("@password", obj.password);
                    comm.Parameters.AddWithValue("@status", obj.status);
                    comm.Parameters.AddWithValue("@saveDate", DateTime.Now);
                    comm.Parameters.AddWithValue("@isAdmin", obj.isAdmin);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand comm = new SqlCommand("Delete From members Where id = @id", conn))
                {
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
    }

    public class admin
    {
        public static DataSet memberList()
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From members ", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public class member_permissions
    {
        public struct stMember_permission
        {
            public string id, memberID, ePubID;
        }

        public static void add(stMember_permission obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into member_permission ( memberID, ePubID) Values( @memberID, @ePubID)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@memberID", obj.memberID);
                    cmd.Parameters.AddWithValue("@ePubID", obj.ePubID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string memberID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From member_permission Where memberID=@memberID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@memberID", memberID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static DataSet getEpubID(string name)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select id From epubs Where name=@name", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.ExecuteNonQuery();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
    }

    public class Platform
    {
        public struct stPlatform
        {
            public string id, platform, new_item, url, path, contact_link, header_text;
        }
        public static DataSet list()
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From platform ", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stPlatform getData(string id="")
        {
            id = id != "" ? id : rbm.getPlatformID();

            stPlatform obj = new stPlatform {id = "0"};
	        using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From platform Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.platform = Convert.ToString(dr["platform"]);
                            obj.url = Convert.ToString(dr["url"]);
							obj.path = Convert.ToString(dr["path"]);
                            obj.new_item = Convert.ToString(dr["new_item"]);
                            obj.contact_link = Convert.ToString(dr["contact_link"]);
                            obj.header_text = Convert.ToString(dr["header_text"]);
                        }
                    }
                }
            }
            return obj;
        }
        
		public static string getPlatformTitle(string resourceID)
        {
            string platform="";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("select  p.platform from resources r join bucket b on b.id = r.bucketID join platform p on p.id = b.platformID where r.id = @resourceID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            platform = Convert.ToString(dr["platform"]);
                        }
                    }
                }
            }
            return platform;
        }
		public static string add(stPlatform obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into platform (platform, new_item, url, path,contact_link,header_text) OUTPUT INSERTED.ID Values(@platform, @new_item, @url, @path,@contact_link,@header_text)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@platform", obj.platform);
                    cmd.Parameters.AddWithValue("@new_item", obj.new_item);
                    cmd.Parameters.AddWithValue("@contact_link", obj.header_text);
                    cmd.Parameters.AddWithValue("@header_text", obj.contact_link);


                    cmd.Parameters.AddWithValue("@path", obj.path);
                    cmd.Parameters.AddWithValue("@url", obj.url);

                    conn.Open();
                    string platformID = toStr(cmd.ExecuteScalar());
					addPages(platformID);
	                return platformID;
                }
            }
        }
        public static void update(stPlatform obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update platform Set platform=@platform, new_item=@new_item, path=@path, url=@url, contact_link=@contact_link, header_text=@header_text Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@platform", obj.platform);
					cmd.Parameters.AddWithValue("@new_item", obj.new_item);
					cmd.Parameters.AddWithValue("@path", obj.path);
					cmd.Parameters.AddWithValue("@url", obj.url);
                    cmd.Parameters.AddWithValue("@contact_link", obj.contact_link);
                    cmd.Parameters.AddWithValue("@header_text", obj.header_text);



                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From platform Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

	    private static void addPages(string platformID)
	    {
			List<stPage> pages = new List<stPage>
			{
				new stPage {title = "Home", keyword = "home", status = "0", isContentEditable = true, isHideable = true},
				new stPage {title = "Promotional Library", keyword = "promotionallibrary", status = "1", isContentEditable = false, isHideable = false},
				new stPage {title = "FAQ", keyword = "faq", status = "1", isContentEditable = false, isHideable = true},
				new stPage {title = "Extra Page 1", keyword = "extra1", status = "0", isContentEditable = true, isHideable = true},
				new stPage {title = "Extra Page 2", keyword = "extra2", status = "0", isContentEditable = true, isHideable = true},
				new stPage {title = "Extra Page 3", keyword = "extra3", status = "0", isContentEditable = true, isHideable = true},
				new stPage {title = "Extra Page 4", keyword = "extra4", status = "0", isContentEditable = true, isHideable = true},
			};

		    foreach (stPage page in pages)
		    {
				using (SqlConnection conn = new SqlConnection(getConn()))
				{
					using (SqlCommand cmd = new SqlCommand("Insert Into pages (platformID, keyword, title, status, isContentEditable, isHideable) Values(@platformID, @keyword, @title, @status, @isContentEditable, @isHideable)", conn))
					{
						cmd.Parameters.AddWithValue("@platformID", platformID);
						cmd.Parameters.AddWithValue("@keyword", page.keyword);
						cmd.Parameters.AddWithValue("@title", page.title);
						cmd.Parameters.AddWithValue("@status", page.status);
						cmd.Parameters.AddWithValue("@isContentEditable", page.isContentEditable);
						cmd.Parameters.AddWithValue("@isHideable", page.isHideable);
						cmd.CommandType = CommandType.Text;
						conn.Open();
						cmd.ExecuteNonQuery();
					}
				}
		    }
	    }
		public struct stPage
		{
			public string id, platformID, keyword, title, status, html;
			public bool isContentEditable, isHideable;
		}
		public static DataSet pageList(string platformID)
	    {
			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				conn.Open();
				using (SqlCommand cmd = new SqlCommand("Select * From pages Where platformID=@platformID Order by ordr asc", conn))
				{
					cmd.Parameters.AddWithValue("@platformID", platformID);
					using (SqlDataAdapter da = new SqlDataAdapter(cmd))
					{
						DataSet ds = new DataSet();
						da.Fill(ds);
						return ds;
					}
				}
			}
	    }
		public static stPage getPage(string id)
		{
			stPage obj = new stPage();
			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				using (SqlCommand cmd = new SqlCommand("Select * From pages Where id = @id", conn))
				{
					conn.Open();
					cmd.Parameters.AddWithValue("@id", id);
					using (SqlDataReader dr = cmd.ExecuteReader())
					{
						if (dr.Read())
						{
							obj.id = Convert.ToString(dr["id"]);
							obj.platformID = Convert.ToString(dr["platformID"]);
							obj.title = Convert.ToString(dr["title"]);
							obj.status = Convert.ToString(dr["status"]);
							obj.html = Convert.ToString(dr["html"]);
							obj.isContentEditable = Convert.ToBoolean(dr["isContentEditable"]);
							obj.isHideable = Convert.ToBoolean(dr["isHideable"]);
						}
					}
				}
			}
			return obj;
		}
		public static stPage getPage(string keyword, bool tmp = false)
		{
            string platformID = rbm.getPlatformID();
			stPage obj = new stPage();
			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				using (SqlCommand cmd = new SqlCommand("Select * From pages Where platformID=@platformID And keyword = @keyword", conn))
				{
					conn.Open();
					cmd.Parameters.AddWithValue("@keyword", keyword);
                    cmd.Parameters.AddWithValue("@platformID", platformID);

					using (SqlDataReader dr = cmd.ExecuteReader())
					{
						if (dr.Read())
						{
							obj.id = Convert.ToString(dr["id"]);
							obj.platformID = Convert.ToString(dr["platformID"]);
							obj.title = Convert.ToString(dr["title"]);
							obj.status = Convert.ToString(dr["status"]);
							obj.html = Convert.ToString(dr["html"]);
							obj.isContentEditable = Convert.ToBoolean(dr["isContentEditable"]);
							obj.isHideable = Convert.ToBoolean(dr["isHideable"]);
						}
					}
				}
			}
			return obj;
		}
		public static void updatePage(stPage obj)
		{
			StringBuilder sql = new StringBuilder("Update pages Set title = @title");
			if (!isNull(obj.status))
				sql.Append(", status = @status");
			if (!isNull(obj.html))
				sql.Append(", html = @html");

			sql.Append(" Where id=@id");
			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				using (SqlCommand cmd = new SqlCommand(sql.ToString(), conn))
				{
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.AddWithValue("@id", obj.id);
					cmd.Parameters.AddWithValue("@title", obj.title);
					if (!isNull(obj.status))
						cmd.Parameters.AddWithValue("@status", obj.status);
					if (!isNull(obj.html))
						cmd.Parameters.AddWithValue("@html", obj.html);
					conn.Open();
					cmd.ExecuteNonQuery();
				}
			}
		}

        public static void setPageOrder(string id, string ordr)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update pages set ordr=@ordr Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@ordr", ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
	    public static string topMenu(string platformID)
	    {
			StringBuilder topMenu= new StringBuilder("<ul id=\"topMenu\" class=\"menu\">");

			using (SqlConnection conn = new SqlConnection(getConn()))
			{
				using (SqlCommand cmd = new SqlCommand("Select keyword, title From pages Where platformID = @platformID and status = 1 Order by ordr asc", conn))
				{
					conn.Open();
					cmd.Parameters.AddWithValue("@platformID", platformID);
					using (SqlDataReader dr = cmd.ExecuteReader())
					{
						while (dr.Read())
						{
							topMenu.Append("<li><a id=\"" + dr["keyword"] + "\" class=\"header_link\" href=\"" + getLink(dr["keyword"]) + "\" title=\"" + dr["title"] + "\">" + dr["title"] + "</a></li>");
						}
					}
				}
			}

			topMenu.Append("</ul>");

		    return topMenu.ToString();
	    }
	    private static string getLink(object tmp)
	    {
		    string dom = "/" + getPlatformName();
			string link = toStr(tmp);
		    if (link == "assetLink")
				return dom + "/";
			
			if (link == "faq")
				return dom + "/faqs";

		    return dom + "/content?page=" + link;
	    }
    }

    public class Categories
    {
        public struct stCategories
        {
            public string id, title, type, platformID;
        }

        public static DataSet list(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From categories where platformID = @platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet listFaqCategoriesAdmin(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select *, cast(0 as bit) as 'selected' From categories where type='faq' And platformID = @platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet listFaqCategories(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select *, cast(0 as bit) as 'selected' From categories where type='faq' And platformID = @platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet listBucketCategories(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From categories where type='bucket' And platformID = @platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static stCategories getData(string id)
        {
            stCategories obj = new stCategories();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From categories Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.type = Convert.ToString(dr["type"]);

                        }
                    }
                }
            }
            return obj;
        }
        public static void add(stCategories obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into categories (title, type , platformID ) Values(@title, @type, @platformID )", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@type", obj.type);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void update(stCategories obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update categories Set title=@title, type=@type Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@type", obj.type);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From categories Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class Bucket
    {

        public struct stBucket
        {
            public string id, title, description, saveDate, bucketTypeID, categoryID, platformID, active;
        }

        public static DataSet list(string platformID)
        {
            string sql = "Select b.*, (Select c.title From categories c Where c.id = b.categoryID) as category, (Select bt.title From bucketType bt Where bt.id = b.bucketTypeID) as bucketType , p.platform From bucket b join platform p on p.id = b.platformID where b.active = 1 And b.platformID  = @platformID";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listAdmin(string platformID)
        {
            string sql = "Select b.*, (Select c.title From categories c Where c.id = b.categoryID) as category, (Select bt.title From bucketType bt Where bt.id = b.bucketTypeID) as bucketType From bucket b where b.platformID=@platformID";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet ToolsByBucketType(string bucketTypeID)
        {
            string sql = "Select t.* From tools t Where t.bucketTypeID =@bucketTypeID";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@bucketTypeID", bucketTypeID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet ToolsByBucketID(string bucketID)
        {
            string sql = "select bt.toolID, (Select t.title from tools t where t.id = bt.toolID) as title from bucketTools bt where bucketID = @bucketID";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static bool SelectedTools(string bucketID, string toolID)
        {
            string sql = "select cast(count(*) as bit) as checked from bucketTools Where bucketID = @bucketID and toolID = @toolID ";
            bool isChecked = false;
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    cmd.Parameters.AddWithValue("@toolID", toolID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            isChecked = Convert.ToBoolean(dr["checked"]);
                        }
                    }
                }
            }
            return isChecked;
        }

        public static void ClearBucketTools(string bucketID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete  From bucketTools Where bucketID = @bucketID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    cmd.ExecuteNonQuery();
                }
            }
        }


        public static void AddBucketTools(string bucketID, string toolID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into bucketTools (toolID, bucketID) Values(@toolID, @bucketID)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    cmd.Parameters.AddWithValue("@toolID", toolID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }


        public static stBucket getData(string id)
        {
            stBucket obj = new stBucket();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From bucket Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                            obj.bucketTypeID = Convert.ToString(dr["bucketTypeID"]);
                            obj.categoryID = Convert.ToString(dr["categoryID"]);
                            obj.platformID = Convert.ToString(dr["platformID"]);
                            obj.active = Convert.ToString(dr["active"]);

                        }
                    }
                }
            }
            return obj;
        }

        public static string add(stBucket obj)
        {
            string insertedID = "";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into bucket ( title, description, saveDate, bucketTypeID, categoryID, platformID, active ) Values( @title, @description, @saveDate, @bucketTypeID, @categoryID, @platformID, @active) SELECT SCOPE_IDENTITY() AS [insertedID];", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@saveDate", obj.saveDate);
                    cmd.Parameters.AddWithValue("@bucketTypeID", obj.bucketTypeID);
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);
                    cmd.Parameters.AddWithValue("@active", "1");

                    conn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            insertedID = Convert.ToString(dr["insertedID"]);
                        }
                    }
                }
            }
            return insertedID;
        }

        public static void update(stBucket obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update bucket Set title=@title, description=@description, saveDate=@saveDate, bucketTypeID=@bucketTypeID, categoryID=@categoryID, platformID=@platformID Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@saveDate", obj.saveDate);
                    cmd.Parameters.AddWithValue("@bucketTypeID", obj.bucketTypeID);
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);


                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void disableBucket(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update bucket Set active = 0 Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void enableBucket(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update bucket Set active = 1 Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class BucketType
    {
        public struct stBucketType
        {
            public string id, title;
        }

        public static DataSet list()
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From bucketType", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static bool isVideo(string id)
        {
            bool isVideo = false;
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select cast(count(bt.id) as bit) as isVideo From bucketType bt Inner Join bucket b on b.bucketTypeID = bt.id Inner Join resources r on r.bucketID = b.id Where r.id = @id And bt.id = 1", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isVideo = Convert.ToBoolean(dr["isVideo"]);
                        }
                    }
                }
            }
            return isVideo;
        }

        public static bool isDocument(string resourceID)
        {
            bool isDocument = false;
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select cast(count(bt.id) as bit) as isDocument From bucketType bt Inner Join bucket b on b.bucketTypeID = bt.id Inner Join resources r on r.bucketID = b.id Where r.id = @id And bt.id = 3", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isDocument = Convert.ToBoolean(dr["isDocument"]);
                        }
                    }
                }
            }
            return isDocument;
        }

        public static stBucketType getData(string id)
        {
            stBucketType obj = new stBucketType();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From bucketType Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.title = Convert.ToString(dr["title"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void add(stBucketType obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into bucketType (title ) Values(@title)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void update(stBucketType obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update bucketType Set title=@title Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From bucketType Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

    }

    public class FrequentQuestions
    {
        public struct stFrequentQuestion
        {
            public string id, question, answer, categoryID, platformID;
        }

        public static DataSet list(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select *, Cast(0 as Bit) as 'selected' From frequentQuestion fq join categories c on c.id = fq.categoryID Where c.platformID = @platformID  order by fq.id desc", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listAdmin(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("select q.*, (select c.title from categories c where c.id = q.categoryID) as category from frequentQuestion q where platformID = @platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listByBucketID(string bucketID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select fqa.questionID, (Select fq.question from frequentQuestion fq Where fq.id = fqa.questionID) as question from frequentQuestionAssoc fqa Where bucketID=@bucketID", conn))
                {
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stFrequentQuestion getData(string id)
        {
            stFrequentQuestion obj = new stFrequentQuestion();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From frequentQuestion Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.question = Convert.ToString(dr["question"]);
                            obj.answer = Convert.ToString(dr["answer"]);
                            obj.categoryID = Convert.ToString(dr["categoryID"]);
                            obj.platformID = Convert.ToString(dr["platformID"]);


                        }
                    }
                }
            }
            return obj;
        }

        public static string add(stFrequentQuestion obj)
        {
            string insertedID = "";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into frequentQuestion (question, answer, categoryID, platformID ) Values( @question, @answer, @categoryID, @platformID) SELECT SCOPE_IDENTITY() AS [insertedID];", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@question", obj.question);
                    cmd.Parameters.AddWithValue("@answer", obj.answer);
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);


                    conn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            insertedID = Convert.ToString(dr["insertedID"]);
                        }
                    }
                }
            }
            return insertedID;
        }

        public static void update(stFrequentQuestion obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update frequentQuestion Set  question=@question, answer=@answer,categoryID=@categoryID Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@question", obj.question);
                    cmd.Parameters.AddWithValue("@answer", obj.answer);
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From frequentQuestion Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }


    }

    public class FrequentQuestionsAssoc
    {
        public struct stFrequentQuestionAssoc
        {
            public string id, questionID, bucketID;
        }

        public static DataSet list()
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From frequentQuestionAssoc", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stFrequentQuestionAssoc getData(string id)
        {
            stFrequentQuestionAssoc obj = new stFrequentQuestionAssoc();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From frequentQuestionAssoc Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.questionID = Convert.ToString(dr["questionID"]);
                            obj.bucketID = Convert.ToString(dr["bucketID"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void add(stFrequentQuestionAssoc obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into frequentQuestionAssoc ( questionID, bucketID ) Values( @questionID, @bucketID) ", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@questionID", obj.questionID);
                    cmd.Parameters.AddWithValue("@bucketID", obj.bucketID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void update(stFrequentQuestionAssoc obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update frequentQuestionAssoc Set  questionID=@questionID, bucketID=@bucketID Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@questionID", obj.questionID);
                    cmd.Parameters.AddWithValue("@bucketID", obj.bucketID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From frequentQuestionAssoc Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void clearAssociations(string questionID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete  From frequentQuestionAssoc Where questionID=@questionID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@questionID", questionID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static string bucketIsChecked(string bucketID, string questionID)
        {
            string isChecked = "";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("select convert(varchar(10),id) as checked from frequentQuestionAssoc where bucketID = @bucketID and questionID = @questionID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    cmd.Parameters.AddWithValue("@questionID", questionID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isChecked = Convert.ToString(dr["checked"]);
                        }
                        else
                            isChecked = "";
                    }
                }
            }
            return isChecked;
        }

    }

    public class BucketTypeControlAssoc
    {
        public struct stBucketTypeControlAssoc
        {
            public string id, bucketTypeID, bucketControlID;
        }

        public static DataSet list()
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From bucketTypeControlAssoc", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stBucketTypeControlAssoc getData(string id)
        {
            stBucketTypeControlAssoc obj = new stBucketTypeControlAssoc();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From bucketTypeControlAssoc Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.bucketTypeID = Convert.ToString(dr["bucketTypeID"]);
                            obj.bucketControlID = Convert.ToString(dr["bucketControlID"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void add(stBucketTypeControlAssoc obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into bucketTypeControlAssoc ( bucketTypeID, bucketControlID ) Values( @bucketTypeID, @bucketControlID)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@bucketTypeID", obj.bucketTypeID);
                    cmd.Parameters.AddWithValue("@bucketControlID", obj.bucketControlID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void update(stBucketTypeControlAssoc obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update bucketTypeControlAssoc Set id=@id, bucketTypeID=@bucketTypeID, bucketControlID=@bucketControlID Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@bucketTypeID", obj.bucketTypeID);
                    cmd.Parameters.AddWithValue("@bucketControlID", obj.bucketControlID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From bucketTypeControlAssoc Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void clearAssociations(string bucketControlID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete  From bucketTypeControlAssoc Where bucketControlID=@bucketControlID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@bucketControlID", bucketControlID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static string bucketIsChecked( string bucketControlID, string bucketTypeID)
        {
            string isChecked = "";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("select convert(varchar(10),id) as checked from bucketTypeControlAssoc where bucketTypeID = @bucketTypeID and bucketControlID = @bucketControlID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@bucketTypeID", bucketTypeID);
                    cmd.Parameters.AddWithValue("@bucketControlID", bucketControlID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isChecked = Convert.ToString(dr["checked"]);
                        }
                        else
                            isChecked = "";
                    }
                }
            }
            return isChecked;
        }

        public static string controlEsists(string bucketControlID, string bucketTypeID)
        {
            string isChecked = "";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select bucketTypeID from bucketTypeControlAssoc where bucketControlID = @bucketControlID and bucketTypeID=@bucketTypeID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@bucketControlID", bucketControlID);
                    cmd.Parameters.AddWithValue("@bucketTypeID", bucketTypeID); 


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isChecked = Convert.ToString(dr["bucketTypeID"]);
                        }
                        else
                            isChecked = "";
                    }
                }
            }
            return isChecked;
        }

    }
    
    public class Resources {
        public struct stResources
        {
            public string id, bucketID, title, description, filename, platformID, saveDate, saveBy, editDate, editBy, downloads, shares, bucket, path, link, linkImage, bucketTypeID;
        }
        public static DataSet list(string platformID)
        {

			log("asset list 4 " + platformID);

            var obj = Platform.getData();
            string whatIsNew = !rbm.isNull(obj.new_item) ? obj.new_item : "7";
            string showActive = "cast(1 as bit)";
            try
            {
                rbm.member.stMembers user = (rbm.member.stMembers)HttpContext.Current.Session["user"];
                if (rbm.isNull(user.id))
                {
                    showActive = "cast(b.active as bit)";
                }
            }
            catch (Exception)
            {
                showActive = "cast(b.active as bit)";
            }

            string sql = " Select r.*, (Select " + showActive + "  From bucket b Where b.id = r.bucketID) as active, (Select b.title From bucket b Where b.id = r.bucketID) as bucket, (Select b.bucketTypeID From bucket b Where b.id = r.bucketID) as bucketTypeID, (Select '/_resources/' + (Select p.platform From platform p Where p.id = r.platformID) + '/' +  b.title +'/' + r.filename From bucket b Where b.id = r.bucketID) as path  "
                        +" ,tags = case "
                         + " When Stuff((Select ', ' + cast(tag as varchar(10)) [text()] from tags where resourceID = r.id for xml path(''), type).value('.', 'nvarchar(max)'),1,2,' ') is null then '' "
                         + " else Stuff((Select ', ' + cast(tag as varchar(10)) [text()] from tags where resourceID = r.id for xml path(''), type).value('.', 'nvarchar(max)'),1,2,' ') end "

                        + " ,'new' = CASE When Datediff(day,r.saveDate, getdate())< " + whatIsNew + " then cast(1 as bit) else cast(0 as bit) end "
                        + " From resources r Where platformID=@platformID order by r.id desc ";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet resourceStats(string platformID)
        {
            
            var obj = rbm.Platform.getData();
            string sql = "select s.*, (Select r.title From resources r Where r.id = s.resourceID) as title,(Select ra.saveDate From resources ra Where ra.id = s.resourceID) as saveDate from stats s  Where s.platformID = @platformID order by cnt desc ";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {

                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet exportStats(string platformID)
        {

            var obj = rbm.Platform.getData();
            string sql = "select state,city, (Select r.title From resources r Where r.id = s.resourceID) as 'product',cnt as 'downloads',(Select ra.saveDate From resources ra Where ra.id = s.resourceID) as 'uploaded on', convert(varchar,statDate,1) as 'last downloaded' from stats s  Where s.platformID = @platformID order by cnt desc ";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {

                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet recentlyAdded(string platformID)
        {
            var obj = rbm.Platform.getData();
            string sql = " Select top 10 r.id, r.title,r.bucketID, b.bucketTypeID, '/_resources/" + obj.platform + "/' + b.title + '/' + r.filename  as 'path' From resources r Join bucket b On r.bucketID = b.id Where b.bucketTypeID = 2 And r.platformID=@platformID order by r.id desc";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {

                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static string getPathAndFile(string resourceID)
        {
            string path_filename = "";
            string sql = "select ('/_resources/' + (select p.platform From platform p Where p.id = r.platformID) + '/' +  (select b.title from bucket b where b.id = r.bucketID) + '/') as path, r.filename From resources r Where r.id = @resourceID";
            using (SqlConnection conn = new SqlConnection(getConn())) 
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            path_filename = Convert.ToString(dr["path"]) + "|" + Convert.ToString(dr["filename"]);
                        }
                    }
                }
            }
            return path_filename;
        }
        public static string getResourcePath(string resourceID)
        {
            string path = "";
            string sql = "select ('/_resources/' + (select p.platform From platform p Where p.id = r.platformID) + '/' +  (select b.title from bucket b where b.id = r.bucketID) + '/') as 'path' From resources r Where r.id = @resourceID";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            path = Convert.ToString(dr["path"]);
                        }
                    }
                }
            }
            return path;
        }
        public static stResources getData(string id)
        {
            string platform = Platform.getPlatformTitle(id);
            stResources obj = new stResources();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select r.*, (Select b.title From bucket b Where b.id = r.bucketID) as bucket, (Select '/_resources/" + platform + "/'  + b.title +'/' + r.filename From bucket b Where b.id = r.bucketID) as path,  (Select b.bucketTypeID From bucket b Where b.id = r.bucketID) as bucketTypeID From resources r Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.bucketID = Convert.ToString(dr["bucketID"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.filename = Convert.ToString(dr["filename"]);
                            obj.platformID = Convert.ToString(dr["platformID"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                            obj.saveBy = Convert.ToString(dr["saveBy"]);
                            obj.editDate = Convert.ToString(dr["editDate"]);
                            obj.editBy = Convert.ToString(dr["editBy"]);
                            obj.downloads = Convert.ToString(dr["downloads"]);
                            obj.shares = Convert.ToString(dr["shares"]);
                            obj.bucket = Convert.ToString(dr["bucket"]);
                            obj.path = Convert.ToString(dr["path"]);
                            obj.link = Convert.ToString(dr["link"]);
                            obj.linkImage = Convert.ToString(dr["linkImage"]);
                            obj.bucketTypeID = Convert.ToString(dr["bucketTypeID"]);

                        }
                    }
                }
            }
            return obj;
        }
        public static stResources add(stResources obj)
        {
            var video = rbm.CustomTasks.processVideoLink(obj.link);
            obj.link = video.link;
            obj.linkImage = video.image;

            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into resources ( bucketID, title, description, filename, platformID, saveDate, saveBy,link,linkImage ) Values( @bucketID, @title, @description, @filename, @platformID, @saveDate, @saveBy,@link,@linkImage) SELECT SCOPE_IDENTITY() AS [insertedID]", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@bucketID", obj.bucketID);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@filename", obj.filename);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);
                    cmd.Parameters.AddWithValue("@saveDate", DateTime.Now.ToString());
                    cmd.Parameters.AddWithValue("@saveBy",  obj.saveBy);

                    cmd.Parameters.AddWithValue("@link", obj.link);
                    cmd.Parameters.AddWithValue("@linkImage", obj.linkImage);


                    conn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["insertedID"]);
                        }
                    }
                }
            }
            var bucket = Bucket.getData(obj.bucketID);
            if (bucket.bucketTypeID == "4" || bucket.bucketTypeID == "2")
            {
                CustomTasks.pdfThumb(obj.id, ".30"); 
            }
            return obj;
        }
        public static void update(stResources obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update resources Set  bucketID=@bucketID, title=@title, description=@description, filename=@filename, platformID=@platformID, editDate=@editDate, editBy=@editBy, link=@link, linkImage=@linkImage Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@bucketID", obj.bucketID);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@filename", obj.filename);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);


                    cmd.Parameters.AddWithValue("@editDate", obj.editDate);
                    cmd.Parameters.AddWithValue("@editBy", obj.editBy);

                    cmd.Parameters.AddWithValue("@link", obj.link);
                    cmd.Parameters.AddWithValue("@linkImage", obj.linkImage);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void setThumb(string id, string thumbpath)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update resources Set  linkImage=@linkImage Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@linkImage", thumbpath);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void countDownload(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update resources Set downloads = downloads + 1 Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From resources Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }


    }

    public class ResourceTools
    {
        public struct stResourceTools
        {
            public string id, resourceID, toolID, width, height, isTrue, title;
        }

        public struct stToolList
        {
            public List<stResourceTools> tools;
        }

        public static DataSet list(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select toolID, width, height, isTrue From resourceTools Where resourceID=@resourceID Order By toolID Asc", conn))
                {
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }



        public static DataSet ListResourceTools(string resourceID, string bucketID)
        {
            string sql = "";
            if (resourceID != "")
            {
                sql = " Select bt.toolID, (Select t.title From tools t Where t.id = bt.toolID) as title, (Select t.description From tools t Where t.id = bt.toolID) as description, (Select t.bucketTypeID From tools t Where t.id = bt.toolID) as bucketTypeID "
                            + " ,'width'= "
                            + " CASE When (Select rt.width From resourceTools rt Where rt.resourceID = @resourceID and rt.toolID = bt.toolID) is not null then (Select rt.width From resourceTools rt Where rt.resourceID = @resourceID and rt.toolID = bt.toolID) else 0 end  "
                            + " ,'height'= "
                            + " CASE When (Select rt.height From resourceTools rt Where rt.resourceID = @resourceID and rt.toolID = bt.toolID) is not null then (Select rt.height From resourceTools rt Where rt.resourceID = @resourceID and rt.toolID = bt.toolID) else 0 end  "
                            + " , 'selected' =  "
                            + " CASE When (Select rt.toolID From resourceTools rt Where rt.resourceID = @resourceID and rt.toolID = bt.toolID) is not null then cast(1 as bit) "
                            + " else cast(0 as bit) end  "
                            + " from bucketTools bt  "
                            + " where bt.bucketID = @bucketID ";
            }
            else
            {
                sql = " Select bt.toolID"
                     + " , (Select t.title From tools t Where t.id = bt.toolID) as title"
                     + " , (Select t.bucketTypeID From tools t Where t.id = bt.toolID) as bucketTypeID  "
                     + " ,'width'= 0  ,'height'= 0 ,'selected' = cast(0 as bit) "
                     + " from bucketTools bt  "
                     + " where bt.bucketID = @bucketID ";
            }
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    if (resourceID != "") {
                        cmd.Parameters.AddWithValue("@resourceID", resourceID);                    
                    }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stToolList getTools(string resourceID)
        {
            stToolList lst = new stToolList();
            lst.tools = new List<stResourceTools>();
            var obj = new stResourceTools();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select rt.*, (Select t.title From tools t Where t.id = rt.toolID) as title From resourceTools rt Where resourceID=@resourceID Order By rt.toolID Asc", conn))
                {
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.resourceID = Convert.ToString(dr["resourceID"]);
                            obj.toolID = Convert.ToString(dr["toolID"]);
                            obj.width = Convert.ToString(dr["width"]);
                            obj.height = Convert.ToString(dr["height"]);
                            obj.height = Convert.ToString(dr["height"]);
                            obj.isTrue = Convert.ToString(dr["isTrue"]);
                            obj.title = Convert.ToString(dr["title"]);

                            lst.tools.Add(obj);
                        }
                    }
                }
            }
            return lst;
        }

        public static void add(stResourceTools obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into resourceTools ( resourceID, toolID, width, height, isTrue ) Values( @resourceID, @toolID, @width, @height, @isTrue)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", obj.resourceID);
                    cmd.Parameters.AddWithValue("@toolID", obj.toolID);
                    cmd.Parameters.AddWithValue("@width", obj.width);
                    cmd.Parameters.AddWithValue("@height", obj.height);
                    cmd.Parameters.AddWithValue("@isTrue", obj.isTrue);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void clearTools(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From resourceTools Where resourceID=@resourceID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }


    }

    public class Tags
    {
        public struct stTags
        {
            public string id, resourceID, tag, platformID;
        }
        public static DataSet list(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From tags Where resourceID=@resourceID", conn))
                {
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listForAutocomplete(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select distinct(tag) as 'text' from tags where platformID=@platformID", conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet getThemes(string platformID)
        {
            string sql = "Select  Count(Distinct resourceID) as 'rating', tag as 'theme', Cast(0 as bit) as 'selected' From tags Where tag != '' and platformID = @platformID Group By tag Order By Count(Distinct resourceID) desc";
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static string getTags(string resourceID)
        {
            StringBuilder sbTags = new StringBuilder();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand comm = new SqlCommand("Select tag From tags Where resourceID = @resourceID", conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        string sp = "";
                        while (dr.Read())
                        {
                            sbTags.Append(sp + toStr(dr["tag"])); sp = ",";
                        }
                    }
                }
            }
            return sbTags.ToString();
        }
        public static void add(stTags obj, string platformID)
        {

            if (obj.platformID == "" || rbm.isNull(obj.platformID))
                obj.platformID = platformID;
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into tags ( resourceID, tag, platformID ) Values( @resourceID, @tag, @platformID)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", obj.resourceID);
                    cmd.Parameters.AddWithValue("@tag", obj.tag);
                    cmd.Parameters.AddWithValue("@platformID", obj.platformID);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void clearTags(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From tags Where resourceID=@resourceID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void deleteTheme(string tag, string platformID)
        {

            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From tags Where tag=@tag and platformID=@platformID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@tag", tag);
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class ResourceQuestions
    {
        public struct stResourceQuestions
        {
            public string id, questionID, resourceID, question, answer;
        }
        public struct stFaqList
        {
            public List<stResourceQuestions> faqs;
        }

        public static stFaqList getFaqs(string resourceID)
        {
            stFaqList lst = new stFaqList();
            lst.faqs = new List<stResourceQuestions>();
            var obj = new stResourceQuestions();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select rq.*, (Select q.question From frequentQuestion q Where q.id = rq.questionID) as question,(Select q.answer From frequentQuestion q Where q.id = rq.questionID) as answer From resourceQuestions rq Where rq.resourceID=@resourceID", conn))
                {
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.resourceID = Convert.ToString(dr["resourceID"]);
                            obj.questionID = Convert.ToString(dr["questionID"]);
                            obj.question = Convert.ToString(dr["question"]);
                            obj.answer = Convert.ToString(dr["answer"]);
                            lst.faqs.Add(obj);
                        }
                    }
                }
            }
            return lst;
        }

        public static bool bucketIsChecked(string resourceID, string questionID)
        {
            bool isChecked = false;
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("select cast(count(id) as bit) as checked from resourceQuestions where resourceID = @resourceID and questionID = @questionID ", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    cmd.Parameters.AddWithValue("@questionID", questionID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            isChecked = Convert.ToBoolean(dr["checked"]);
                        }
                    }
                }
            }
            return isChecked;
        }

        public static DataSet list(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select * From resourceQuestions Where resourceID=@resourceID", conn))
                {
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listResourceQuestions(string resourceID, string bucketID)
        {
            string sql = "";
            if (resourceID == "")
            {
                sql = " Select bq.questionID, (Select q.question From frequentQuestion q Where q.id = bq.questionID) as question "
                        + " , 'selected' =   cast(0 as bit)   "
                        + " from frequentQuestionAssoc bq "
                        + " where bq.bucketID = @bucketID";
            }
            else
            {
                sql = " Select rq.questionID, (Select fq.question From frequentQuestion fq Where fq.id = rq.questionID) as 'question',  Cast(1 as bit) as 'selected' From resourceQuestions rq Where resourceID = @resourceID";
            }
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@bucketID", bucketID);
                    if (resourceID != "") { cmd.Parameters.AddWithValue("@resourceID", resourceID); }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static void add(stResourceQuestions obj)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into resourceQuestions ( questionID, resourceID ) Values( @questionID, @resourceID)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@questionID", obj.questionID);
                    cmd.Parameters.AddWithValue("@resourceID", obj.resourceID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void delete(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From resourceQuestions Where resourceID=@resourceID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void clearResourceQuestions(string resourceID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Delete From resourceQuestions Where resourceID=@resourceID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@resourceID", resourceID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class Asset
    {
        public struct stAsset
        {
            public rbm.Resources.stResources asset;
            public rbm.ResourceTools.stToolList tools;
            public rbm.ResourceQuestions.stFaqList faqs;
            public bool video;
            public bool document;
        }

        public static stAsset getAsset(string id)
        {
            var obj = new stAsset();
            obj.asset = rbm.Resources.getData(id);
            obj.tools = rbm.ResourceTools.getTools(id);
            obj.faqs = rbm.ResourceQuestions.getFaqs(id);
            obj.video = rbm.BucketType.isVideo(id);
            obj.document = rbm.BucketType.isDocument(id);
            return obj;
        }
    }

    public class FilterOptions 
    {
        public struct stOption
        {
            public string option, type;
            public bool selected;
        }

        public struct stListOptions
        {
            public List<stOption> Options;
        }
        //TODO
        //public static DataSet listCategories(string platformID)
        //{
        //    using (SqlConnection conn = new SqlConnection(getConn()))
        //    {
        //        string sql = "Select distinct b.categoryID , (Select c.title From categories c Where c.id = b.categoryID) as 'categoryTitle', Cast(0 as Bit) as 'categorySelected' From bucket b Where b.platformID=@platformID Order By categoryID ";
        //        using (SqlCommand cmd = new SqlCommand(sql, conn))
        //        {
        //            cmd.Parameters.AddWithValue("@platformID", platformID);

        //            conn.Open();
        //            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //            {
        //                DataSet ds = new DataSet();
        //                da.Fill(ds);
        //                return ds;
        //            }
        //        }
        //    }
        //}
        public static DataSet listCategories(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                string sql = "Select c.id as categoryID, c.title as categoryTitle, cast(0 as bit) as 'categorySelected' from categories c Where  c.platformID = @platformID And c.type='bucket'";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet listBucketsByCategory(string categoryID)
        {

            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                string sql = "Select b.categoryID as 'bucketCategoryID', (Select c.title From categories c Where c.id = b.categoryID) as 'bucketCategory', b.title, Cast(0 as Bit) as 'selected' From bucket b  Where b.categoryID=@categoryID Order By b.categoryID ";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@categoryID", categoryID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listBuckets(string platformID)
        {
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                string sql = "Select b.categoryID as 'bucketCategoryID', (Select c.title From categories c Where c.id = b.categoryID) as 'bucketCategory', b.title, Cast(0 as Bit) as 'selected' From bucket b Where b.active = 1 And b.platformID = @platformID Order By b.title ";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@platformID", platformID);

                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static stListOptions bucketOptions()
        {
            stListOptions lst = new stListOptions();
            lst.Options = new List<stOption>();
            var obj = new stOption();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select title as 'option', (Select c.title From categories c Where c.id=b.categoryID) as 'category',  CAST(0 as bit) as 'selected' from bucket b order by title asc ", conn))
                {
                    //cmd.Parameters.AddWithValue("@groupType", groupType);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj.option = Convert.ToString(dr["option"]);
                            obj.type = Convert.ToString(dr["category"]);

                            obj.selected = Convert.ToBoolean(dr["selected"]);
                            lst.Options.Add(obj);
                        }
                    }
                }
            }
            return lst;
        }


        public static stListOptions themeOptions()
        {
            stListOptions lst = new stListOptions();
            lst.Options = new List<stOption>();
            var obj = new stOption();
            string[] array = new string[] { "Winter", "Summer", "Fall", "Spring" };
            for (int i = 0; i < array.Length; i++)
            {
                obj.option = array[i].ToString();
                obj.selected = false;
                lst.Options.Add(obj);
            }
            return lst;
        }

        public static stListOptions tagOptions()
        {
            stListOptions lst = new stListOptions();
            lst.Options = new List<stOption>();
            var obj = new stOption();
            using (SqlConnection conn = new SqlConnection(getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select top 10 tag as 'option',  CAST(0 as bit) as 'selected' from tags group by tag having count(*) > 1 order by tag asc", conn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj.option = Convert.ToString(dr["option"]);
                            obj.selected = Convert.ToBoolean(dr["selected"]);
                            lst.Options.Add(obj);
                        }
                    }
                }
            }
            return lst;
        }

        
    }

    public class CustomTasks
    {
        public struct stVideo
        {
            public string link, image;
        }
        public static stVideo processVideoLink(string link)
        {
            var vid = new stVideo();
            if (link.Replace(".", "").Contains("youtube"))
            {
                vid.link = rbm.CustomTasks.fixYouTube(link);
                vid.image = rbm.CustomTasks.fixYouTubeImage(link);
                return vid;
            }
            else if (link.Replace(".", "").Contains("vimeo"))
            {
                vid.link = rbm.CustomTasks.fixVimeo(link);
                vid.image = rbm.CustomTasks.fixVimeoImage(link.Substring((link.LastIndexOf("/") + 1)));
                return vid;
            }
            else
            {
                vid.link = link;
                vid.image = "/images/noImgAvail.png";
                return vid;
            }
        }
        public static string getFileSize(string path)
        {
            var fileSize = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(path)).Length;
            return Convert.ToDecimal(fileSize / (1024.0)).ToString("#.##") + " KB";
        }

        public static stFileDims getDimensions(string path)
        {
            try
            {
                System.Drawing.Image img = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                stFileDims obj = new stFileDims();
                obj.width = img.Width.ToString();
                obj.height = img.Height.ToString();
                return obj;
            }
            catch (Exception)
            {
                stFileDims obj = new stFileDims();
                obj.width = "";
                obj.height = "";
                return obj;
            }

        }

        public struct stFileData
        {
            public string filename, extension, filesize, width, height;
        }

        public struct stFileDims
        {
            public string width, height;
        }

        public struct stFileAndPath
        {
            public string filename, path;
        }

        public static string fixYouTube(string link)
        {
            int hash = link.LastIndexOf('/');
            string id = link.Substring(hash);
            return "https://www.youtube.com/embed" + id;
        }

        public static string fixYouTubeImage(string link)
        {
            int hash = link.LastIndexOf('/');
            string id = link.Substring(hash);
            if (id.Contains("?")) {
                id = id.Substring(0, id.IndexOf("?"));
            }
            return "http://img.youtube.com/vi" + id + "/0.jpg";
        }

        public static string fixVimeo(string link)
        {
            int hash = link.LastIndexOf('/');
            string id = link.Substring(hash);
            return "//player.vimeo.com/video" + id;
        }
        public static string fixVimeoImage(string vimeoID)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("http://vimeo.com/api/v2/video/" + vimeoID + ".xml");
                XmlElement root = doc.DocumentElement;
                string vimeoThumb = root.FirstChild.SelectSingleNode("thumbnail_medium").ChildNodes[0].Value;
                string imageURL = vimeoThumb;
                return imageURL;
            }
            catch
            {
                return "/images/file_icons/icon_pdf.png";
            }
        }

        public static stFileAndPath DownloadResource(string resourceID, string size, string targetType)
        {
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var file_and_path = new stFileAndPath();
            switch (targetType) { 
                case "pdf":
                    if (System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(path + obj.filename)) != ".pdf")
                    {
                        file_and_path = CustomTasks.NewPngToPdf(resourceID, "1");
                        //file_and_path = CustomTasks.ResizePdf(file_and_path, size);
                    }
                    else {
                        file_and_path = CustomTasks.ResizePdf(resourceID, size);
                    }
                    break;
                case "img":
                    string ext = System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(path + obj.filename));
                    switch (ext) { 
                        case ".png":
                            file_and_path = CustomTasks.ResizeImage(resourceID, size);
                            break;
                        case ".jpg":
                            file_and_path = CustomTasks.ResizeImage(resourceID, size);
                            break;
                        case ".jpeg":
                            file_and_path = CustomTasks.ResizeImage(resourceID, size);
                            break;
                        case ".gif":
                            file_and_path = CustomTasks.ResizeImage(resourceID, size);
                            break;
                        case ".pdf":
                            file_and_path = CustomTasks.PdfToPng(resourceID, size);
                            break;
                    }
                    break;

            }
            return file_and_path;
        }
        public static string ImgToPng(Resources.stResources obj)
        {
            var file = HttpContext.Current.Server.MapPath("/resources/images/" + obj.filename);

            var bitmap = Bitmap.FromFile(HttpContext.Current.Server.MapPath("/resources/images/" + obj.filename));
            bitmap.Save(Path.GetFileName(HttpContext.Current.Server.MapPath("/resources/images/" + obj.filename)) + ".png", ImageFormat.Png);
            return Path.GetFileName(HttpContext.Current.Server.MapPath("/resources/images/" + obj.filename)) + ".png";
        }
        public static stFileAndPath ResizeImage(string resourceID, string size)
        {
            float _size;

            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var newPng = resourceID + "_" + DateTime.Now.Second.ToString() + "_" + obj.filename;
            var file_and_path = new stFileAndPath();

            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path + obj.filename));
                var r = new ResizeSettings();
                r.Width = (int) Math.Round(_size*img.Width);
                r.Height = (int) Math.Round(_size*img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path + obj.filename),
                    HttpContext.Current.Server.MapPath(path + newPng), r);
                file_and_path.filename = newPng;
                file_and_path.path = path + newPng;
                return file_and_path;
            }
            catch (Exception)
            {
                file_and_path.filename = obj.filename;
                file_and_path.path = path + obj.filename;
                return file_and_path;
            }

        }

        public static stFileAndPath ResizePdf(string resourceID, string size)
        {
            float _size;
            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var file_and_path = new stFileAndPath();
            var newPdf = resourceID + "_" + DateTime.Now.Second.ToString() + ".pdf";
            if (size == "1" || _size == 1)
            {
                file_and_path.filename = obj.filename;
                file_and_path.path = path + obj.filename;
                return file_and_path;
            }
            else
            {

                    try
                    {
                        PdfManager objPdf = new PdfManager();
                        // Create a new document
                        PdfDocument objDoc = objPdf.CreateDocument();
                        //Get original document
                        PdfDocument objActual =
                            objPdf.OpenDocument(HttpContext.Current.Server.MapPath(path + obj.filename));
                        float actualWidth = objActual.Pages[1].Width;
                        float actualHeight = objActual.Pages[1].Height;
                        //Create the resized page
                        PdfPage objPage = objDoc.Pages.Add(actualWidth * _size, actualHeight * _size);
                        PdfGraphics objGraphics = objDoc.CreateGraphicsFromPage(objActual, 1);
                        //draw on the document
                        string graphicParams = "x=0;y=0;scalex=" + toStr(Convert.ToDecimal(size)) + ";scaley=" +
                                               toStr(Convert.ToDecimal(size));
                        objPage.Canvas.DrawGraphics(objGraphics, graphicParams);

                        //save the new version
                        objDoc.Save(HttpContext.Current.Server.MapPath(path + newPdf), true);
                        file_and_path.filename = newPdf;
                        file_and_path.path = path + newPdf;
                        return file_and_path;
                    }
                    catch (Exception)
                    {
                        file_and_path.filename = newPdf;
                        file_and_path.path = path + obj.filename;
                        return file_and_path;
                    }
 
            }

        }

        public static stFileAndPath ResizePdf(stFileAndPath stf, string size)
        {
            string path = stf.path.Replace(stf.filename, "");
            float _size;
            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var newPdf = DateTime.Now.ToString() + ".pdf";

                try
                {
                    PdfManager objPdf = new PdfManager();
                    // Create a new document
                    PdfDocument objDoc = objPdf.CreateDocument();
                    //Get original document
                    PdfDocument objActual =
                        objPdf.OpenDocument(HttpContext.Current.Server.MapPath(stf.path));
                    float actualWidth = objActual.Pages[1].Width;
                    float actualHeight = objActual.Pages[1].Height;
                    //Create the resized page
                    PdfPage objPage = objDoc.Pages.Add(actualWidth * _size, actualHeight * _size);
                    PdfGraphics objGraphics = objDoc.CreateGraphicsFromPage(objActual, 1);
                    //draw on the document
                    string graphicParams = "x=0;y=0;scalex=" + toStr(Convert.ToDecimal(size)) + ";scaley=" +
                                           toStr(Convert.ToDecimal(size));
                    objPage.Canvas.DrawGraphics(objGraphics, graphicParams);

                    //save the new version
                    objDoc.Save(HttpContext.Current.Server.MapPath(path + newPdf));
                    stf.filename = newPdf;
                    stf.path = path + newPdf;
                    return stf;
                }
                catch (Exception)
                {
                    return stf;
                }

        }

        public static stFileAndPath PngToPdf(string resourceID, string size)
        {
            float _size;
            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var newPdf = obj.id + "_" + DateTime.Now.ToString() + ".pdf";
            var file_and_path = ResizeImage(resourceID, size);

                try
                {
                    PdfManager objPdf = new PdfManager();
                    // Create empty document
                    PdfDocument objDoc = objPdf.CreateDocument();
                    // Add a new page
                    PdfPage objPage = objDoc.Pages.Add();
                    // Open image
                    PdfImage objImage = objDoc.OpenImage(HttpContext.Current.Server.MapPath(path + file_and_path.filename));
                    // Create empty param object
                    PdfParam objParam = objPdf.CreateParam();
                    objParam["x"] = (objPage.Width - objImage.Width / _size) / 2.0f;
                    objParam["y"] = objPage.Height - objImage.Height * _size / 2.0f - 200;
                    objParam["ScaleX"] = 1.0f / _size;
                    objParam["ScaleY"] = 1.0f / _size;
                    objPage.Canvas.DrawImage(objImage, objParam);
                    objDoc.Save(HttpContext.Current.Server.MapPath(path + newPdf), true);
                    file_and_path.filename = newPdf;
                    file_and_path.path = path + newPdf;
                    return file_and_path;
                }
                catch (Exception)
                {
                    file_and_path.filename = obj.filename;
                    file_and_path.path = path + obj.filename;
                    return file_and_path;
                }

        }

        public static stFileAndPath NewPngToPdf(string resourceID, string size)
        {
            float _size;
            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var newPdf = obj.id + "_" + DateTime.Now.Second.ToString();
            var file_and_path = ResizeImage(resourceID, size);

            // create instance of the PDF manager
            PdfManager objPDF;
            objPDF = new PdfManager();

            // Create new document
            PdfDocument objDoc = objPDF.CreateDocument();

            // Open image file
            PdfImage objImage;
            try
            {
                objImage = objDoc.OpenImage(HttpContext.Current.Server.MapPath(path + file_and_path.filename));
                objImage.SetColorMask("Min1=250;Max1=255; Min2=250;Max2=255; Min3=250;Max3=255");

            }
            catch (Exception e)
            {
                return file_and_path;
            }
            		// Add empty page. Page size is based on resolution and size of image
		    float fWidth = objImage.Width * 72.0f / objImage.ResolutionX;
		    float fHeight = objImage.Height * 72.0f / objImage.ResolutionY;
		    PdfPage objPage = objDoc.Pages.Add(fWidth,fHeight);

		    // Draw image
		    objPage.Canvas.DrawImage( objImage, "x=0, y=0" );

		    // Make page fit viewer
		    //objDoc.OpenAction = objPage.CreateDest("Fit=true");

		    // We use Session ID for file names.
		    // false means "do not overwrite"
		    // The method returns generated file name.
		    String strPath = HttpContext.Current.Server.MapPath(path) + newPdf + ".pdf";
		    String strFileName = objDoc.Save(strPath);
            file_and_path.filename = newPdf + ".pdf";
            file_and_path.path = path + newPdf + ".pdf";
            return file_and_path;
        }

        public static stFileAndPath PdfToPng(string resourceID, string size)
        {
            float _size;
            try
            {
                _size = float.Parse(size, CultureInfo.InvariantCulture.NumberFormat);
                if (size == "0")
                {
                    _size = 1;
                }
            }
            catch (Exception)
            {
                _size = 1;
            }
            var path = rbm.Resources.getResourcePath(resourceID);
            var obj = rbm.Resources.getData(resourceID);
            var newPdf = obj.id + "_" + toStr(_size).Replace(".", "").Replace("0", "");
            var file_and_path = new stFileAndPath();

            var newPng = obj.id + "_" + DateTime.Now.Second.ToString() + ".jpg";
            var resFactor = toStr(Math.Round(Convert.ToDecimal(_size*72)));
            var resolution = "ResolutionX=" + resFactor +";ResolutionY=" + resFactor;

                try
                {
                    PdfManager objPdf = new PdfManager();
                    PdfDocument objDoc = objPdf.OpenDocument(HttpContext.Current.Server.MapPath(path + obj.filename));
                    // Save and reopen as Page.Preview only works on new documents.
                    PdfDocument objNewDoc = objPdf.OpenDocument(objDoc.SaveToMemory());
                    PdfPage objPage = objNewDoc.Pages[1];
                    PdfPreview objPreview = objPage.ToImage(resolution);
                    objPreview.Save((HttpContext.Current.Server.MapPath(path + newPng)),true);
                    file_and_path.filename = newPng;
                    file_and_path.path = path + newPng;
                    return file_and_path;
                }
                catch (Exception)
                {
                    file_and_path.filename = newPng;
                    file_and_path.path = path + obj.filename;
                    return file_and_path;
                }

        }

        public static void pdfThumb(string resourceID, string size) 
        {
            var res = rbm.Resources.getData(resourceID);
            var path = rbm.Resources.getResourcePath(resourceID) + res.filename;
            string extension = System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(path));
            var obj = new stFileAndPath();
            switch (extension)
            {
                case ".pdf":
                    try
                    {
                        obj = CustomTasks.PdfToPng(resourceID, ".2");
                        Resources.setThumb(resourceID, obj.path);
                    }
                    catch (Exception)
                    {
                        
                    }
                    break;
            }
        }
    }

}