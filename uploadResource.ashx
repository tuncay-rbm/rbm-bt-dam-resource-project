﻿<%@ WebHandler Language="C#" Class="uploadResource" %>
 
using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using Newtonsoft.Json;

public class uploadResource : IHttpHandler
{
   
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Expires = -1;
        try{
            HttpPostedFile oFile = context.Request.Files["myFile"];
            if (oFile != null)
	        {
		        string id = context.Request["id"].Trim();
		        string folder = context.Request["folder"].Trim();

		        string savepath = context.Server.MapPath(folder + id + "/");
                string filename = oFile.FileName;
		        if (!Directory.Exists(savepath))
		        {
			        Directory.CreateDirectory(savepath);
		        }

                oFile.SaveAs(savepath + @"\" + filename);

                rbm.CustomTasks.stFileDims dims = rbm.CustomTasks.getDimensions(folder + id + "/"  + filename);

                rbm.CustomTasks.stFileData obj = new rbm.CustomTasks.stFileData();
                obj.filename = filename;
                obj.filesize = rbm.CustomTasks.getFileSize(folder + id + "/" + filename);
                obj.width = dims.width;
                obj.height = dims.height;
                string json = JsonConvert.SerializeObject(obj);
                context.Response.Write(json);
                
	        }
        } catch (Exception ex) {
            context.Response.Write("Error: " + ex.Message);
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}

